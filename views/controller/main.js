
$(document).ready(function () {

    if (location.pathname.substring(location.pathname.lastIndexOf("/") + 1) != "") {
        $("nav").addClass("fixedNav");
    }

    $(window).scroll( function () {

        if (location.pathname.substring(location.pathname.lastIndexOf("/") + 1) == "") {
            if (document.body.scrollTop > 400 || document.documentElement.scrollTop > 400) {
                $("nav").addClass("fixedNav");
                $("nav").removeClass("navbar");
            } else {
                $("nav").removeClass("fixedNav");
                $("nav").addClass("navbar");
            }
        }
    });
});

