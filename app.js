// Author: Wesley Gury Schrauwen
// Copyright © 2016 Stackle. All rights reserved.

// ----------------------------------
// ------------ READ ME -------------
// ----------------------------------

// GET requests reserved solely for serving web related content.

// POST requests reserved for Creating, Reading, Updating and Deleting Data.

// DELETE and UPDATE requests are invalid using this build.

// All other requests not used.

// ----------------------------------

// MARK: Import

const express = require("express");
const bodyParser = require("body-parser");
const firebase = require("firebase");
const ejs = require("ejs");
const algoliasearch = require("algoliasearch");
const performRequest = require("request");
const crypto = require("crypto");
const moment = require("moment");
const apn = require("apn");
moment().format();

var api_key = "key-59860ce0cbf86b7e8c4576d522692494";
var domain = "mg.stackle.co.za";
const mailgun = require("mailgun-js")({apiKey: api_key, domain: domain});

const paramError = "Parameters undefined";
const success200 = {
    code: 200,
    state: "success"
};

const provider = new apn.Provider({
    cert: __dirname+"/stackle_product_cert.pem",
    key: __dirname+"/stackle_production_key.pem",
    production: false
});

const failure500 = {
    code: 500,
    state: "failed",
    reason: "An internal error occurred"
};

const failure403 = {
    code: 403,
    state: "failed",
    reason: "Bad request; some parameters may be undefined."
};

const failure404 = {
    code: 404,
    state: "failed",
    reason: "The object you requested does not exist."
};

const failure400 = {
    code: 400,
    state: "failed",
    reason: "Invalid request or insufficient permissions."
};

const success202 = {
    "code": 202,
    "reason": "Client Response Required"
};

const businessLimitReachedError = {"code": 400, "reason": "user has reached their business limit"};

const pathNotFound = {
    code: 404,
    state: "failed",
    reason: "Path not found."
};

const insufficientFundsError = {
    code: 400,
    state: "transaction failed",
    reason: "Insufficient funds available"
};

const dailyLimitTooLowError = {
    code: 404,
    state: "update failed",
    reason: "Daily Limit is too low."
};

const DepositTooLowError = {
    code: 201,
    state: "update partially succeeded",
    reason: "BidWord successfully added to database. Remaining deposit too low, bidword failed to index."
};

const internalKey = "WNFW-M-BMMSDKFv378tfgckjx23ikjngkj-necASFGTHKTTJGUTfwkbcnqk";

const techEmails = ["wesley@stackle.co.za", "munei@stackle.co.za", "jaco@stackle.co.za", "timothy@stackle.co.za"];

const engineRef = "https://stackle-live.appspot.com";

var app = express();

app.engine("html", ejs.renderFile);
app.set("view engine", "ejs");

// MARK: Middleware

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(__dirname +"/views"));


//live server config
firebase.initializeApp({
    serviceAccount: {
        "type": "service_account",
        "project_id": "stackle-live",
        "private_key_id": "939e823d1bacd8ba5feacaea3c98377615d360d0",
        "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCfMEnOe1DQfkS/\n/zX+GAQk9JQozloKePO7OvyKS2BwEVIJVZ4/12TgY5meXG1ehJhhmZ5cXF9u5Kt/\nE45QI0gMa11+w7z/BRVRoc8s/WR410AyG6/YysjaWqYbS15/HMujHvu+vXswBIOA\nTOnYJc0uewP6hyIyzN6fCzu54mnE7T/63bTLsB+Z2Tynnd5N/90cVRKzTxDscdtH\n8wbRrTE28OgHHcF0kU53a5+mtgtraNyuvMmnOmgqDiGZJmHkTTZS2RqEtoTO01K9\nOVxxLG7643dtxkiNT43m3D/WRFst8cbXvERv6tRW1rhptp5pc+vIXN0QCkSr9ssi\nbnhCkhEzAgMBAAECggEAEGuCsXG23Eqp/MPuZc9EXit8PNlMIFhpi56B/iHYSLcA\np0XVlPA6z4p2hgcyoNU8gpS96Pj+PwoHRoxV2Z5KCD3q3vCBxixYDpuKKzHX8z8A\nuCpy8QG5xgwjph0YGjR2LNU9UmdpFYLhrZJBbLI5f9nghy75MBRBwlX+SyY3ksBc\ncfeovQrOwJ3Zz5tQWZB9tP4azTCCCBcYsMjDWx5EqcJuP+rXme7VO45bKTyMokS4\ninpLArUve6n1CtV0pqoJeGuNYtPUhOZ1lSCJI89af3skJpHm8avzeuzZD/mhQy94\nh32lsX7+7uWKkOIoAe3/coigUYJmIEUsSljJPcOWwQKBgQDStJfC59CTRgD+cN4L\n8X8y3pKx4mxUMZFcLIksdOpmjah+EiK3+MoPqubIE8b5o+GtaKEBxbYhprPHO0ON\nC0N13MY4LEqHTzYA0E1/lfLnfOiQw2UjG5p+UuFORVApXL2TcmigVeB1U/58utPF\n4jcjaMCdZSu98HYTHw4CuzE1xwKBgQDBaKeysltQg9c+Adcms7kK+vbMPdNQ2qUW\nSbYKsFDjMynQkiy9kCLFONeZLg9Dgmb29DPPQ/iHUGa/X5v1aUvKqzRK7W5wSsl1\nPF4ky8FRnA/4ea1Ic7flNNa9k+Gq0XqmQaWL5cEJ9aXi+ddMtapmzPnBQLsYDS07\ng/EjLdKZNQKBgBDtYzfMScxbFl9aj+wjtZVPnjdeOle1N7lQ9qA0sFQW0091g4aG\nHS2PDEVMmXKawNmeizjKOmyPqOe7khy1p/DDEMWYgVBHg84i7ebvE/mW5lQi7nAY\nmpvFasZP+TWpdq1uxosC+ypR2iMJG1RNbSKSGs9KPm5e8A1vND/x6+0nAoGAHwmB\nZ97RYpFVuECpWy+aMVXmxw3g3GfJ+FKp4Ps8b8llG3Bpt53qxoWBq7ZFgsrpZRX5\nPnik6GoRowJfz7kvDX397Uw+3cxSmfqvDO6ZvlPyCCjWkvrm9uRM1a1mmHf3W7jL\nLTEYklWWYl8/mrLIfqjEJ6wQmiJ7MMLSrpeK5UUCgYACx2UOZALK6+VE1C5QnhMn\njEY3tzSHorbmKc242APXCStjSwfNe8Ai+0amgyLxsHs/0CvotMSEa1MD/sb/FTQH\nu/tvLVHUo5c+ZVSajm/mGK8i5EE1hgk8CvDibUk39qrAWPPMDymCaZyGqbOx+S2r\n3sFoszzYIELPAhJiKNopfA==\n-----END PRIVATE KEY-----\n",
        "client_email": "firebase-stackle-live@stackle-live.iam.gserviceaccount.com",
        "client_id": "103229542244182859256",
        "auth_uri": "https://accounts.google.com/o/oauth2/auth",
        "token_uri": "https://accounts.google.com/o/oauth2/token",
        "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
        "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-stackle-live%40stackle-live.iam.gserviceaccount.com"
    },
    databaseURL: "https://stackle-live.firebaseio.com/"
});

//test server config
// firebase.initializeApp({
//     serviceAccount: {
//         "type": "service_account",
//         "project_id": "stackl3-dc415",
//         "private_key_id": "f9a97b4af678b618de047e7ad3ae28148d642ee8",
//         "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCSRDIm7623WF0g\nRETi7sbg0UeGjaPQ8oFVMH93h0E0oU4JQM/q2UZPFc7mZnArU3ed3QbZ3iBLT+Q7\n4oBJSjNwjF4ZPCY+iiKmFnIHOFmnhTMEkSKhn0rRvpbvBHxomB84Z4sC7PCnT+GK\nPlFND5CnSVqogzO0R2M1FynDS5gRKTql//Mm6gTtYPSQ7wggyLfVMyiSJ9e4ARjX\nYH2Yh+iuFCV3h7c7s302fdd9HvzQi84SdkKDNJP1+vatQkRwtDcjI0jU8U/UXt9X\nSKgSy0e297V45rvp+xokiO+WGPpkG356FMcA7XQykPmQCRbKH4LHQEmWZ0RZ6Xsh\nu/edRo0DAgMBAAECggEAUPPNjh85VjBh2mnudmsRR3yMybu63vpY+n1NDyqcvDRK\nrHRinuiCMWMwj/RCQLiBi625DPwTg5tEBqDlv6qBrdkCe5T+1b4+Tw49LiHR/RWL\nlwt8z1cKYevKPZ+N+5W2MdGc7pGU4AUmHw0+khf7R5shwgJbq3uXPNsFEkHUeCov\nt4NXIDucgM30HqEsUvMQE6HeeRD3LAwrrZmalz/S7Hh72ZYTzOttTLRxTKm33Hhj\nuc85TxfDhrZ4Sl2sDGb4+7xTlxWriV+ofybk0lTzycwBTAMzh4MpQdg2J5bg5DFJ\nuOV6CAgK4y/Cmm7F29+RM8mMsajhxlcyUfq8p9CDgQKBgQDoMrUNp8l8NTIUuWi2\neyxyL8z4Lknh5MQ24ZZDNxwrYREa82uzdd1O5YmXBzK8lBZcNaQUgRhgHqRqdsJC\nQ8qLplFiLVua7phbj+TfG0bBn6yn/p0h0CwEQCWerbhoYQtlEyI1MRtrQkzdP1+o\nFmuHc/QV9b9+pfz213G5L/BwTQKBgQChQnzRmL3ca/+GNDLKVY0eafyz3ruhJx3/\n7dkTh0o98rRjCMg+Px+CgCTvtjZMKZk9kB/ZjXOWb2B9+n64lydJ5tGRJYsSvp/h\noKk0ob0rMg7JGgtFVO8mkdRo9kmzIjb1wk8lpcxBUlKGKpi3sGPTcHggPQQ7tujX\nkFLy3NAajwKBgCDTclcg3lIp7w+Q+qW5mhn2egKoAJfn0Hl1wNl1xAy550IpIZO4\nWPYbay0nvlt8kACvMKgZbAnCqznF2kal8M93TtATW3z2uCFRoWiNIZ6j5TPGwg4S\nsPZU3zykkIppFcRPRSnB7H9oWxd4KxzHNqJqmRo1qMHN04pOAOzOQy6NAoGBAIij\nmLXUkZOy4wnpMtwlD3Uk413ZreyIA7B27gfRDARfoZ+Esn+j5uQiBqojH/D7EoCK\nnaIubDGc4Ar+9N8eHMlOzmYJhMT2RuzcwGL7ZRmzybl22vP9WsK4sF3bymkB5cXp\nxjylNuRmh8xtTYff60Yet3rBmNOsUbFN9isZFTBbAoGAHKIVnGuzwtlsfGp2IVZB\nnH2BxZwwvm87JBRlY1q4I758o/qLDsijisIXhWlCcA+CKh22+EUDdnGl/B55eirF\nGsyNUUzUSC9qNMjqsTJvpGrzN5EjH5c2uBZVVpSrfFlyRebuvnW+c9hbZpOsyH6M\nOZkmZPCqMU263AjAbWkqhaU=\n-----END PRIVATE KEY-----\n",
//         "client_email": "stackl3-dc415@appspot.gserviceaccount.com",
//         "client_id": "110820097843843183468",
//         "auth_uri": "https://accounts.google.com/o/oauth2/auth",
//         "token_uri": "https://accounts.google.com/o/oauth2/token",
//         "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
//         "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/stackl3-dc415%40appspot.gserviceaccount.com"
//     },
//     databaseURL: "https://stackl3-dc415.firebaseio.com/"
// });

var client = algoliasearch("XPW9B3D2VF", "daeee49f34f3bf3b354f1ebb8b98ca49");
//live algolia config
var algolia_index = client.initIndex("Central");
// var algolia_index = client.initIndex("index_test");

var geocodeAPIKEY = "AIzaSyD6QmI3tSbm0BxY0tfOn3lzha20-T54SBI";
var timezoneAPIKEY = "AIzaSyCawKJq_wBmQc3n_-1rMyVIc4OzietHdow";

var linkExpiryPeriod = 24 * 60 * 60 * 1000;
/**
 *  The details for payfast
 *  notify_url is just a mule call. IE user doesnt see anything only background things happen.
 *  return_url is the success page
 *  cancel_url is the fail page due to a payment cancellation
*/
var merchantID = "10011072130";
var merchantPassPhrase = "PagXJZnn&w-KK978>~Sn2_;Gtw@YLS5]";
var notify_URL = "https://stackle-live.appspot.com/notify_url/";
var return_url = "http://www.stackle.co.za/payment=success";
const cancel_url = "http://www.stackle.co.za/payment=failure";
const merchantKey = "secret";
const halfHour = 1000 * 60 * 2;


var data = {

    from: "Info @ Stackle <info@stackle.co.za>",
    to: "wesley@stackle.co.za",
    subject: "test",
    text: "some stuff"

};

var EmailTemplate_DailyLimitUpdated = {
    from: "Stackle <noreply@stackle.co.za>",
    subject: "Daily Limit Updated"
};

var EmailTemplate_DepositDepleted = {
    from: "Stackle <noreply@stackle.co.za>",
    subject: "Deposit Running Low"
};

var EmailTemplate_StoreAdded = {
    from: "Stackle <noreply@stackle.co.za>",
    subject: "Store Added"
};

var EmailTemplate_BusinessAdded = {
    subject: "Business Added",
    from: "Stackle <noreply@stackle.co.za>"
};

var EmailTemplate_WeeklyRoundUp = {
    from: "Stackle <stats@stackle.co.za>",
    subject: "Weekly Round Up"
};

var EmailTemplate_DepositReceived = {
    from: "Stackle <noreply@stackle.co.za>",
    subject: "Deposit Received"
};


// MARK: Payment Processing

app.post("/notify_url/:receipt_number", function (request, response) {

    try {

        response.status(200).send();

        var paygateID = request.body.PAYGATE_ID;
        var paygateReceipt = request.body.PAY_REQUEST_ID;
        var amount = request.body.AMOUNT;
        var currency = request.body.CURRENCY;
        var transactionStatus = request.body.TRANSACTION_STATUS;
        var resultCode = request.body.RESULT_CODE;
        var authCode = request.body.AUTH_CODE;
        var authDescription = request.body.RESULT_DESC;
        var transactionID = request.body.TRANSACTION_ID;
        var riskIndicator = request.body.RISK_INDICATOR;
        var payMethod = request.body.PAY_METHOD;
        var payMethodProvider = request.body.PAY_METHOD_DETAIL;
        var businessID = request.body.USER1;
        var userID = request.body.USER2;
        var checkSum = request.body.CHECKSUM;

        // console.log(paygateID);
        // console.log(paygateReceipt);
        // console.log(amount);
        // console.log(currency);
        // console.log(transactionStatus);
        // console.log(resultCode);
        // console.log(authCode);
        // console.log(authDescription);
        // console.log(transactionID);
        // console.log(riskIndicator);
        // console.log(payMethod);
        // console.log(payMethodProvider);
        // console.log(businessID);
        // console.log("THEIR CHECKSUM");
        // console.log(checkSum);

        var __checksum = merchantID+paygateReceipt+request.params.receipt_number+transactionStatus+resultCode+authCode+currency+amount+authDescription+transactionID+riskIndicator+payMethod+payMethodProvider+businessID+userID+merchantKey;
        __checksum = crypto.createHash("md5").update(__checksum).digest("hex");

        // console.log("OUR CHECKSUM");
        // console.log(__checksum);

        //validate the data
        if (paygateID == merchantID && __checksum == checkSum){

            amount = Number(amount)/100;

            firebase.database().ref("receipts/" +request.params.receipt_number+ "/transaction_state").once("value").then(function (snapshot) {

                //check that the snapshot exists and that we havent processed this call before..
                if (snapshot.exists() && snapshot.val() != transactionStatus){

                    //update the receipt
                    firebase.database().ref("receipts/" +request.params.receipt_number+ "/").update({
                        paygate_receipt: paygateReceipt,
                        transaction_state: transactionStatus,
                        result_code: resultCode,
                        result_description: authDescription,
                        auth_code: authCode,
                        transaction_id: transactionID,
                        risk_indicator: riskIndicator,
                        pay_method: payMethod,
                        pay_provider: payMethodProvider
                    }).then(function () {

                    }, function (error) {
                        console.log(error);
                    });

                    //update the business with the new deposit
                    if (transactionStatus == "1" && resultCode == "990017"){

                        //get the current deposit in the business and update
                        firebase.database().ref("advertising/" +businessID+ "/deposit_remaining").once("value").then(function (snapshot) {

                            var deposit = amount;

                            if (snapshot.exists()){
                                deposit += snapshot.val();
                            }

                            firebase.database().ref("advertising/" +businessID+ "/").update({
                                deposit_remaining: deposit
                            }).then(function () {

                            }, function (error) {
                                console.log(error);
                            });

                        });

                        buildEmail_DepositReceived(businessID, userID, function(email){

                            mailgun.messages().send(email, function(error, body){
                                if (error){
                                    console.log(error);
                                }
                            });

                        });

                        var storeIDS;
                        var bidwords;

                        var hasStoreIDS = false;
                        var hasBidwords = false;

                        function __updateAlgolia(){
                            if (hasStoreIDS && hasBidwords){
                                updateAlgolia(storeIDS, {
                                    "adwords": bidwords
                                }, function (error) {
                                   if (error != null){
                                       console.log("Error: updating algolia for business: " +businessID+ " receiptID: " + request.params.receipt_number);
                                   }
                                });
                            }
                        }

                        //update all possible bidwords to be active
                        //get all store ids
                        firebase.database().ref("businesses/" +businessID+ "/store_manifest").once("value").then(function (snapshot) {

                            if (snapshot.exists()){
                                hasStoreIDS = true;
                                storeIDS = Object.keys(snapshot.val());
                                __updateAlgolia();
                            }

                        });

                        //get all bidwords
                        firebase.database().ref("advertising/" +businessID+ "/adwords").once("value").then(function (snapshot) {
                            if (snapshot.exists()){
                                hasBidwords = true;
                                bidwords = Object.keys(snapshot.val());
                                __updateAlgolia();
                            }
                        });

                    }

                }

            });

        }

    } catch (error){
        console.log(error);
    }

});

app.post("/payment/deposit", function(request, response){

    try {

        var amount = request.body.amount;
        var businessID = request.body.business_id;
        var userID = request.body.user_id;
        var hashKey = request.body.hash_key;

        if (parametersInvalid([amount, hashKey, userID, businessID])){
            throw paramError;
        } else {



            var receiptNumber = ""+userID.substring(0,4)+Math.round(Date.now() / (60 * 1000 * 60));

            var user_email;

            var hasUserEmail = false;
            var hasUploadedReceipt = false;

            //Sends the payment request to paygates servers
            function sendRequest(){

                if (hasUserEmail && hasUploadedReceipt){
                    var transactionDate = moment().format("YYYY-MM-DD HH:mm:ss");
                    var checkSum = merchantID+receiptNumber+amount+"ZAR"+return_url+transactionDate+"en"+"ZAF"+user_email+notify_URL+receiptNumber+businessID+userID+merchantKey;
                    checkSum = crypto.createHash("md5").update(checkSum).digest("hex");

                    performRequest.post("https://secure.paygate.co.za/payweb3/initiate.trans", {form: {

                        PAYGATE_ID: merchantID,
                        REFERENCE: receiptNumber,
                        AMOUNT: amount+"",
                        CURRENCY: "ZAR",
                        RETURN_URL: return_url,
                        TRANSACTION_DATE: transactionDate,
                        LOCALE: "en",
                        COUNTRY: "ZAF",
                        EMAIL: user_email,
                        NOTIFY_URL: notify_URL+receiptNumber,
                        USER1: businessID,
                        USER2: userID,
                        CHECKSUM: checkSum

                    }}, function (error, res, body) {

                        var __res = body.split("&");
                        // check the data for being the same...
                        if (__res.indexOf("PAYGATE_ID="+merchantID) >= 0 && __res.indexOf("REFERENCE="+receiptNumber.replace(" ", "+")) >= 0){

                            var paygateReceipt = __res[1].replace("PAY_REQUEST_ID=", "");
                            var checkSum = merchantID+""+paygateReceipt+""+receiptNumber+""+merchantKey;
                            checkSum = crypto.createHash("md5").update(checkSum).digest("hex");

                            if (checkSum == __res[3].replace("CHECKSUM=", "")){
                                response.status(200).send({
                                    code: 200,
                                    state: "success",
                                    redirect_ticket: paygateReceipt,
                                    checkSum: checkSum,
                                    key: hashKey
                                });
                            }
                        }

                    });
                }
            }

            //uploads the receipt to our server
            firebase.database().ref("receipts/" +receiptNumber+ "/").update({
                user_id: userID,
                date_initiated: Date.now(),
                business_id: businessID,
                amount: amount,
                country: "ZAF",
                transaction_state: 0
            }).then(function () {

                hasUploadedReceipt = true;
                sendRequest();

            }, function (error) {
                console.log(error);
                response.status(500).send(failure500);
            });

            //retrieves the users email address
            firebase.database().ref("users/" + userID+ "/details/email").once("value").then(function (snapshot) {

                if (snapshot.exists()){
                    user_email = snapshot.val();
                    hasUserEmail = true;
                    sendRequest();
                } else {
                    response.status(500).send(failure500);
                }

            });

        }

    } catch (error) {
        console.log(error);
        response.status(403).send(failure403);
    }
});

// MARK: Website & HTML Content

app.get("/member/confirmation/:confirmation_id/:business_id", function(request, response){

    var confirmationID = request.params.confirmation_id;
    var businessID = request.params.business_id;

    firebase.database().ref("confirmation_links/add_member/" +businessID+ "_" +confirmationID+ "/" +confirmationID).once("value").then(function (_snapshot) {

        if (_snapshot.exists()){

            // console.log("found snapshot");

            if (Date.now() <= _snapshot.val()["expires"]){

                // console.log("snapshot is valid..");

                var timer = setTimeout(function breakExecution() {

                    response.status(404).render("webUpdate/failed.ejs", {failReason: "Invitation does not exist"});

                }, 12500);


                // console.log("\n\n"+_snapshot.val());
                // console.log(_snapshot.val()["user_email"]);

                firebase.database().ref("users_lookup/").orderByValue().equalTo(_snapshot.val()["user_email"]).on("child_added", function (snapshot) {

                    clearTimeout(timer);
                    // console.log("\n\nHERE\n\n");

                    // console.log(snapshot.key);
                    // console.log("snapshot found...");

                    // user exists
                    if (snapshot.exists()){

                        var userID = snapshot.key;
                        //user is not in a business
                        userMayAddBusiness(userID, function (mayAdd) {

                            if (mayAdd){

                                var userRole = _snapshot.val()["user_role"];

                                // console.log(userRole);
                                // console.log(userRole == "Member");

                                if (userRole == "Admin" || userRole == "Member"){

                                    addUserToBusinessRole(businessID, snapshot.key, userRole, function (error) {
                                        if (error === null){
                                            response.status(200).render("webUpdate/Success.html");
                                            firebase.database().ref("confirmation_links/add_member/" +businessID+ "_" +confirmationID).remove();
                                        } else {
                                            response.status(500).render("webUpdate/failed.ejs", {failReason: "An unknown error occurred. Please try clicking the confirmation link. If this issue does not go away please get in contact with us."});
                                        }
                                    });

                                } else {

                                    // console.log(_snapshot.val()["store_id"]);

                                    addUserToStoreRole(businessID, _snapshot.val()["store_id"], snapshot.key, userRole, function (error) {
                                        if (error === null){

                                            // console.log("\n\nSUCCESS\n\n");

                                            response.status(200).render("webUpdate/Success.html");
                                            firebase.database().ref("confirmation_links/add_member/" +businessID+ "_" +confirmationID).remove();
                                        } else {

                                            // console.log("\n\nERROR");
                                            response.status(500).render("webUpdate/failed.ejs", {failReason: "An unknown error occurred. Please try clicking the confirmation link. If this issue does not go away please get in contact with us."});
                                        }
                                    });

                                }

                            } else {
                                response.status(400).send(businessLimitReachedError);
                            }

                        });

                    } else {
                        response.status(404).render("webUpdate/failed.ejs", {failReason: "Invitation does not exist."});;
                    }


                });

            } else {
                response.status(400).render("webUpdate/failed.ejs", {failReason: "This link has expired. Please try requesting a new invitation from whoever invited you."});
                firebase.database().ref("confirmation_links/add_member/" +businessID+ "_" +confirmationID).remove();
            }

        } else {
            response.status(400).render("webUpdate/failed.ejs", {failReason: "Invitation does not exist."});
        }

    });

});

app.get("/remove/:businessID/:storeID/:removeID/:key", function (request, response) {
    try {

        var business_id = request.params.businessID;
        var store_id = request.params.storeID;
        var remove_id = request.params.removeID;
        var key = request.params.key;
        

        if (parametersInvalid([business_id, store_id, remove_id])){
            throw paramError;
        } else {

            var algolia_updated = false;
            var store_removed = false;
            var link_removed = false;
            var final_remove_request = false;

            function sendResponse() {
                if (final_remove_request && link_removed && algolia_updated && store_removed) {
                    response.status(200).render("webUpdate/Success.html");
                }
            }

            function removeStore(){

                deleteStore(business_id, store_id, "", function (error) {
                    if (error){
                        response.status(500).render("webUpdate/failed.ejs", {failReason: "Unknown error occurred please try again. If this issue does not get resolved please contact us."});
                    } else {
                        store_removed = true;
                        sendResponse();
                    }
                });

                // request the admin app to remove all store images
                firebase.database().ref("remove_store/" +store_id).update({
                    "business_id": business_id,
                    "store_id": store_id
                });

                algolia_index.deleteObject(store_id, function (error, content) {
                    if (error){
                        console.log(error);
                        response.status(500).render("webUpdate/failed.ejs", {failReason: "Unknown error occurred please try again. If this issue does not get resolved please contact us."});
                    } else {
                        algolia_updated = true;
                        sendResponse();
                    }
                });

                //remove link
                firebase.database().ref("confirmation_links/remove_store/" +store_id).remove().then(function () {

                    link_removed = true;
                    sendResponse();

                }, function (error) {

                    console.log(error);
                    response.status(500).render("webUpdate/failed.ejs", {failReason: "Unknown error occurred please try again. If this issue does not get resolved please contact us."});

                });
            }

            ///determine if this is the server attempting to delete the store or an admin
            if (key == internalKey) {

                removeStore();

                /// else validate user request
            } else {
                firebase.database().ref("confirmation_links/remove_store/" +store_id).once("value").then(function (removeIDSnapshot) {
                    if (removeIDSnapshot.exists() && removeIDSnapshot.val() == remove_id) {

                        if (Date.now() <= removeIDSnapshot.val()){

                            firebase.database().ref("permanent_delete/stores/").update(function(){
                                var obj = {};
                                obj[business_id+store_id] = {
                                    business_id: business_id,
                                    store_id: store_id
                                };
                                return obj;
                            }()).then(function(){

                                final_remove_request = true;
                                sendResponse();

                            }, function (error){
                                console.log(error);
                                response.status(500).render("webUpdate/failed.ejs", {failReason: "Unknown error occurred please try again. If this issue does not get resolved please contact us."});
                            });

                            removeStore()

                        } else {
                            firebase.database().ref("confirmation_links/remove_store/" +store_id).remove().then(function () {
                                response.status(404).render("webUpdate/failed.ejs", {failReason: "Request does not exist."});
                            });
                        }


                    } else {
                        response.status(404).render("webUpdate/failed.ejs", {failReason: "Request does not exist."});
                    }
                });

            }
        }

    } catch (e) {
        console.log(e);
        response.status(400).render("webUpdate/failed.ejs", {failReason: "Bad request."});
    }
});

app.get("/:remove_id/:businessID/36", function(request, response){

    var key = request.params.remove_id;
    var business_id = request.params.businessID;

    function storeDelete() {
        firebase.database().ref("businesses/" +business_id+ "/store_manifest").once("value").then(function(snapshot){

            if (snapshot.exists()){

                var storeIDS = Object.keys(snapshot.val());
                var storesToUpdate = storeIDS.length;

                function beginBusinessDelete() {
                    if (storesToUpdate == 0){
                        deleteBusiness(business_id, "", function (error) {

                            error ? response.status(404).render("webUpdate/failed.ejs", {failReason: "Request does not exist."}) : response.status(200).render("webUpdate/Success.html");

                        });

                        // orphan delete request - handled by server
                        firebase.database().ref("permanent_delete/businesses/").update(function(){

                            var obj = {};
                            obj[business_id] = Date.now();
                            return obj;

                        }());

                        // remove images request - handled by admin app
                        firebase.database().ref("remove_businesses/" +business_id).update({
                            "business_id": business_id
                        });

                    }
                }

                storeIDS.forEach(function (store_id) {

                    var membersToUpdate = -1;
                    var algolia_updated = false;

                    function storeDeleted (){
                        if (membersToUpdate == 0 && algolia_updated){
                            storesToUpdate -- ;
                            beginBusinessDelete();
                        }
                    }

                    algolia_index.deleteObject(store_id, function (error, content) {
                        if (error){
                            console.log(error);
                            callback(error);
                        } else {
                            algolia_updated = true;
                            beginBusinessDelete()
                        }
                    });

                    // Get all members for that particular store
                    firebase.database().ref("businesses/" +business_id+ "/stores/" +store_id+ "/members").once("value").then(function (snapshot) {

                        if (snapshot.exists()) {

                            var member_ids = Object.keys(snapshot.val());
                            membersToUpdate = member_ids.length;

                            //cycle through all the members and update. Go to the user -> business manifest and remove that storeID
                            member_ids.forEach(function (member_id) {
                                //updates the user -> business manifest
                                firebase.database().ref("users/" + member_id + "/businesses/" + business_id + "/" +
                                    "" + store_id).remove().then(function () {

                                    membersToUpdate--;
                                    storeDeleted();

                                }, function (error) {
                                    console.log(error);
                                    callback(error);

                                });

                            });

                        } else {
                            membersToUpdate = 0;
                            storeDeleted();
                        }

                    });

                });

            } else {
                callback(error);
            }

        });
    }

    if (key == internalKey){

        storeDelete();

    } else {
        firebase.database().ref("confirmation_links/remove_business/" +business_id).once("value").then(function (snapshot) {
            if (snapshot.exists()){
                storeDelete();
            } else {
                response.status(404).render("webUpdate/failed.ejs", {failReason: "Request does not exist."})
            }
        });
    }

});

// MARK: Complete Algolia Query

app.post("/store_query", function (request, response) {

    try {

        var pageNumber = request.body.page_number;
        var data = request.body["data"];
        var userID = request.body["user_id"];

        if (parametersInvalid([data, userID, pageNumber]) || !Array.isArray(data)){
            throw paramError;
        } else {

            var responseObj = [];
            var toBuild = data.length;
            var sortObj = [];

            function sendResponse() {
                if (toBuild == 0){

                    responseObj = sortObj.slice();

                    sortObj.sort(function (a, b) {
                       return parseFloat(b.ad_value) - parseFloat(a.ad_value);
                    });

                    if (sortObj.length == 1){

                        sortObj[0].ad_value = 0.5;

                    } else {
                        for (var i = 0; i < sortObj.length - 1; i++){
                            if (sortObj[i+1].ad_value != 0){
                                sortObj[i].ad_value = sortObj[i+1].ad_value + 0.5;
                            } else if (sortObj[i].ad_value != 0){
                                sortObj[i].ad_value = sortObj[i].ad_value * 0.5 + 0.5;
                            }
                        }
                    }

                    var tempObj = sortObj;
                    tempObj["code"] = 200;

                    response.status(200).send(tempObj);

                    // save statistic related data

                    sortObj.forEach(function (obj) {

                        // is there any data to save?

                        var currentWeek = getWeek();

                        if (obj.adwords.length > 0) {

                            obj.adwords.forEach(function (adword) {
                                firebase.database().ref("advertising/" +obj.business_id+ "/before_sort/" +currentWeek + "/" +adword).once("value").then(function (snapshot) {

                                    var start;

                                    for(var n = 0; n < responseObj.length; n++){
                                        if (responseObj[n].business_id == obj.business_id){
                                            start = n + 1 + (pageNumber * 10);
                                            break;
                                        }
                                    }

                                    if (snapshot.exists()){
                                        start += snapshot.val();
                                    }

                                    firebase.database().ref("advertising/" +obj.business_id+ "/before_sort/" +currentWeek + "/").update(function(){
                                        var obj = {};
                                        obj[adword] = start;
                                        return obj;

                                    }());

                                });


                                firebase.database().ref("advertising/" +obj.business_id+ "/after_sort/" +currentWeek + "/" +adword).once("value").then(function (snapshot) {

                                    var end;

                                    for (var n = 0; n < sortObj.length; n++){
                                        if(sortObj[n].business_id == obj.business_id){
                                            end = n + 1 + (pageNumber * 10);
                                            break;
                                        }
                                    }

                                    if (snapshot.exists()){
                                        end += snapshot.val();
                                    }

                                    firebase.database().ref("advertising/" +obj.business_id+ "/after_sort/" +currentWeek + "/").update(function(){
                                        var obj = {};
                                    obj[adword] = end;
                                    return obj;
                                    }());

                                });


                                firebase.database().ref("advertising/" +obj.business_id+ "/adword_searches/" +currentWeek + "/" +adword).once("value").then(function (snapshot) {

                                    var searches = 1;

                                    if (snapshot.exists()){
                                        searches += snapshot.val();
                                    }

                                    firebase.database().ref("advertising/" +obj.business_id+ "/adword_searches/" +currentWeek + "/").update(function(){

                                        var obj = {};
                                        obj[adword] = searches;
                                        return obj;

                                    }());

                                });
                            });
                        }

                    });
                }
            }

            data.forEach(function (datum) {

                var businessID = datum["business_id"];
                var storeID = datum["store_id"];
                var adwords = datum["adwords"];

                var description_downloaded = false;
                var numberOfReviews_downloaded = false;
                var likes_downloaded = false;
                var images_downloaded = false;
                var has_types_downloaded = false;
                var rating_downloaded = false;
                var is_liked_downloaded = false;
                var home_image_downloaded = false;
                var logo_downloaded = false;
                var time_table_completed = false;
                var advalue_downloaded = false;

                var has_offset = false;
                var has_holiday_hours = false;
                var has_standard_hours = false;

                var description;
                var numberOfReviews;
                var images = 0;
                var hasTypes = {
                    "Products": false,
                    "Services": false,
                    "Accommodation": false
                };
                var rating;
                var likes;
                var is_liked = false;
                var home_image;
                var logo;
                var is_open = false;
                var next_significant_time;

                var utc_offset = 0;
                var adValue = 0;
                var holiday_hours = {};
                var standard_hours = {};
                var timeTable = {};

                function buildObject(){
                    // console.log("home image: " + home_image_downloaded);
                    // console.log("logo: " + logo_downloaded);
                    // console.log("description: " + description_downloaded);
                    // console.log("number of reviews: " + numberOfReviews_downloaded);
                    // console.log("images: " + images_downloaded);
                    // console.log("has types: " + has_types_downloaded);
                    // console.log("rating: " + rating_downloaded);
                    // console.log("likes: " + likes_downloaded);
                    // console.log("is liked: " + is_liked_downloaded);
                    // console.log("time table: " + time_table_completed);

                    if (home_image_downloaded && logo_downloaded && description_downloaded && numberOfReviews_downloaded
                        && images_downloaded && has_types_downloaded && rating_downloaded && likes_downloaded
                        && is_liked_downloaded && time_table_completed && advalue_downloaded){
                    // if (home_image_downloaded && logo_downloaded && description_downloaded && numberOfReviews_downloaded
                    //         && images_downloaded && has_types_downloaded && rating_downloaded && likes_downloaded
                    //         && is_liked_downloaded && time_table_completed){

                        var obj = {
                            business_id: businessID,
                            store_id: storeID,
                            description: description,
                            numberOfReviews: numberOfReviews,
                            images: images,
                            hasTypes: hasTypes,
                            rating: rating,
                            likes: likes,
                            is_liked: is_liked,
                            home_image: home_image,
                            logo: logo,
                            is_open: is_open,
                            next_significant_time: next_significant_time,
                            ad_value: adValue,
                            adwords: adwords
                        };

                        // var obj = {
                        //     business_id: businessID,
                        //     store_id: storeID,
                        //     description: description,
                        //     numberOfReviews: numberOfReviews,
                        //     images: images,
                        //     hasTypes: hasTypes,
                        //     rating: rating,
                        //     likes: likes,
                        //     is_liked: is_liked,
                        //     home_image: home_image,
                        //     logo: logo,
                        //     is_open: is_open,
                        //     next_significant_time: next_significant_time
                        // };

                        // console.log("finished with object... pushing into sorter..");
                        sortObj.push(obj);
                        toBuild--;
                        sendResponse();

                    }
                }

                // build timetable for that week
                function buildTimeTable() {
                    if(has_offset && has_holiday_hours && has_standard_hours){



                        //  -   Get the interval at that stores location
                        var todayMinutes = new Date().getUTCMinutes();
                        var todayHours = Number(new Date().getUTCHours()) + Number(utc_offset);
                        // Now get the date at that store using the offset hours
                        var todayDate = getDayAtStore(todayHours);
                        var todayMonth = new Date().getUTCMonth();
                        var todayYear = new Date().getUTCFullYear();
                        // Lastly get the interval for that stores location
                        var todayInterval = new Date(Date.UTC(todayYear, todayMonth, todayDate, todayHours, todayMinutes));

                        // console.log(todayMinutes + " Minutes");
                        // console.log(todayHours + " Hours");
                        // console.log(todayDate + " Date");
                        // console.log(todayMonth + " Month");
                        // console.log(todayYear + " Year");
                        // console.log(todayInterval.valueOf() + " SUM");
                        // Given the holiday hours we have.

                        // Are we inside any of the holiday intervals?
                        Object.keys(holiday_hours).forEach(function (holidayKey) {

                            // Get all the start components
                            var startDateComponents = holiday_hours[holidayKey]["start_date"].split(" ");
                            var startTimeComponents = holiday_hours[holidayKey]["open_hour"];

                            var startYear = startDateComponents[2];
                            var startMonth = getMonthIntegerForString(startDateComponents[0]);
                            var startDate = startDateComponents[1].split(",")[0];
                            var startHour = getHoursFromString(startTimeComponents);
                            var startMinutes = getMinutesFromString(startTimeComponents);

                            // Get all the end components
                            var endDateComponents = holiday_hours[holidayKey]["end_date"].split(" ");
                            var endTimeComponents = holiday_hours[holidayKey]["close_hour"];

                            var endYear = endDateComponents[2];
                            var endMonth = getMonthIntegerForString(endDateComponents[0]);
                            var endDate = endDateComponents[1].split(",")[0];
                            var endHour = getHoursFromString(endTimeComponents);
                            var endMinutes = getMinutesFromString(endTimeComponents);

                            // console.log("YEAR: " + endYear);
                            // console.log("Month: " + endMonth);
                            // console.log("Date: " + endDate);
                            // console.log("Hour: " + endHour);
                            // console.log("Minutes: " + endMinutes);

                            // Build up each interval
                            var startInterval = new Date(Date.UTC(startYear, startMonth, startDate, startHour, startMinutes, 0, 0));
                            var endInterval = new Date(Date.UTC(endYear, endMonth, endDate, endHour, endMinutes, 0, 0));

                            var todayIntervalValue = todayInterval;

                            for (var i = 0; i < 8; i++){

                                todayIntervalValue = todayIntervalValue.valueOf() + (i * 24 * 60 * 60 * 1000);

                                // Determine if we are currently inside this interval or not.
                                if (todayIntervalValue >= startInterval.valueOf() && todayIntervalValue <= endInterval.valueOf()){

                                    var dayInt = todayInterval.getUTCDay() + i;

                                    if (dayInt >= 7){
                                        dayInt -= 7;
                                    }

                                    var todayText = getDayTextFromInt(dayInt);

                                    if (i == 7){
                                        timeTable["next_"+todayText + "_open"] = startTimeComponents;
                                        timeTable["next_"+todayText + "_close"] = endTimeComponents;
                                        timeTable["holiday_end"] = holiday_hours[holidayKey]["end_date"];
                                    } else {
                                        // Add this holidays open / close time to the timetable
                                        timeTable[todayText + "_open"] = startTimeComponents;
                                        timeTable[todayText + "_close"] = endTimeComponents;
                                    }

                                }

                            }

                        });

                        // fill in any blank slots there might be with standard hours
                        for (var i = 0; i < 7; i++){
                            if (timeTable[getDayTextFromInt(i) + "_open"] === undefined ){
                                timeTable[getDayTextFromInt(i) + "_open"] = standard_hours[getDayTextFromInt(i) + "_open"];
                                timeTable[getDayTextFromInt(i) + "_close"] = standard_hours[getDayTextFromInt(i) + "_close"];
                            }
                        }

                        var todayTimeInterval = todayInterval.getUTCHours() * 60 + todayInterval.getUTCMinutes();
                        // var todayTimeInterval = 1000;
                        console.log(todayInterval.getUTCHours() + " UTC HOURS");

                        for (var i = 0; i < 8; i++){
                            var todayInt = todayInterval.getUTCDay() + i;
                            if (todayInt > 6){
                                todayInt -= 7;
                            }

                            // console.log("Loop # " + i);

                            if(timeTable[getDayTextFromInt(todayInt) + "_open"] !== timeTable[getDayTextFromInt(todayInt) + "_close"]){

                                var todayOpen = getHoursFromString(timeTable[getDayTextFromInt(todayInt) + "_open"]) * 60 + getMinutesFromString(timeTable[getDayTextFromInt(todayInt) + "_open"]);
                                var todayClose = getHoursFromString(timeTable[getDayTextFromInt(todayInt) + "_close"]) * 60 + getMinutesFromString(timeTable[getDayTextFromInt(todayInt) + "_close"]);

                                // Reverse the flow if needed
                                if (todayClose < todayOpen){
                                    todayInt += 1;

                                    if (todayInt > 6){
                                        todayInt -= 7;
                                    }

                                    todayClose = getHoursFromString(timeTable[getDayTextFromInt(todayInt) + "_close"]) * 60 + getMinutesFromString(timeTable[getDayTextFromInt(todayInt) + "_close"]) + 1440;
                                }

                                // console.log(todayOpen + " TODAY OPEN");
                                // console.log(todayClose + "TODAY CLOSE");
                                // console.log(todayTimeInterval + " TODAY TIME INTERVAL");

                                // If the current time is between open and close, the shop is open.
                                if (todayTimeInterval >= todayOpen && todayTimeInterval <= todayClose && i == 0){
                                    // console.log("option 1");
                                    // response.status(200).send("The store is open until: " + timeTable[getDayTextFromInt(todayInt) + "_close"]);

                                    is_open = true;
                                    next_significant_time = timeTable[getDayTextFromInt(todayInt) + "_close"];
                                    time_table_completed = true;
                                    buildObject();

                                    break;
                                    // However if the current time is less than the open time and we are still on todays position in the timetable then the store will be open later
                                } else if (todayTimeInterval < todayOpen && i == 0){
                                    // console.log("option 2");
                                    // response.status(200).send("The store will be open at: " + timeTable[getDayTextFromInt(todayInt) + "_open"]);
                                    next_significant_time = timeTable[getDayTextFromInt(todayInt) + "_open"];
                                    is_open = false;
                                    time_table_completed = true;
                                    buildObject();

                                    break;
                                    //Otherwise if the current time is less than open time and we are on the next day in the time table the store will be open tomorrow.
                                } else if (todayTimeInterval < todayOpen && i == 1) {
                                    // console.log("option 3");
                                    // response.status(200).send("The store will be open at: " + timeTable[getDayTextFromInt(todayInt) + "_open"] + " tomorrow.");

                                    next_significant_time = timeTable[getDayTextFromInt(todayInt) + "_open"];
                                    is_open = false;
                                    time_table_completed = true;
                                    buildObject();

                                    break;
                                } else if (todayTimeInterval < todayOpen) {
                                    // console.log("option 4");
                                    // response.status(200).send("The store will be open on: " +getDayTextFromInt(todayInt)+" at "+ timeTable[getDayTextFromInt(todayInt) + "_open"]);

                                    next_significant_time = getDayTextFromInt(todayInt)+ " at " + timeTable[getDayTextFromInt(todayInt) + "_open"];
                                    is_open = false;
                                    time_table_completed = true;
                                    buildObject();

                                    break;
                                } else {
                                    todayTimeInterval = 0;
                                }

                            } else if (i == 0){
                                todayTimeInterval = 0;
                            } else if (i == 7 && timeTable["next_"+getDayTextFromInt(todayInt) + "_open"] !== undefined){
                                // console.log("The store is on holiday and will be open again on: " + timeTable["holiday_end"]);
                                // response.status(200).send("The store is on holiday and will be open again on: " + timeTable["holiday_end"]);

                                next_significant_time = timeTable["holiday_end"];
                                is_open = false;
                                time_table_completed = true;
                                buildResponse();

                            }

                            if (i == 7){
                                // response.status(200).send({holidayOBJ: holiday_hours, standardHoursOBJ: standard_hours, timeTableOBJ: timeTable});
                                // response.status(200).send("Store is closed indefinitely");
                                next_significant_time = "Indefinitely Closed.";
                                is_open = false;
                                time_table_completed = true;
                                buildResponse();

                            }

                        }
                    }
                }

                //get logo
                firebase.database().ref("businesses/" +businessID+ "/logo").once("value").then(function (snapshot) {
                    if(snapshot.exists()){
                        logo = snapshot.val();
                    } else {
                        logo = "placeholder.png";
                    }

                    logo_downloaded = true;
                    buildObject();

                });

                //get homeimage
                firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/home_image").once("value").then(function (snapshot) {
                    if(snapshot.exists()){
                        home_image = snapshot.val();
                    } else {
                        home_image = "placeholder.png";
                    }

                    home_image_downloaded = true;
                    buildObject();

                });

                //get likes
                firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/likes").once("value").then(function (snapshot) {
                    if(snapshot.exists()){
                        likes = snapshot.val();
                    } else {
                        likes = 0;
                    }

                    likes_downloaded = true;
                    buildObject();

                });

                //get ratings
                firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/rating").once("value").then(function (snapshot) {
                    if (snapshot.exists()){
                        var num = snapshot.val()["numerator"];
                        var denom = snapshot.val()["denominator"];
                        rating = Math.round((num / denom) * 50) / 10;
                        rating_downloaded = true;
                        buildObject();
                    } else {
                        rating = 3;
                    }

                });

                //get has_types
                firebase.database().ref("businesses/" +businessID+ "/has_types/").once("value").then(function(snapshot){

                    if (snapshot.exists()){

                        if (snapshot.val()["Products"] !== undefined) {
                            hasTypes["Products"] = snapshot.val()["Products"];
                        }

                        if (snapshot.val()["Services"] !== undefined) {
                            hasTypes["Services"] = snapshot.val()["Services"];
                        }

                        if (snapshot.val()["Accommodation"] !== undefined) {
                            hasTypes["Accommodation"] = snapshot.val()["Accommodation"];
                        }

                    }

                    has_types_downloaded = true;
                    buildObject();

                });

                //get description
                firebase.database().ref("businesses/" +businessID+ "/description").once("value").then(function (snapshot) {
                    if (snapshot.exists()){
                        description = snapshot.val();
                    } else {
                        description = "";
                    }

                    description_downloaded = true;
                    buildObject();

                });

                //get number of reviews
                firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/review_count").once("value" +
                    "").then(function (snapshot) {

                    if (snapshot.exists()){
                        numberOfReviews = snapshot.val();
                    } else {
                        numberOfReviews = 0;
                    }

                    numberOfReviews_downloaded = true;
                    buildObject();

                });

                //get image count
                firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/images_official").once("value" +
                    "").then(function (snapshot) {

                    if(snapshot.exists()){
                        images = snapshot.val();
                    } else {
                        images = 0;
                    }

                    images_downloaded = true;
                    buildObject();

                });

                //get is liked
                firebase.database().ref("likes_registry/businesses/" +businessID+ "/stores/" +storeID+ "/likes/" +userID).once("value").then(function (snapshot) {

                    if(snapshot.exists()){
                        is_liked = true;
                    }

                    is_liked_downloaded = true;
                    buildObject();
                });

                // get standard hours
                firebase.database().ref("businesses/" +businessID+ "/business_hours/standard").once("value").then(function (snapshot) {

                    if (snapshot.exists()){

                        standard_hours = snapshot.val();
                        has_standard_hours = true;
                        buildTimeTable();

                    } else {

                        time_table_completed = true;
                        next_significant_time = "--";
                        buildObject();

                    }

                });

                // get holiday hours
                firebase.database().ref("businesses/" +businessID+ "/business_hours/holiday").once("value").then(function (snapshot) {

                    if (snapshot.exists()) {
                        holiday_hours = snapshot.val();
                    }

                    has_holiday_hours = true;
                    buildTimeTable();
                });

                // get offset
                firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/utc_offset").once("value").then(function (snapshot) {
                    if (snapshot.exists()){
                        utc_offset = snapshot.val();
                    }

                    has_offset = true;
                    buildTimeTable();

                });

                //get adword value
                if (Array.isArray(adwords) && adwords.length != 0){

                    var toDo = adwords.length;
                    var depositRemaining = 0;
                    var remainingSpend = 0;
                    var _adValue = 0;

                    function setAdwordsComplete() {
                        if (toDo == 0) {

                            if (_adValue <= remainingSpend && remainingSpend <= depositRemaining){
                                adValue = _adValue;
                            }

                            console.log("found advalue of: " +adValue);
                            advalue_downloaded = true;
                            buildObject();
                        }
                    }

                    firebase.database().ref("advertising/" +businessID+ "/deposit_remaining").once("value").then(function (snapshot) {

                        if (snapshot.exists()){
                            depositRemaining = snapshot.val();
                        }

                    });

                    firebase.database().ref("daily_budget/" +businessID).once("value").then(function (snapshot) {

                        if (snapshot.exists()){
                            remainingSpend = snapshot.val();
                        }

                    });

                    adwords.forEach(function (adword) {

                        firebase.database().ref("advertising/" +businessID+ "/adwords/" +adword).once("value").then(function (snapshot) {

                            if (snapshot.exists()){
                                _adValue += snapshot.val();
                            }

                            toDo -= 1;
                            setAdwordsComplete();

                        });
                    });

                } else {
                    advalue_downloaded = true;
                    buildObject();
                }
            });

            // Convenience Functions
            //  -   Mostly used with determining business hours
            // returns the integer of the day in a week ie 0 for sunday, 1 for monday etc
            function getDayAtStore(hoursAtLocation) {
                var today = new Date().getUTCDate();

                // is store ahead by one day
                if (hoursAtLocation >= 24){

                    today += 1;
                    return today;
                    //is store behind by one day
                } else if (hoursAtLocation < 0){

                    today -= 1;
                    return today;

                } else {
                    return today;
                }
            }

            // given a shortened month text expression determine its corresponding integer value
            function getMonthIntegerForString(monthText) {
                var obj = {
                    "Jan": 0,
                    "Feb": 1,
                    "Mar": 2,
                    "Apr": 3,
                    "May": 4,
                    "Jun": 5,
                    "Jul": 6,
                    "Aug": 7,
                    "Sep": 8,
                    "Oct": 9,
                    "Nov": 10,
                    "Dec": 11
                };

                return obj[monthText];

            }

            // Given the full the time description ie 2:30 PM extract the 24 clock hours
            function getHoursFromString(hoursData) {

                var components = hoursData.split(" ");
                var hours = Number(components[0].split(":")[0]);

                if (components[1] == "PM" && hours != 12){
                    hours += 12;
                } else if (components[0] == "AM" && hours == 12){
                    hours -= 12;
                }

                return hours;
            }

            // Given the full time description ie 2:30 PM extract the minutes
            function getMinutesFromString(minutesData) {
                var components = minutesData.split(" ");
                var minutes = Number(components[0].split(":")[1]);

                return minutes;
            }

            function getDayTextFromInt(dayInt) {

                var obj = {
                    0: "sunday",
                    1: "monday",
                    2: "tuesday",
                    3: "wednesday",
                    4: "thursday",
                    5: "friday",
                    6: "saturday"
                };

                return obj[dayInt];

            }
        }

    } catch (error) {
        console.log(error);
        response.status(403).send(paramError);
    }

});

// MARK: Adding Data - Start up phase

/**
 * Following posts only used by admin for self adding of businesses.
 *
 */

app.post("/unowned_business/add_store", function(request, response){

    try {

        var business_id = request.body["business_id"];
        var store_phone = request.body["store_phone"];
        var store_email = request.body["store_email"];
        var storeImageURL = request.body.store_image_url;
        var lat = request.body["lat"];
        var long = request.body["long"];
        var store_id = request.body["new_store_id"];
        var title = "Store";
        var location_name = request.body["location_name"];
        var business_name = request.body["business_name"];
        var currentStoreID = request.body.current_store_id;

        if(parametersInvalid([currentStoreID, business_id, storeImageURL, store_phone, store_email, lat, long, store_id, location_name,
                business_name])){

            throw paramError;

        } else {

            var business_updated = false;
            var manifest_updated = false;
            var algolia_updated = false;

            var completed_timezone_query = false;
            var completed_geocode_query = false;

            var uct_offset;
            var geo_address;

            function sendResponse(){
                if(business_updated && manifest_updated && algolia_updated){
                    response.status(200).send(success200);

                }
            }

            var geocodeURL = "https://maps.googleapis.com/maps/api/geocode/json?" +
                "latlng="+lat+","+long+
                "&key=" + geocodeAPIKEY;
            var timezoneURL = "https://maps.googleapis.com/maps/api/timezone/json?" +
                "location="+lat+","+long+"&timestamp=1331161200&key=" +timezoneAPIKEY;

            performRequest(geocodeURL, function (error, res, body) {
                if (!error && res.statusCode == 200){

                    var body = JSON.parse(body);
                    geo_address = body.results[2].formatted_address;
                    completed_geocode_query = true;
                    continueRequest();

                } else {
                    console.log(error);
                    response.status(500).send(failure500);
                }
            });

            performRequest(timezoneURL, function (error, res, body) {
                if (!error && res.statusCode == 200){

                    var body = JSON.parse(body);
                    uct_offset = Number(body.rawOffset) / 3600;
                    completed_timezone_query = true;
                    continueRequest();

                } else {
                    console.log(error);
                    response.status(500).send(failure500);
                }
            });

            function continueRequest() {
                if(completed_geocode_query && completed_timezone_query){
                    // Build new store item for algolia
                    // Get existing items entry and copy it into the new store.
                    algolia_index.getObject(currentStoreID, ["items", "category"], function (error, content) {
                        if (error){
                            response.status(500).send("Internal Error");
                        } else {

                            var items = content["items"];
                            var category = content["category"];

                            algoliaAddStore(business_name, store_id, business_id, location_name, geo_address, lat, long, items, category,
                                function (error, updated) {

                                    if (updated){
                                        algolia_updated = true;
                                        sendResponse();
                                    }
                                });

                        }
                    });

                    // add new store to store manifest
                    firebase.database().ref("businesses/" +business_id+ "/store_manifest/").update(function () {

                        var _obj = {};

                        if (location_name !== ""){
                            _obj[store_id] = location_name + ", " + geo_address;
                        } else {
                            _obj[store_id] = geo_address;
                        }

                        return _obj;

                    }()).then(function () {
                        manifest_updated = true;
                        sendResponse();
                    }, function (error) {
                        console.log(error);
                        response.status(500).send(failure500);
                    });

                    // Get the timezone and location name for that store

                    // create the store object
                    firebase.database().ref("businesses/" + business_id + "/stores/" + store_id+ "/").update(buildStore(storeImageURL, lat, long, store_email, store_phone, title, location_name, geo_address, uct_offset)).then(function () {

                        business_updated = true;
                        sendResponse();

                    }, function (error) {
                        console.log(error);
                        response.status(500).send(failure500);
                    });
                }
            }

        }

    } catch (e) {
        console.log(e);
        response.status(403).send(failure403);
    }

});

app.post("/unowned_business/add_member", function(request, response){

    try {
        var email = request.body.email_address;
        var businessID = request.body.business_id;

        if (parametersInvalid([email])){
            throw paramError;
        } else {

            firebase.database().ref("unowned_businesses/"+businessID+ "/members").update(function(){

                var obj = {};
                obj[Date.now()] = email;
                return obj;

            }()).then(function () {

                response.status(200).send(success200);

            }, function (error) {

                response.status(500).send(error);

            });
        }

    } catch (e){
        console.log(e);
        response.status(403).send(failure403);
    }

});

app.post("/add_business_no_owner", function(request, response){

    try {
        var name = request.body["business_name"];
        var logo = request.body["logo"];
        var lat = request.body["lat"];
        var long = request.body["long"];
        var business_id = request.body["business_id"];
        var store_id = request.body["store_id"];
        var home_image = request.body["store_image"];
        var location_name = request.body["location_name"];

        var monday_open = request.body["monday_open"];
        var tuesday_open = request.body["tuesday_open"];
        var wednesday_open = request.body["wednesday_open"];
        var thursday_open = request.body["thursday_open"];
        var friday_open = request.body["friday_open"];
        var saturday_open = request.body["saturday_open"];
        var sunday_open = request.body["sunday_open"];

        var monday_close = request.body["monday_close"];
        var tuesday_close = request.body["tuesday_close"];
        var wednesday_close = request.body["wednesday_close"];
        var thursday_close = request.body["thursday_close"];
        var friday_close = request.body["friday_close"];
        var saturday_close = request.body["saturday_close"];
        var sunday_close = request.body["sunday_close"];

        var store_email = request.body["store_email"];
        var store_phone = request.body["store_phone"];

        if (parametersInvalid([name, logo, lat, long, business_id, store_id, store_email, store_phone, home_image,
                monday_open, monday_close, tuesday_open, tuesday_close, wednesday_open, wednesday_close, thursday_open,
                thursday_close, friday_open, friday_close, saturday_open, saturday_close, sunday_open, sunday_close, location_name])) {

            throw paramError;

        } else {

            var businessPost_complete = false;
            var storePost_complete = false;
            var algolia_updated = false;
            var daily_limit_updated = false;
            var business_unowned_updated = false;

            var geo_address;
            var uct_offset;

            var completed_geocode_query = false;
            var completed_timezone_query = false;

            var geocodeURL = "https://maps.googleapis.com/maps/api/geocode/json?" +
                "latlng=" + lat + "," + long +
                "&key=" + geocodeAPIKEY;
            var timezoneURL = "https://maps.googleapis.com/maps/api/timezone/json?" +
                "location=" + lat + "," + long + "&timestamp=1331161200&key=" + timezoneAPIKEY;

            function sendResponse() {
                if (business_unowned_updated && daily_limit_updated && businessPost_complete && storePost_complete && algolia_updated) {
                    console.log("CREATED: business_name: " + request.body.business_name + " business_id: " + request.body.business_id + " Date: " + Date.now());
                    response.status(200);
                    response.send(success200);

                    emailUser(user_id, EmailTemplate_BusinessAdded, function (error) {
                        if (error != null) {
                            console.log(error);
                        }
                    });
                }
            }

            // performs the geocode query
            performRequest(geocodeURL, function (error, res, body) {
                if (!error && res.statusCode == 200) {

                    var body = JSON.parse(body);
                    geo_address = body.results[2].formatted_address;
                    completed_geocode_query = true;
                    continueRequest();

                } else {
                    console.log(error);
                    response.status(500).send("Internal Error");
                }
            });

            // performs the timezone query
            performRequest(timezoneURL, function (error, res, body) {
                if (!error && res.statusCode == 200) {

                    var body = JSON.parse(body);
                    uct_offset = Number(body.rawOffset) / 3600;
                    completed_timezone_query = true;
                    continueRequest();

                } else {
                    console.log(error);
                    response.status(500).send("Internal Error");
                }
            });

            function continueRequest() {

                if (completed_geocode_query && completed_timezone_query) {

                    //adds the business to the unowned list
                    firebase.database().ref("unowned_businesses/").update(function () {

                        var obj = {};
                        obj[business_id] = {
                            date_added: Date.now()
                        };
                        return obj;

                    }()).then(function () {
                        business_unowned_updated = true;
                        sendResponse();
                    }, function (error) {
                        console.log(error);
                        response.status(500).send(failure500);
                    });

                    //adds the business ID to daily limit
                    firebase.database().ref("daily_budget/").update(function () {

                        var obj = {};
                        obj[business_id] = 0;
                        return obj;

                    }()).then(function () {
                        daily_limit_updated = true;
                        sendResponse();
                    }, function (error) {
                        console.log(error);
                        response.status(500).send(failure500);
                    });

                    // builds the business into firebase
                    firebase.database().ref("businesses/" + business_id + "/").update(buildBusiness()).then(function () {
                        businessPost_complete = true;
                        sendResponse();
                    }, function (error) {
                        console.log(error);
                        response.status(500).send("Internal Error");

                    });

                    var title = "Store";
                    //adds a store to the business
                    firebase.database().ref("businesses/" + business_id + "/stores/" + store_id + "/").update(buildStore(home_image, lat, long, store_email, store_phone, title, location_name, geo_address, uct_offset)).then(function () {
                        storePost_complete = true;
                        sendResponse();
                    }, function (error) {
                        console.log(error);
                        response.status(500).send("Internal Error");
                    });

                    //adds the store to algolia
                    algolia_index.addObject({
                        business_name: name,
                        storeID: store_id,
                        businessID: business_id,
                        location_name: location_name,
                        geo_address: geo_address,
                        _geoloc: {
                            lat: Number(lat),
                            lng: Number(long)
                        }

                    }, store_id, function (error, constant) {
                        if (error) {
                            print(error)
                        } else {
                            algolia_updated = true;
                            sendResponse();
                        }
                    });
                }

            }

            function buildBusiness() {
                var obj = {
                    name: name,
                    logo: logo,
                    is_verified: false,

                    business_hours: {},

                    permissions: {
                        owner: {
                            read: {
                                verification: true,
                                advertising: true
                            },
                            write: {
                                logo: true,
                                description: true,
                                items: true,
                                business_hours: true,
                                verification: true,
                                contact_details_business: true,
                                contact_details_store: true,
                                categories: true,
                                members_business: true,
                                members_store: true,
                                stores: true,
                                store_home_image: true,
                                advertising: true,
                                promotion: true,
                                images: true,
                                reply: true,
                                location: true
                            }
                        },
                        admin: {
                            read: {
                                logo: true,
                                description: true,
                                single_promotion_status: true,
                                products: true,
                                services: true,
                                rent: true,
                                business_hours: true,
                                verification: true,
                                contact_details_store: true,
                                contact_details_business: true,
                                members: true,
                                stores: true
                            },
                            write: {
                                logo: true,
                                description: true,
                                single_promotion_status: true,
                                products: true,
                                services: true,
                                rent: true,
                                business_hours: true,
                                verification: true,
                                contact_details_store: true,
                                contact_details_business: true,
                                members: true,
                                stores: true
                            }
                        },
                        member: {
                            read: {
                                logo: true,
                                description: true,
                                single_promotion_status: true,
                                products: true,
                                services: true,
                                rent: true,
                                business_hours: true,
                                verification: true,
                                contact_details_store: true,
                                contact_details_business: true,
                                members: true,
                                stores: true
                            },
                            write: {
                                logo: true,
                                description: true,
                                single_promotion_status: true,
                                products: true,
                                services: true,
                                rent: true,
                                business_hours: true,
                                verification: true,
                                contact_details_store: true,
                                contact_details_business: true,
                                members: true,
                                stores: true
                            }
                        },
                        store_admin: {
                            read: {
                                logo: true,
                                description: true,
                                single_promotion_status: true,
                                products: true,
                                services: true,
                                rent: true,
                                business_hours: true,
                                verification: true,
                                contact_details_store: true,
                                contact_details_business: true,
                                members: true,
                                stores: true
                            },
                            write: {
                                logo: true,
                                description: true,
                                single_promotion_status: true,
                                products: true,
                                services: true,
                                rent: true,
                                business_hours: true,
                                verification: true,
                                contact_details_store: true,
                                contact_details_business: true,
                                members: true,
                                stores: true
                            }
                        },
                        store_member: {
                            read: {
                                logo: true,
                                description: true,
                                single_promotion_status: true,
                                products: true,
                                services: true,
                                rent: true,
                                business_hours: true,
                                verification: true,
                                contact_details_store: true,
                                contact_details_business: true,
                                members: true,
                                stores: true
                            },
                            write: {
                                logo: true,
                                description: true,
                                single_promotion_status: true,
                                products: true,
                                services: true,
                                rent: true,
                                business_hours: true,
                                verification: true,
                                contact_details_store: true,
                                contact_details_business: true,
                                members: true,
                                stores: true
                            }
                        }
                    }
                };

                var storeList = {};
                if (location_name !== "") {
                    storeList[store_id] = location_name + ", " + geo_address;
                } else {
                    storeList[store_id] = geo_address;
                }

                obj["store_manifest"] = storeList;

                var standardHours = {
                    monday_open: monday_open,
                    tuesday_open: tuesday_open,
                    wednesday_open: wednesday_open,
                    thursday_open: thursday_open,
                    friday_open: friday_open,
                    saturday_open: saturday_open,
                    sunday_open: sunday_open,

                    monday_close: monday_close,
                    tuesday_close: tuesday_close,
                    wednesday_close: wednesday_close,
                    thursday_close: thursday_close,
                    friday_close: friday_close,
                    saturday_close: saturday_close,
                    sunday_close: sunday_close
                };

                var standard_hours = {};

                standard_hours["standard"] = standardHours;

                obj["business_hours"] = standard_hours;

                return obj;
            }

        }
    } catch (e){
        console.log(e);
        response.status(403).send(failure403);
    }

});

app.post("/add_business_owner", function(request, response){

    try {

        var businessID = request.body.business_id;
        var userEmail = request.body.user_email;

        if (parametersInvalid([businessID, userEmail])){
            throw paramError;
        } else {

            doesBusinessExist(businessID, function (exists) {
               if (exists){
                   //determine if their is a userID associated with the given email
                   firebase.database().ref("users_lookup/").orderByValue().equalTo(userEmail).on("child_added", function(snapshot){

                       firebase.database().ref("users_lookup/").off();

                       if (snapshot.exists()){

                           var updatedMembersManifest_User = false;
                           var removedFromBusinessUnowned = false;

                           function sendResponse(){
                               if (updatedMembersManifest_User && removedFromBusinessUnowned){
                                   response.status(200).send(success200);
                               }
                           }

                           addUserToBusinessRole(businessID, snapshot.key, "owner", function(error){
                               if (error == null){
                                   updatedMembersManifest_User = true;
                               }
                           });

                           //remove business from business unowned
                           firebase.database().ref("unowned_businesses/"+businessID).remove().then(function(){
                               removedFromBusinessUnowned = true;
                               sendResponse();
                           }, function(error){
                               console.log(error);
                               response.status(500).send(failure500);
                           });

                       } else {
                           var error = failure404;
                           error["reason"] = "user does not exist";
                           response.status(404).send(error);
                       }

                   });
               } else {
                   response.status(404).send(failure404);
               }
            });

        }

    } catch (e){
        console.log(e);
        response.status(403).send(failure403);
    }

});

// MARK: Manage Business

app.get("/test/page_display/:page_name", function (request, response) {

    response.render("webUpdate/" +request.params.page_name + ".html");

});

app.get("/test/:email_address", function (request, response){

    // var type = request.params.type;
    //
    // sendPushNotification("U8ZhRsI9IJVeOSyXRVyss1Qs7Xu2", "Wesley", "Schrauwen", 5, 5, type, function (error) {
    //     console.log(error);
    // });

    // getWeeklyStatisticsForBusinessID("-KUTHmcbDb7spAmzzSuc", function (data) {
    //    response.status(200).send(data);
    // });


    // var email = EmailTemplate_BusinessAdded;
    // email["to"] = request.params.email_address;
    //
    // if (email.subject == "Business Added"){
    //     email["text"] = "" +
    //         "Hi,\n\n" +
    //         "" +
    //         "This is just to confirm that you have successfully added "+"Test"+" to Stackle.\n\n" +
    //         "" +
    //         "Now that your business has been added; you can continue setting it up by adding images, products, promotions, a description and more.\n\n" +
    //         "" +
    //         "To get started with adding these details, navigate to your now available Business Page on the app, slide down and click on 'Manage'.\n" +
    //         "From there you will be able to add a variety of information to your page.\n\n" +
    //         "" +
    //         "The two most important aspects of Stackle are:\n\n" +
    //         "" +
    //         "Unique Stores:\n" +
    //         "You can have many stores. Most importantly each store shares the businesses description, products, promotions, business hours, members and logo.\n" +
    //         "Every store has unique reviews, images, ratings, members.\n\n" +
    //         "" +
    //         "Bidwords and Stats:\n" +
    //         "Every week you will be receiving an email breaking down how your store performed: how many views and clicks you received.\n" +
    //         "Bidwords place a priority on your stores, it increases your ranking in search results.\n" +
    //         "If you have active bidwords you will receive a report on how effective your bidwords were.\n\n" +
    //         "" +
    //         "If you need any help or additional information please check out: http://stackle.co.za/business-docs \n\n" +
    //         "" +
    //         "Alternatively if you any unanswered questions please feel free to get in contact with us via facebook, google or:\n" +
    //         "email: info@stackle.co.za\n" +
    //         "phone: 021 851 2820\n\n" +
    //         "" +
    //         "Please note that our office hours are 9:00AM to 17:00PM, weekdays only.\n\n" +
    //         "" +
    //         "We trust you will have a rewarding experience on Stackle.\n\n" +
    //         "" +
    //         "Sincerely,\n" +
    //         "The Stackle Team";
    // }
    //
    // ejs.renderFile(__dirname +"/views/emails/BusinessAdded.ejs", {businessName: "Test"}, function (error, body) {
    //
    //     error ? console.log(error) : email["html"] = body;
    //
    //     mailgun.messages().send(email, function(error, body){
    //
    //         if(error){
    //             response.status(500).send(error);
    //         } else {
    //             response.status(200).send(success200);
    //         }
    //
    //     });
    //
    // });

    // emailUser("VPk11cxFcYhKKtjxDoiUuYQir2F2", EmailTemplate_BusinessAdded, "Stackle", function (error) {
    //     error ? response.status(500).send(failure500) : response.status(200).send(success200);
    // });

    // var user_id = "2d7Hwk9zoPdBRhHmWeSSagwIvAl2";
    // var geo_address = "South Africa";
    // var location_name = "Somerset West";
    // var business_name = "Stackle";
    //
    // buildEmail_StoreAdded(user_id, geo_address, location_name, business_name, function (email) {
    //
    //     mailgun.messages().send(email, function(error, body){
    //
    //         if (error){
    //             console.log(error);
    //         }
    //
    //     });
    //
    // });
    //
    // var mail = {
    //     from: "Stackle <noreply@stackle.co.za>",
    //     to: "wesley.schrauwen@gmail.com",
    //     subject: "Remove Business Request",
    //     text:
    //
    //     "Hi," +
    //     "We have received a request to delete " +value+ "." +
    //     "" +
    //     "Please note that if a business is delete none of its data can be recovered" +
    //     "" +
    //     "If you still wish to remove this business please visit this link: "+link+", if you do not wish to delete this business you may ignore this message" +
    //     "If you did not make this request or authorise anyone to remove this business please contact us immediately at:" +
    //     "Email: info@stackle.co.za"+
    //     "Phone: 021 851 2820"+
    //     "Sincerely" +
    //     "" +
    //     "Stackle pty Ltd"
    //
    // };
    //
    // ejs.renderFile(__dirname+ "/views/emails/removeBusinessRequest.ejs", {businessName: value, removeLink: link}, function (error, body) {
    //     error ? console.log(error) : mail["html"] = body;
    //
    //     mailgun.messages().send(mail, function (error, body) {
    //         if (error) {
    //             console.log(error);
    //             response.status(500).send(failure500);
    //         } else {
    //             email_sent = true;
    //             sendResponse();
    //         }
    //     });
    //
    // });
    //
    // // firebase.database().ref("test/deeptest").remove().then(function () {
    // //
    //     response.status(200).send(success200);

    // }, function (error) {
    //
    //     response.status(500).send(failure500);
    //
    // });

    // response.render("emails/DepositReceived", {businessName: "Stackle"});

    // response.render("emails/DepositReceived", {businessName: "Stackle"});

    // buildEmail_StoreAdded("2d7Hwk9zoPdBRhHmWeSSagwIvAl2", "Loc", "Geo", "Stackle", function (email) {
    //     mailgun.messages().send(email, function (error, body) {
    //         console.log(error);
    //         console.log(body);
    //     });
    // });

    // buildEmail_DepositReceived("-KUTHmcbDb7spAmzzSuc", "2d7Hwk9zoPdBRhHmWeSSagwIvAl2", function (email) {
    //
    //     // console.log(email["html"]);
    //
    //     mailgun.messages().send(email, function (error, body) {
    //
    //         console.log(error);
    //         console.log("\n\n");
    //         console.log(body);
    //
    //     });
    // });

    // var email ={};
    // console.log(__dirname + "/views/emails/DepositReceived.ejs");
    // // var str = fs.readFileSync(__dirname + "/views/emails/DepositReceived.ejs", "utf8");
    // // console.log(str);
    // email["html"] = ejs.renderFile(__dirname + "/views/emails/DepositReceived.ejs", {businessName: "Stackle"}, function (error, result) {
    //     console.log(error);
    //     console.log(result);
    // });
    // console.log(email["html"]);

    // notifyUser("U8ZhRsI9IJVeOSyXRVyss1Qs7Xu2", "2d7Hwk9zoPdBRhHmWeSSagwIvAl2", "Test", "-KSvpc0qtV_Com3HThko", "-KUTHmcbDb7spAmzzSud")

});

app.get("/bidword_test/:round", function (request, response) {

    var businessID = "-KUTHmcbDb7spAmzzSuc";

    var newBidword = "/advertising/bidword_value_limit";
    var dailyLimit = "/advertising/daily_spend_limit";
    var deposit = "advertising/" +businessID+ "/deposit_remaining";
    var localHost = "http://192.168.1.121:8080";

    var round = request.params.round;

    var test = ["complete success", "double success", "single success", "no success"];

    if (round == "1"){
        firstRoundTest();
    } else if (round == "2"){

    } else if (round == "3"){

    } else if (round == "4"){

    }

    function firstRoundTest() {

        var dailyLimitUpdated = false;
        var dailyBudgetUpdated = false;
        var depositUpdated = false;

        function testAdwords() {
            if (dailyLimitUpdated && dailyBudgetUpdated && depositUpdated){

                var adwordPrices = [1000, 400, 100];
                var adwordsToUpdate = adwordPrices.length;
                var words = ["expensive", "middle", "cheap"];

                function testAdwords() {
                    if (adwordsToUpdate == 0){

                        algolia_index.getObject("-KUTHmcbDb7spAmzzSud", ["adwords"], function (error, body) {
                           console.log(body);
                        });

                    }
                }

                for (var i = 0; i < adwordPrices.length || i < words.length; i++){

                    performRequest(localHost + newBidword, function () {
                        return adwordOPTIONS = {
                            json: true,
                            method: "post",
                            body: {
                                business_id: businessID,
                                user_id: "U8ZhRsI9IJVeOSyXRVyss1Qs7Xu2",
                                word: words[i],
                                max_value: adwordPrices[i]
                            }
                        };

                    }(), function (error, res, body) {

                        console.log("\n\nadword : " +adwordsToUpdate+ " is completed");
                        console.log("error\n" +error);
                        console.log("res\n" +res);
                        console.log("body\n" +body);

                        adwordsToUpdate -- ;
                        testAdwords();
                    });
                }

            }
        }

        var options = {
            method: "post",
            body: {
                business_id: businessID,
                daily_limit: 1500,
                deposit_amount: 1500,
                receipt_id: "001"
            },
            json: true
        };

        //daily limit update
        performRequest(localHost +dailyLimit, options, function (error, res, body) {
            console.log("\n\nupdating daily limit");
            console.log("error\n"+error);
            console.log("res\n"+res);
            console.log("body\n"+body);
        });

        //daily budget
        firebase.database().ref("daily_budget/").update(function () {
            var obj = {};
            obj[businessID] = 1500;
            return obj;
        }()).then(function () {

            console.log("\n\nupdated daily budget");


        }, function (error) {
            console.log(error);
        });

        //update remaining deposit
        performRequest(localHost +deposit, options, function (error, res, body) {
            console.log("\n\nupdating remaining deposit");
            console.log("error\n"+error);
            console.log("res\n"+res);
            console.log("body\n"+body);
        });
    }

});

app.get("/manage_business/verify_permission", function(request, response){

    try {

        var user_id = request.headers.user_id;
        var business_id = request.headers.business_id;
        var read_write = request.headers.read_write;
        var permission_type = request.headers.permission_type;

        if (user_id || business_id){
            throw "Parameters Undefined";
        }

        var user_authorised_access = false;
        var permission_granted = false;

        function sendResponse(){
            if(user_authorised_access && permission_granted){

                response.status(200).send("authorized");

            }
        }

        function UAuth(user_auth){
            if(user_auth) {

                user_authorised_access = true;
                sendResponse();

            } else {

                response.status(403).send("unauthorized access");

            }
        }

        function UPermission(user_permission){
            if (user_permission){

                permission_granted = true;
                sendResponse();

            } else if (user_permission == 403) {

                response.status(403).send("unauthorized access");

            } else if (user_permission == 500) {

                response.status(500).send("500 - Internal Error");

            }
        }

        userIsInBusiness(user_id, business_id, UAuth);
        userHasPermission(user_id, business_id, read_write, permission_type, UPermission);

    } catch (e){
        console.log(e);
        response.status(403).send("Parameters non-existant or passed incorrectly.");
    }
});

app.post("/dashboard", function (request, response) {

    try {

        var businessID = request.body.business_id;
        var storeID = request.body.store_id;

        if (parametersInvalid([businessID, storeID])){
            throw paramError;
        } else {

            var has_likes = false;
            var has_reviews = false;
            var has_official_images = false;
            var has_review_images = false;
            var has_rating = false;
            // var has_views = false;
            // var has_advert = false;
            var has_presence = false;
            var has_categories = false;
            var has_location_name = false;
            var has_geo_address = false;
            var has_url = false;
            var has_coords = false;

            var item_count = 0;
            var official_image_count = 0;
            var category_count = 0;

            var has_products = false;
            var has_services = false;
            var has_accommodation = false;

            var obj = {};

            function sendResponse() {
                if(has_coords && has_location_name && has_geo_address && has_url && has_likes && has_reviews && has_rating && has_official_images && has_review_images && has_presence && has_categories){
                    obj["presence"] = Math.round(item_count * 4 + official_image_count * 4 + category_count * 2) / 10;
                    obj["code"] = 200;
                    response.status(200).send(obj);
                }
            }

            function setPresence() {
                if (has_accommodation && has_services && has_products){
                    obj["item_count"] = item_count;
                    has_presence = true;
                    sendResponse();
                }
            }

            // get coords
            firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/-geoloc").once("value").then(function (snapshot) {

                var lat = "-34.0862876";
                var long = "18.8246912";

                if (snapshot.exists()){
                    var geoLoc = snapshot.val();

                    lat = geoLoc["lat"];
                    long = geoLoc["long"];

                }

                obj["lat"] = lat;
                obj["long"] = long;
                has_coords = true;
                sendResponse();
            });

            // get home image url
            firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/home_image").once("value").then(function (snapshot) {

                var home_image = "";

                if (snapshot.exists()){
                    home_image = snapshot.val();

                }

                obj["home_image_url"] = home_image;
                has_url = true;
                sendResponse();
            });

            // get geo address
            firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/geo_address").once("value").then(function (snapshot) {

                var geo_address = "";

                if (snapshot.exists()){
                    geo_address = snapshot.val();

                }

                obj["geo_address"] = geo_address;
                has_geo_address = true;
                sendResponse();
            });

            // get location name
            firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/location_name").once("value").then(function (snapshot) {

                var location_name = "";

                if (snapshot.exists()){
                    location_name = snapshot.val();

                }

                obj["location_name"] = location_name;
                has_location_name = true;
                sendResponse();
            });

            // get category count
            firebase.database().ref("businesses/" +businessID+ "/category_count").once("value").then(function (snapshot) {
                var categoryCount = 0;

                if (snapshot.exists()){
                    categoryCount = snapshot.val();
                    category_count = categoryCount;
                }

                obj["category_count"] = categoryCount;
                has_categories = true;
                sendResponse();
            });

            // get item count
            firebase.database().ref("businesses/" +businessID+ "/Products_item_count").once("value").then(function (snapshot) {

                if (snapshot.exists()){
                    item_count += snapshot.val();
                }

                has_products = true;
                setPresence();

            });

            firebase.database().ref("businesses/" +businessID+ "/Services_item_count").once("value").then(function (snapshot) {

                if (snapshot.exists()){
                    item_count += snapshot.val();
                }

                has_services = true;
                setPresence();

            });

            firebase.database().ref("businesses/" +businessID+ "/Accommodation_item_count").once("value").then(function (snapshot) {

                if (snapshot.exists()){
                    item_count += snapshot.val();
                }

                has_accommodation = true;
                setPresence();

            });

            // get likes
            firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/likes").once("value").then(function (snapshot) {

                if (snapshot.exists()){
                    obj["likes"] = snapshot.val();
                } else {
                    obj["likes"] = 0;
                }

                has_likes = true;
                sendResponse();

            });

            // get reviews
            firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/review_count").once("value").then(function (snapshot) {

                if (snapshot.exists()){
                    obj["reviews"] = snapshot.val();
                } else {
                    obj["reviews"] = 0;
                }

                has_reviews = true;
                sendResponse();

            });

            //get rating
            firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/rating").once("value").then(function (snapshot) {

                if (snapshot.exists()){
                    var num = snapshot.val()["numerator"];
                    var denom = snapshot.val()["denominator"];

                    var rating = Math.round(num / denom * 500);

                    obj["rating"] = rating / 100;
                } else {
                    obj["rating"] = 3;
                }

                has_rating = true;
                sendResponse();

            });

            // get review images
            firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/images_review").once("value").then(function (snapshot) {

                if (snapshot.exists()){
                    obj["images_review"] = snapshot.val();
                } else {
                    obj["images_review"] = 0;
                }

                has_review_images = true;
                sendResponse();

            });

            // get official images
            firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/images_official").once("value").then(function (snapshot) {

                if (snapshot.exists()){
                    obj["images_official"] = snapshot.val();
                    official_image_count = snapshot.val();
                } else {
                    obj["images_official"] = 0;
                }

                has_official_images = true;
                sendResponse();

            });

        }

    } catch (e) {
        console.log(e);
        response.status(403).send(failure403);
    }

});

app.post("/advertising/bidword_value_limit", function (request, response) {

    try {

        var businessID = request.body.business_id;
        var userID = request.body.user_id;
        var word = request.body.word;
        var maxValue = request.body.max_value;

        if (parametersInvalid([businessID, userID, maxValue]) && typeof(maxValue) == Number){
            throw paramError;
        } else {

            doesBusinessExist(businessID, function (exists) {
                if (exists){

                    var algolia_updated = false;
                    var firebase_updated = false;

                    word = word.toLocaleLowerCase();

                    function sendResponse() {
                        if (algolia_updated && firebase_updated){
                            response.status(200).send(success200);
                        }
                    }

                    //update adword
                    firebase.database().ref("advertising/" + businessID+ "/adwords/").update(function () {
                        var obj = {};
                        obj[word] = maxValue;
                        return obj;
                    }()).then(function () {

                        firebase_updated = true;
                        sendResponse();

                    }, function (error) {
                        console.log(error);
                        response.status(500).send(failure500);
                    });

                    // check that daily budget is high enough
                    firebase.database().ref("daily_budget/" +businessID).once("value").then(function (snapshot) {
                        if (snapshot.exists() && snapshot.val() > maxValue){
                            //check if max value is not too high for daily limit
                            firebase.database().ref("advertising/" +businessID+ "/daily_limit").once("value").then(function (snapshot) {

                                if (snapshot.exists() && snapshot.val() > maxValue){

                                    // check that there is deposit remaining
                                    firebase.database().ref("advertising/" +businessID+ "/deposit_remaining").once("value").then(function (snapshot) {

                                        if (snapshot.exists() && snapshot.val() > maxValue){


                                            //update algolia
                                            firebase.database().ref("businesses/" +businessID+ "/store_manifest").once("value").then(function (snapshot) {

                                                if(snapshot.exists()){

                                                    var storeIDS = Object.keys(snapshot.val());

                                                    updateAlgolia(storeIDS, {
                                                        adwords: {
                                                            value: word,
                                                            _operation: "Add"
                                                        }
                                                    }, function (error) {

                                                        if (error != null){
                                                            console.log(error);
                                                        } else {
                                                            algolia_updated = true;
                                                            sendResponse();
                                                        }

                                                    });

                                                } else {
                                                    console.log("post error :: /advertising/word_value_limit\n" +
                                                        "description: couldnt find store manifest\n\n" +
                                                        "parameters:\n" +
                                                        "businessID: " +businessID);
                                                    response.status(500).send(failure500);
                                                }

                                            });

                                        } else {
                                            algolia_updated = true;
                                            sendResponse();
                                        }


                                    });




                                } else {
                                    algolia_updated = true;
                                    sendResponse();
                                }
                            });
                        } else {
                            algolia_updated = true;
                            sendResponse();
                        }
                    });

                } else {
                    response.status(404).send(failure404);
                }
            });
        }

    } catch (error){
        console.log(error);
        response.status(403).send(failure403);
    }

});

app.post("/business/bill", function (request, response) {

    /**
     *
     * When using this post be sure to set adValue correctly.
     * If there are no adwords or the server did not return an advalue in storeQuery set adValue to 0
     * If not done it will result in wasted server time and / or incorrectly billed amounts from the businesses account.
     *
    */
    try {

        var businessID = request.body.business_id;
        var adValue = request.body.ad_value;
        var userID = request.body.user_id;
        var words = request.body.queries;

        if (parametersInvalid([businessID, adValue, userID]) || !Array.isArray(words)){
            throw paramError;
        } else {

            doesBusinessExist(businessID, function (exists) {
               if (exists){
                   var click_through_updated = false;
                   var billed = false;

                   var week = getWeek();
                   var toDo = words.length;

                   function sendResponse(){
                       if (toDo == 0 && billed){
                           response.status(200).send(success200);
                       }
                   }

                   // Updates stats for this business of words that have been searched..
                   words.forEach(function (word) {
                       firebase.database().ref("advertising/"+businessID+"/click_through/" +week+ "/" +word).once("value").then(function (snapshot) {

                           var count = 1;

                           if (snapshot.exists()){
                               count += snapshot.val();
                           }

                           firebase.database().ref("advertising/" +businessID+ "/click_through/" +week+ "/").update(function () {

                               var obj = {};
                               obj[word] = count;
                               return obj;

                           }()).then(function () {

                               toDo -= 1;
                               sendResponse();

                           }, function (error) {
                               console.log(error);
                               response.status(500).send(failure500);
                           });

                       });
                   });

                   if (adValue > 0){

                       var daily_funds_available;
                       var deposit_available;

                       var billAmount = adValue;
                       var remainingDeposit;
                       var remainingDailyBudget;

                       function performBill() {
                           if (daily_funds_available && deposit_available){

                               var daily_budget_updated = false;
                               var deposit_updated = false;
                               var stackle_account_updated = false;
                               var weekly_expenditure_updated = false;

                               function _sendResponse() {
                                   if (daily_budget_updated && deposit_updated && stackle_account_updated && weekly_expenditure_updated){


                                       // if the current remaining deposit is less than 0.5 we should remove all the bidwords for this business.
                                       // otherwise check which bidwords arent viable and remove them.
                                       if (remainingDeposit - billAmount < 0.5){

                                           firebase.database().ref("businesses/" +businessID+ "/store_manifest").once("value").then(function (snapshot) {

                                               if (snapshot.exists()){
                                                   var storeIDS = Object.keys(snapshot.val());

                                                   var object = {
                                                       "adwords": [""]
                                                   };

                                                   updateAlgolia(storeIDS, object, function (error) {
                                                       if (error != null){
                                                           console.log(error);
                                                       } else {
                                                           billed = true;
                                                           sendResponse();
                                                       }
                                                   });

                                                   emailBusinessOwner(businessID, EmailTemplate_DepositDepleted,function (error) {
                                                       if (error != null){
                                                           console.log(error);
                                                       }
                                                   });

                                               } else {

                                                   console.log("error at post /business/bill\n" +
                                                       "description: store manifest could not be found\n\n" +
                                                       "parameters:\n" +
                                                       "businessID: " +businessID);

                                               }

                                           });

                                       } else {
                                           //check for all bidwords that are still viable with the remaining daily budget
                                           //get the value of all bidwords

                                           var newValue = (remainingDailyBudget < remainingDeposit ?
                                               remainingDailyBudget : remainingDeposit);

                                           newValue -= billAmount;

                                           removeBidwordsLessThanValue(businessID, newValue, function (error) {
                                               if (error) {
                                                   console.log(error);
                                               } else {
                                                   billed = true;
                                                   sendResponse();
                                               }
                                           });

                                       }
                                   }
                               }

                               var currentDate = new Date().getUTCFullYear() +""+ new Date().getUTCMonth();

                               //update the stackle account
                               firebase.database().ref("stackle_account/bidwords/" +currentDate).once("value").then(function (snapshot) {

                                   var currentAmount = billAmount;

                                   if(snapshot.exists()){
                                       currentAmount += snapshot.val();
                                   }

                                   firebase.database().ref("stackle_account/bidwords/").update(function(){

                                       var obj = {};
                                       obj[currentDate] = currentAmount;
                                       return obj;

                                   }()).then(function () {

                                       stackle_account_updated = true;
                                       _sendResponse();

                                   }, function (error) {

                                       console.log(error);
                                       response.status(500).send(failure500);

                                   });

                               });

                               //update the businesses current deposit
                               firebase.database().ref("advertising/" +businessID).update({

                                   deposit_remaining: remainingDeposit - billAmount

                               }).then(function () {

                                   deposit_updated = true;
                                   _sendResponse();

                               }, function (error) {
                                   console.log(error);
                                   response.status(500).send(failure500);
                               });

                               //update the businesses daily budget
                               firebase.database().ref("daily_budget/").update(function(){
                                   var obj = {};
                                   obj[businessID] = remainingDailyBudget - billAmount;
                                   return obj;
                               }()).then(function () {

                                   daily_budget_updated = true;
                                   _sendResponse();

                               }, function (error) {

                                   console.log(error);
                                   response.status(500).send(failure500);

                               });

                               //update the businesses weekly expenditure
                               firebase.database().ref("advertising/" +businessID+ "/weekly_expenditure/" +week+ "/").once("value").then(function (snapshot) {

                                   var expenditure = billAmount;

                                   if (snapshot.exists()){
                                       expenditure += snapshot.val();
                                   }

                                   firebase.database().ref("advertising/" +businessID+ "/weekly_expenditure/").update(function(){

                                       var obj = {};
                                       obj[week] = expenditure;
                                       return obj;

                                   }()).then(function () {

                                       weekly_expenditure_updated = true;
                                       _sendResponse();

                                   }, function (error) {
                                       console.log(error);
                                       response.status(500).send(failure500);
                                   });

                               });

                           }
                       }

                       // validate that the business still has daily budget left to spend
                       firebase.database().ref("daily_budget/" +businessID).once("value").then(function (snapshot) {

                           if (snapshot.exists()){

                               remainingDailyBudget = snapshot.val();

                               if (remainingDailyBudget >= billAmount && remainingDailyBudget > 0){
                                   daily_funds_available = true;
                                   performBill();
                               } else {
                                   response.status(400).send(insufficientFundsError);
                               }

                           } else {
                               daily_funds_available = true;
                               performBill();
                           }

                       });

                       //validate that there is still a deposit available
                       firebase.database().ref("advertising/" +businessID+ "/deposit_remaining").once("value").then(function (snapshot) {
                           if (snapshot.exists()){

                               remainingDeposit = snapshot.val();

                               if (remainingDeposit >= billAmount && remainingDeposit > 0){

                                   deposit_available = true;
                                   performBill();

                               } else {
                                   response.status(400).send(insufficientFundsError);
                               }

                           } else {
                               console.log("post error at /business/bill\n" +
                                   "description: deposit_remaining does not exist....\n " +
                                   "parameters: \n\n" +
                                   "businessID: " +businessID+ "\n" +
                                   "billAmount: " +billAmount+ "\n" +
                                   "adValue: " +adValue+ "\n" +
                                   "date: " +Date.now());
                               response.status(404).send(failure404);
                           }
                       });

                   } else {
                       billed = true;
                       sendResponse();
                   }
               } else {
                   response.status(404).send(failure404);
               }
            });

        }

    } catch (error){
        console.log(error);
        response.status(403).send(failure403);
    }

});

app.post("/advertising/deposit", function (request, response) {

    try {

        var businessID = request.body.business_id;
        var depositAmount = request.body.deposit_amount;
        var receiptID = request.body.receipt_id;

        if (parametersInvalid([businessID, depositAmount])){
            throw paramError;
        } else {

            var algolia_updated = false;
            var deposit_updated = false;
            var receipt_created = false;

            function sendResponse() {
                if (algolia_updated && deposit_updated && receipt_created) {
                    response.status(200).send(success200);
                }
            }

            doesBusinessExist(businessID, function (exists) {
                if (exists) {

                    //create receipt reference
                    firebase.database().ref("advertising/" +businessID+ "/receipts/").update(function () {

                        var obj = {};
                        obj[receiptID] = Date.now();
                        return obj;

                    }()).then(function () {
                        receipt_created = true;
                        sendResponse();
                    }, function (error) {
                        console.log(error);
                        response.status(500).send(failure500);
                    });

                    // update algolia
                    // var hasStoreManifest = false;
                    //
                    // var adwords;
                    // var storeManifest;

                    function _updateAlgolia() {
                        if (deposit_updated){

                            removeBidwordsLessThanValue(businessID, depositAmount, function (error) {
                               if (error){
                                   console.log(error);
                                   response.status(500).send(failure500);
                               } else {
                                   algolia_updated = true;
                                   sendResponse();
                               }
                            });

                            // firebase.database().ref("advertising/" +businessID+ "/adwords").once("value").then(function (snapshot) {
                            //     if(snapshot.exists()){
                            //
                            //         adwords = Object.keys(snapshot.val());
                            //
                            //         for (var i = 0; i < adwords.length; i++){
                            //
                            //             if (snapshot.val()[adwords[i]] >= depositAmount){
                            //                 adwords.splice(i, 1);
                            //             }
                            //
                            //         }
                            //
                            //         var obj = {
                            //             "adwords": adwords
                            //         };
                            //
                            //         updateAlgolia(storeManifest, obj, function (error) {
                            //
                            //             if (error != null){
                            //                 console.log(error);
                            //                 response.status(500).send(failure500);
                            //             } else {
                            //                 algolia_updated = true;
                            //                 sendResponse();
                            //             }
                            //
                            //         });
                            //
                            //     } else {
                            //         algolia_updated = true;
                            //         sendResponse();
                            //     }
                            // });

                        }
                    }

                    // firebase.database().ref("businesses/" +businessID+ "/store_manifest").once("value").then(function (snapshot) {
                    //
                    //     if (snapshot.exists()){
                    //
                    //         storeManifest = Object.keys(snapshot.val());
                    //         hasStoreManifest = true;
                    //         _updateAlgolia();
                    //
                    //     } else {
                    //         console.log("post error /advertising/deposit\n" +
                    //             "description: store manifest not found\n\n" +
                    //             "parameters:\n" +
                    //             "businessID: " +businessID);
                    //     }
                    //
                    // });

                    //update current deposit
                    firebase.database().ref("advertising/" +businessID+ "/deposit_remaining").once("value").then(function (snapshot) {

                        if (snapshot.exists()){
                            depositAmount += snapshot.val();
                        }

                        firebase.database().ref("advertising/" +businessID+ "/").update({
                            deposit_remaining: depositAmount
                        }).then(function () {

                            deposit_updated = true;
                            _updateAlgolia();
                            sendResponse();

                        }, function (error) {
                            console.log(error);
                            response.status(500).send(failure500);
                        });

                    });



                } else {
                    response.status(404).send(failure404);
                }
            });

        }

    } catch (error){
        console.log(error);
        response.status(403).send(failure403);
    }

});

app.post("/advertising/daily_spend_limit", function (request, response) {

    try {

        var businessID = request.body.business_id;
        var dailyLimit = request.body.daily_limit;

        if (parametersInvalid([businessID, dailyLimit]) || isNaN(dailyLimit)){
            throw paramError;
        } else {

            if (dailyLimit > 10) {

                doesBusinessExist(businessID, function (exists) {
                    if (exists){

                        var ownerEmailed = false;
                        var bidwordsUpdated = false;

                        function sendResponse() {
                            if (ownerEmailed && bidwordsUpdated) {
                                response.status(200).send(success200);
                            }
                        }

                        //email owner
                        firebase.database().ref("advertising/" + businessID + "/").update({daily_limit: dailyLimit}).then(function () {

                            emailBusinessOwner(businessID, EmailTemplate_DailyLimitUpdated, function (error) {
                                if (error == null) {
                                    ownerEmailed = true;
                                    sendResponse();
                                } else {
                                    console.log(error);
                                }
                            });

                        }, function (error) {
                            console.log(error);
                            response.status(500).send(failure500);
                        });

                        //remove bidwords
                        removeBidwordsLessThanValue(businessID, dailyLimit, function (error) {
                            if (error){
                                response.status(500).send(failure500);
                            } else {
                                bidwordsUpdated = true;
                                sendResponse();
                            }
                        });

                    } else {
                        response.status(404).send(failure404);
                    }
                });

            } else {
                response.status(400).send(dailyLimitTooLowError);
            }

        }

    } catch (error){
        console.log(error);
        response.status(403).send(failure403);
    }

});

app.post("/advertising/remove_word", function (request, response) {

    try {

        var businessID = request.body.business_id;
        var userID = request.body.user_id;
        var word = request.body.remove_word;

        if (parametersInvalid([businessID, userID, word])){
            throw paramError;
        } else {

            firebase.database().ref("advertising/" +businessID+ "/adwords/" +word).remove().then(function () {

                response.status(200).send(success200);

            }, function (error) {
                console.log(error);
                response.status(500).send(failure500);
            });

        }

    } catch (error) {
        console.log(error);
        response.status(403).send(failure403);
    }

});

app.post("/advertising/word_details", function (request, response) {

    try {

        var businessID = request.body.business_id;
        var bidword = request.body.bidword;


        if (parametersInvalid([businessID, bidword])){
            throw paramError;
        } else {

            doesBusinessExist(businessID, function (exists) {
               if (exists){
                   bidword = bidword.toLowerCase();
                   var currentWeek = getWeek();

                   var monthWeek4 = currentWeek;
                   var monthWeek3 = currentWeek - 1;
                   var monthWeek2 = currentWeek - 2;
                   var monthWeek1 = currentWeek - 3;

                   if (currentWeek == 2){
                       monthWeek1 = 52;
                   } else if (currentWeek == 1){
                       monthWeek2 = 52;
                       monthWeek1 = 51;
                   } else if (currentWeek == 0){
                       monthWeek3 = 52;
                       monthWeek2 = 51;
                       monthWeek1 = 50;
                   }

                   var hasBidwordValue = false;

                   var hasWeek1_views = false;
                   var hasWeek2_views = false;
                   var hasWeek3_views = false;
                   var hasWeek4_views = false;

                   var hasWeek1_searches = false;
                   var hasWeek2_searches = false;
                   var hasWeek3_searches = false;
                   var hasWeek4_searches = false;

                   var hasWeek1_rank = false;
                   var hasWeek2_rank = false;
                   var hasWeek3_rank = false;
                   var hasWeek4_rank = false;

                   var month = {};
                   var week = {};
                   var bidwordValue;

                   var weekRank;
                   var views = [];
                   var rank = [];
                   var searches = [];

                   function sendResponse() {
                       if (hasWeek1_views && hasWeek2_views && hasWeek3_views && hasWeek4_views
                           && hasWeek1_searches && hasWeek2_searches && hasWeek3_searches && hasWeek4_searches
                           && hasWeek1_rank && hasWeek2_rank && hasWeek3_rank && hasWeek4_rank && hasBidwordValue){

                           month["views"] = sumOfArray(views);
                           month["rank"] = Math.round(averageOfArrays(rank, searches) * 100) / 100;
                           month["searches"] = sumOfArray(searches);

                           if (week["searches"] != 0){
                               week["rank"] = Math.round((weekRank / week["searches"]) * 100) / 100;
                           } else {
                               week["rank"] = -1;
                           }

                           response.status(200).send({
                               month: month,
                               week: week,
                               bidword: bidword,
                               value: bidwordValue,
                               code: 200
                           });
                       }
                   }

                   //get the bidword value
                   firebase.database().ref("advertising/" +businessID+ "/adwords/" +bidword).once("value").then(function (snapshot) {

                       if (snapshot.exists()){
                           bidwordValue = snapshot.val();
                       } else {
                           bidwordValue = 0;
                       }

                       hasBidwordValue = true;
                       sendResponse();

                   });

                   // get the number of views for this word in the current week
                   firebase.database().ref("advertising/" +businessID+ "/click_through/" +monthWeek4+ "/" +bidword).once("value").then(function (snapshot) {
                       var snapValue = 0;
                       if (snapshot.exists()){
                           snapValue = snapshot.val();
                       }

                       week["views"] = snapValue;
                       views.push(snapValue);
                       hasWeek4_views = true;
                       sendResponse();
                   });

                   firebase.database().ref("advertising/" +businessID+ "/click_through/" +monthWeek3+ "/" +bidword).once("value").then(function (snapshot) {
                       var snapValue = 0;
                       if (snapshot.exists()){
                           snapValue = snapshot.val();
                       }

                       views.push(snapValue);
                       hasWeek3_views = true;
                       sendResponse();
                   });

                   firebase.database().ref("advertising/" +businessID+ "/click_through/" +monthWeek2+ "/" +bidword).once("value").then(function (snapshot) {
                       var snapValue = 0;
                       if (snapshot.exists()){
                           snapValue = snapshot.val();
                       }

                       views.push(snapValue);
                       hasWeek2_views = true;
                       sendResponse();
                   });

                   firebase.database().ref("advertising/" +businessID+ "/click_through/" +monthWeek1+ "/" +bidword).once("value").then(function (snapshot) {
                       var snapValue = 0;
                       if (snapshot.exists()){
                           snapValue = snapshot.val();
                       }

                       views.push(snapValue);
                       hasWeek1_views = true;
                       sendResponse();
                   });

                   //get the number of end ranks
                   firebase.database().ref("advertising/" +businessID+ "/after_sort/" +monthWeek4+ "/" +bidword).once("value").then(function (snapshot) {
                       var snapValue = 0;
                       if (snapshot.exists()){
                           snapValue = snapshot.val();
                       }

                       weekRank = snapValue;
                       rank.push(snapValue);
                       hasWeek4_rank = true;
                       sendResponse();
                   });

                   firebase.database().ref("advertising/" +businessID+ "/after_sort/" +monthWeek3+ "/" +bidword).once("value").then(function (snapshot) {
                       var snapValue = 0;


                       if (snapshot.exists()){
                           snapValue = snapshot.val();
                       }

                       rank.push(snapValue);
                       hasWeek3_rank = true;
                       sendResponse();
                   });

                   firebase.database().ref("advertising/" +businessID+ "/after_sort/" +monthWeek2+ "/" +bidword).once("value").then(function (snapshot) {
                       var snapValue = 0;


                       if (snapshot.exists()){
                           snapValue = snapshot.val();

                       }
                       rank.push(snapValue);

                       hasWeek2_rank = true;
                       sendResponse();
                   });

                   firebase.database().ref("advertising/" +businessID+ "/after_sort/" +monthWeek1+ "/" +bidword).once("value").then(function (snapshot) {
                       var snapValue = 0;

                       if (snapshot.exists()){
                           snapValue = snapshot.val();
                       }

                       rank.push(snapValue);

                       hasWeek1_rank = true;
                       sendResponse();
                   });

                   // get the number of searches for this word in the current week
                   firebase.database().ref("advertising/" +businessID+ "/adword_searches/" +monthWeek4+ "/" +bidword).once("value").then(function (snapshot) {
                       var snapValue = 0;
                       if (snapshot.exists()){
                           snapValue = snapshot.val();
                       }

                       week["searches"] = snapValue;
                       searches.push(snapValue);
                       hasWeek4_searches = true;
                       sendResponse();
                   });

                   firebase.database().ref("advertising/" +businessID+ "/adword_searches/" +monthWeek3+ "/" +bidword).once("value").then(function (snapshot) {
                       var snapValue = 0;
                       if (snapshot.exists()){
                           snapValue = snapshot.val();
                       }

                       searches.push(snapValue);
                       hasWeek3_searches = true;
                       sendResponse();
                   });

                   firebase.database().ref("advertising/" +businessID+ "/adword_searches/" +monthWeek2+ "/" +bidword).once("value").then(function (snapshot) {
                       var snapValue = 0;
                       if (snapshot.exists()){
                           snapValue = snapshot.val();
                       }

                       searches.push(snapValue);
                       hasWeek2_searches = true;
                       sendResponse();
                   });

                   firebase.database().ref("advertising/" +businessID+ "/adword_searches/" +monthWeek1+ "/" +bidword).once("value").then(function (snapshot) {
                       var snapValue = 0;
                       if (snapshot.exists()){
                           snapValue = snapshot.val();
                       }

                       searches.push(snapValue);
                       hasWeek1_searches = true;
                       sendResponse();
                   });
               } else {
                   response.status(404).send(failure404);
               }
            });

        }

    } catch (error){
        console.log(error);
        response.status(403).send(failure403);
    }

});

app.post("/advertising/dashboard", function (request, response) {

    try {

        var businessID = request.body.business_id;
        var userID = request.body.user_id;

        if (parametersInvalid([businessID, userID])){
            throw paramError;
        } else {

            var deposit_downloaded = false;
            var weekly_spend_downloaded = false;
            var views_downloaded = false;
            var daily_limit_downloaded = false;
            var daily_limit_remaining_downloaded = false;
            var adword_searches_downloaded = false;
            var adword_price_limit_downloaded = false;
            var before_sort_downloaded = false;
            var after_sort_downloaded = false;

            var deposit_remaining;
            var weekly_expenditure;
            var views;
            var daily_limit_cap;
            var daily_limit_remaining;
            var adword_searches;
            var adword_limits;
            var before_sort;
            var after_sort;

            function sendResponse() {
                if (after_sort_downloaded && before_sort_downloaded && adword_price_limit_downloaded && deposit_downloaded && weekly_spend_downloaded && views_downloaded && daily_limit_downloaded && daily_limit_remaining_downloaded){
                    response.status(200).send({
                        deposit_remaining: deposit_remaining,
                        weekly_expenditure: weekly_expenditure,
                        views: views,
                        daily_cap: daily_limit_cap,
                        daily_limit_remaining: daily_limit_remaining,
                        adword_searches: adword_searches,
                        adword_limits: adword_limits,
                        before_sort: before_sort,
                        after_sort: after_sort,
                        code: 200
                    });
                }
            }

            var currentWeek = getWeek();

            firebase.database().ref("advertising/" +businessID+ "/after_sort/" +currentWeek+ "/").once("value").then(function (snapshot) {

                if (snapshot.exists()){
                    after_sort = snapshot.val();
                } else {
                    after_sort = {
                        "_null": 0
                    }
                }

                after_sort_downloaded = true;
                sendResponse();

            });

            firebase.database().ref("advertising/" +businessID+ "/before_sort/" + currentWeek+ "/").once("value").then(function (snapshot) {

                if (snapshot.exists()){
                    before_sort = snapshot.val();
                } else {
                    before_sort = {
                      "_null": 0
                    };
                }

                before_sort_downloaded = true;
                sendResponse();

            });

            //get adword price limits
            firebase.database().ref("advertising/" +businessID+ "/adwords").once("value").then(function (snapshot) {

                if (snapshot.exists()){
                    adword_limits = snapshot.val();
                } else {
                    adword_limits = {
                      "_null": 0
                    };
                }

                adword_price_limit_downloaded = true;
                sendResponse();

            });

            //get expenditure
            firebase.database().ref("advertising/" +businessID+ "/weekly_expenditure/" +currentWeek).once("value").then(function (snapshot) {

                if (snapshot.exists()){
                    weekly_expenditure = snapshot.val();
                } else {
                    weekly_expenditure = 0;
                }

                weekly_spend_downloaded = true;
                sendResponse();

            });

            //get adword searches
            firebase.database().ref("advertising/" +businessID+ "/adword_searches/" +currentWeek).once("value").then(function (snapshot) {

                if (snapshot.exists()){
                    adword_searches = snapshot.val();
                } else {
                    adword_searches = {
                        "_null": 0
                    };
                }

                adword_searches_downloaded = true;
                sendResponse();

            });

            //daily limit remaining
            firebase.database().ref("daily_budget/" +businessID).once("value").then(function (snapshot) {

                if (snapshot.exists()){
                    daily_limit_remaining = snapshot.val();
                } else {
                    daily_limit_remaining = 0;
                }

                daily_limit_remaining_downloaded = true;
                sendResponse();
            });

            // daily limit hard cap
            firebase.database().ref("advertising/" +businessID+ "/daily_limit").once("value").then(function (snapshot) {
                if (snapshot.exists()){
                    daily_limit_cap = snapshot.val();
                } else {
                    daily_limit_cap = 0;
                }

                daily_limit_downloaded = true;
                sendResponse();
            });

            // deposit
            firebase.database().ref("advertising/" +businessID+ "/deposit_remaining").once("value").then(function (snapshot) {
                if (snapshot.exists()){
                    deposit_remaining = snapshot.val();
                } else {
                    deposit_remaining = 0;
                }

                deposit_downloaded = true;
                sendResponse();
            });

            // views
            firebase.database().ref("advertising/" +businessID+ "/click_through/" +currentWeek).once("value").then(function (snapshot) {
                if (snapshot.exists()){
                    views = snapshot.val();
                } else {
                    views = {
                        "_null": 0
                    };
                }

                views_downloaded = true;
                sendResponse();

            });

        }

    } catch (error){
        console.log(error);
        response.status(403).send(failure403);
    }

});

app.post("/store/get_all", function (request, response) {

    try {

        var businessID = request.body["business_id"];

        if (parametersInvalid([businessID])){

            firebase.database().ref("businesses/" +businessID+ "/store_manifest").once("value").then(function (_snapshot) {
                if (_snapshot.exists()){

                    response.status(200).send(_snapshot.val());

                } else {

                    response.status(500).send("Internal Error");

                }
            });

        } else {

        }

    } catch (error){
        console.log(error);
        response.status(403).send(paramError);
    }

});

app.post("/store/add", function(request, response){
    try {

        var user_id = request.body["user_id"];
        var business_id = request.body["business_id"];
        var store_phone = request.body["store_phone"];
        var store_email = request.body["store_email"];
        var lat = request.body["lat"];
        var long = request.body["long"];
        var store_id = request.body["new_store_id"];
        var title = "Store";
        var location_name = request.body["location_name"];
        var business_name = request.body["business_name"];
        var currentStoreID = request.body["current_store_id"];

        if(parametersInvalid([user_id, business_id, store_phone, store_email, lat, long, store_id, location_name,
                business_name, currentStoreID])){

            throw paramError;

        } else {

            doesBusinessExist(business_id, function (exists) {
               if (exists){
                   var user_updated = false;
                   var business_updated = false;
                   var manifest_updated = false;
                   var algolia_updated = false;

                   var completed_timezone_query = false;
                   var completed_geocode_query = false;

                   var uct_offset;
                   var geo_address;

                   function sendResponse(){
                       if(user_updated && business_updated && manifest_updated && algolia_updated){
                           response.status(200).send(success200);

                           buildEmail_StoreAdded(user_id, geo_address, location_name, business_name, function (email) {

                               mailgun.messages().send(email, function(error, body){

                                   if (error){
                                       console.log(error);
                                   }

                               });

                           });

                       }
                   }

                   var geocodeURL = "https://maps.googleapis.com/maps/api/geocode/json?" +
                       "latlng="+lat+","+long+
                       "&key=" + geocodeAPIKEY;
                   var timezoneURL = "https://maps.googleapis.com/maps/api/timezone/json?" +
                       "location="+lat+","+long+"&timestamp=1331161200&key=" +timezoneAPIKEY;

                   performRequest(geocodeURL, function (error, res, body) {
                       if (!error && res.statusCode == 200){

                           var body = JSON.parse(body);
                           geo_address = body.results[2].formatted_address;
                           completed_geocode_query = true;
                           continueRequest();

                       } else {
                           console.log(error);
                           response.status(500).send(failure500);
                       }
                   });

                   performRequest(timezoneURL, function (error, res, body) {
                       if (!error && res.statusCode == 200){

                           var body = JSON.parse(body);
                           uct_offset = Number(body.rawOffset) / 3600;
                           completed_timezone_query = true;
                           continueRequest();

                       } else {
                           console.log(error);
                           response.status(500).send(failure500);
                       }
                   });

                   function continueRequest() {
                       if(completed_geocode_query && completed_timezone_query){
                           // Build new store item for algolia
                           // Get existing items entry and copy it into the new store.
                           algolia_index.getObject(currentStoreID, ["items", "category"], function (error, content) {
                               if (error){
                                   response.status(500).send("Internal Error");
                               } else {

                                   var items = content["items"];
                                   var category = content["category"];

                                   algoliaAddStore(business_name, store_id, business_id, location_name, geo_address, lat, long, items, category,
                                       function (error, updated) {

                                           if (updated){
                                               algolia_updated = true;
                                               sendResponse();
                                           }
                                       });

                               }
                           });

                           // add new store to store manifest
                           firebase.database().ref("businesses/" +business_id+ "/store_manifest/").update(function () {

                               var _obj = {};

                               if (location_name !== ""){
                                   _obj[store_id] = location_name + ", " + geo_address;
                               } else {
                                   _obj[store_id] = geo_address;
                               }

                               return _obj;

                           }()).then(function () {
                               manifest_updated = true;
                               sendResponse();
                           }, function (error) {
                               console.log(error);
                               response.status(500).send(failure500);
                           });

                           // Get the timezone and location name for that store

                           // create the store object
                           firebase.database().ref("businesses/" + business_id + "/stores/" + store_id+ "/").update(buildStore("placeholder.png", lat, long, store_email, store_phone, title, location_name, geo_address, uct_offset)).then(function () {

                               business_updated = true;
                               sendResponse();

                           }, function (error) {
                               console.log(error);
                               response.status(500).send(failure500);
                           });
                       }
                   }

                   // update the member manifest
                   updateMemberManifest(business_id, store_id, function (error, success) {
                       if (error !== null){
                           console.log(error);
                           response.status(500).send(failure500);
                       } else {
                           user_updated = true;
                           sendResponse();
                       }
                   });

               } else {
                   response.status(404).send(failure404);
               }
            });

        }

    } catch (e) {
        console.log(e);
        response.status(403).send(failure403);
    }
});

app.post("/business/remove/email", function(request, response){
    try {

        var business_id = request.body["business_id"];
        var user_id = request.body["user_id"];

        if (parametersInvalid([business_id, user_id])) {
            throw paramError;
        } else {

            doesBusinessExist(business_id, function (exists) {
               if (exists){
                   var email_sent = false;

                   function sendResponse(){
                       if (email_sent){
                           response.status(200).send(success200);
                       }
                   }

                   // validate that there is a confirmation link attached to this request
                   firebase.database().ref("confirmation_links/remove_business/" +business_id).once("value").then(function(snapshot){

                       if (snapshot.exists()){

                           if (Date.now() < snapshot.val()){

                               businessNameForBusinessID(business_id, function(value){
                                   if (value){

                                       firebase.database().ref("businesses/" +business_id+ "/members/").orderByValue().equalTo("owner").on("child_added", function (snapshot) {

                                           firebase.database().ref("businesses/" +business_id+ "/members/").off("child_added");

                                           firebase.database().ref("users/" +snapshot.key+ "details/email").once("value").then(function (snapshot) {

                                               var link = "http://www.stackle.co.za/remove_business/" +business_id+ "/36";

                                               var mail = {
                                                   from: "Stackle <noreply@stackle.co.za>",
                                                   to: snapshot.val(),
                                                   subject: "Remove Business Request",
                                                   text:

                                                   "Hi," +
                                                   "We have received a request to delete " +value+ "." +
                                                   "" +
                                                   "Please note that if a business is delete none of its data can be recovered" +
                                                   "" +
                                                   "If you still wish to remove this business please visit this link: "+link+", if you do not wish to delete this business you may ignore this message" +
                                                   "If you did not make this request or authorise anyone to remove this business please contact us immediately at:" +
                                                   "Email: info@stackle.co.za"+
                                                   "Phone: 021 851 2820"+
                                                   "Sincerely" +
                                                   "" +
                                                   "Stackle pty Ltd"

                                               };
                                               
                                               ejs.renderFile(__dirname+ "/views/emails/removeBusinessRequest.ejs", {businessName: value, removeLink: link}, function (error, body) {
                                                   error ? console.log(error) : mail["html"] = body;

                                                   mailgun.messages().send(mail, function (error, body) {
                                                       if (error) {
                                                           console.log(error);
                                                           response.status(500).send(failure500);
                                                       } else {
                                                           email_sent = true;
                                                           sendResponse();
                                                       }
                                                   });
                                                   
                                               });

                                           });
                                       });

                                   } else {
                                       response.status(500).send(failure500);
                                   }
                               });

                           } else {
                               firebase.database().ref("confirmation_links/remove_business/" +business_id).remove().then(function () {
                                   response.status(500).send(failure500);
                               });
                           }

                       } else {
                           response.status(500).send(failure500);
                       }

                   });

               } else {
                   response.status(404).send(failure404);
               }
            });

        }

    } catch (error) {
        console.log(error);
        response.status(403).send(failure403);
    }
});

app.post("/store/remove/email", function (request, response) {
    try {

        var business_id = request.body["business_id"];
        var store_id = request.body["store_id"];
        var user_id = request.body["user_id"];

        if (parametersInvalid([business_id, store_id, user_id])) {
            throw paramError;
        } else {

            doesStoreExist(business_id, store_id, function (exists) {
               if (exists){
                   var removeID = getLinkExpiryPeriod();
                   var confirmation_created = false;
                   var email_sent = false;

                   function sendResponse(){
                       if (confirmation_created && email_sent){
                           response.status(200).send(success200);
                       }
                   }

                   var hasBusinessName = false;
                   var hasStoreLocation = false;

                   var business_name = "The Bazaar";
                   var store_location = "Egypt - Giza - BC 5000";

                   function sendEmail(){
                       if (hasBusinessName && hasStoreLocation) {
                           firebase.database().ref("confirmation_links/remove_store/").update(function () {

                               var _obj = {};
                               _obj[store_id] = removeID;
                               return _obj;

                           }()).then(function () {

                               confirmation_created = true;
                               sendResponse();

                           }, function (error) {
                               console.log(error);
                               response.status(500).send(failure500);
                           });

                           firebase.database().ref("businesses/" +business_id+ "/members/").orderByValue().equalTo("owner").on("child_added", function (snapshot) {

                               firebase.database().ref("businesses/" +business_id+ "/members/").off("child_added");

                               firebase.database().ref("users/" +snapshot.key+ "details/email").once("value").then(function (snapshot) {

                                   var link = "http://www.stackle.co.za/remove/" +business_id+ "/" +store_id+ "/" + removeID+ "/0";

                                   var mail = {
                                       from: "Stackle <noreply@stackle.co.za>",
                                       to: snapshot.val(),
                                       subject: "Remove Store Request",
                                       text:

                                       "Hi," +
                                       "" +
                                       "A request has been made to remove a store in " +store_location+ " from " +business_name+ "." +
                                       "" +
                                       "Once a store has been removed its data can not be recovered..." +
                                       "" +
                                       "If you still wish to remove this store please please visit this link: "+link+"." +
                                       "" +
                                       "If you do not wish to remove this store please ignore this message." +
                                       "" +
                                       "This link will expire in 24 hours." +
                                       "" +
                                       "" +
                                       "Sincerely" +
                                       "" +
                                       "Stackle pty Ltd"

                                   };

                                   ejs.renderFile(__dirname +"/views/emails/RemoveStoreRequest.ejs", {businessName: business_name, storeLoc: store_location, removeLink: link}, function (error, body) {
                                      error ? console.log(error) : mail["html"] = body;

                                       mailgun.messages().send(mail, function (error, body) {
                                           if (error) {
                                               console.log(error);
                                               response.status(500).send(failure500);
                                           } else {
                                               email_sent = true;
                                               sendResponse();
                                           }
                                       });

                                   });

                               });
                           });
                       }
                   }

                   function isAllowedToRemove(){
                       businessNameForBusinessID(business_id, function (value) {
                           if (value){
                               hasBusinessName = true;
                               business_name = value;
                               sendEmail();
                           } else {
                               response.status(500).send(failure500);
                           }
                       });

                       storeLocationForIDS(business_id, store_id, function (value) {
                           if (value){
                               hasStoreLocation = true;
                               store_location = value;
                               sendEmail();
                           } else {
                               response.status(500).send(failure500);
                           }
                       });
                   }

                   firebase.database().ref("businesses/" +business_id+ "/store_manifest").once("value").then(function (snapshot) {

                       if (snapshot.exists()){

                           if (Object.keys(snapshot.val()).length > 1){
                               isAllowedToRemove();
                           } else {
                               firebase.database().ref("confirmation_links/remove_business").update(function () {

                                   var obj = {};
                                   obj[business_id] = getLinkExpiryPeriod();
                                   return obj;

                               }()).then(function () {

                                   response.status(202).send(success202);

                               }, function (error) {

                                   response.status(500).send(failure500);

                               });

                           }

                       } else {
                           response.status(500).send(failure500);
                       }

                   });
               } else {
                   response.status(404).send(failure404);
               }
            });

        }

    } catch (error) {
        console.log(error);
        response.status(403).send(failure403);
    }
});

app.post("/image", function(request, response){
    try {

        var business_id = request.body["business_id"];
        var user_id = request.body["user_id"];
        var image_id = request.body["image_id"];
        var store_id = request.body["store_id"];
        var image_url = request.body["image_url"];

        if (parametersInvalid([business_id, user_id, image_id, store_id, image_url])) {
            throw paramError;
        } else {

            doesStoreExist(business_id, store_id, function (exists) {
                if (exists){
                    var manifest_updated = false;
                    var count_updated = false;

                    function sendResponse() {
                        if(manifest_updated && count_updated){
                            response.status(200).send(success200);
                        }
                    }

                    //update count
                    firebase.database().ref("businesses/" +business_id+ "/stores/" +store_id+ "/images_official").once("value").then(function (snapshot) {
                        var newCount = 0;

                        if(snapshot.exists()){
                            newCount = snapshot.val() + 1;
                        } else {
                            newCount = 1;
                        }

                        firebase.database().ref("businesses/" +business_id+ "/stores/" +store_id+ "/").update({

                            "images_official": newCount

                        }).then(function () {

                            count_updated = true;
                            sendResponse();

                        }, function (error) {
                            console.log(error);
                            response.status(500).send(failure500);
                        });
                    });

                    // update manifest
                    firebase.database().ref("businesses/" + business_id + "/stores/" + store_id + "/images/official/" + image_id + "/").update({

                        uploader: user_id,
                        image_url: image_url,
                        created: Date.now(),
                        created_neg: -Date.now(),
                        likes: 0

                    }).then(function () {

                        manifest_updated = true;
                        sendResponse();

                    }, function (error) {
                        console.log(error);
                        response.status(403).send(failure500);
                    });
                } else {
                    response.status(404).send(failure404);
                }
            });

        }


    } catch (e) {
        response.status(403).send(failure403);
    }
});

app.post("/image/delete", function(request, response){
    try {

        var business_id = request.body["business_id"];
        var image_id = request.body["image_id"];
        var store_id = request.body["store_id"];

        if(parametersInvalid([business_id, image_id, store_id])){
            throw paramError;
        } else {

            var manifest_updated = false;
            var count_updated = false;
            var final_remove_updated = false;

            function sendResponse() {
                if(manifest_updated && count_updated && final_remove_updated){
                    response.status(200).send(success200);
                }
            }

            //final remove request
            firebase.database().ref("permanent_delete/images/" + Date.now()).update({
                business_id: business_id,
                image_id: image_id,
                store_id: store_id
            }).then(function(){
                final_remove_updated = true;
                sendResponse();
            }, function(error){
                console.log(error);
                response.status(500).send(failure500);
            });

            //update count
            firebase.database().ref("businesses/" +business_id+ "/stores/" +store_id+ "/images_official").once("value").then(function (snapshot) {

                var newCount = 0;
                if(snapshot.exists()){
                    newCount = snapshot.val() - 1;
                }

                if (newCount < 0){
                    newCount = 0;
                }

                firebase.database().ref("businesses/" +business_id+ "/stores/" +store_id+ "/").update({
                    "images_official": newCount
                }).then(function () {

                    count_updated = true;
                    sendResponse();

                }, function (error) {
                    console.log(error);
                    response.status(500).send(failure500);
                });

            });

            deleteImage(business_id, store_id, "official", image_id, "", function (error) {

                if (error){
                    response.status(500).send(failure500);
                } else {
                    manifest_updated = true;
                    sendResponse();
                }

            });


        }

    } catch (e) {
        console.log(e);
        response.status(403).send(failure403);
    }
});

app.post("/item_group", function(request, response){
    try {

        var business_id = request.body["business_id"];
        var item_group_id = request.body["group_id"];
        var title = request.body["title"];
        var image_url = request.body["url"];
        var type = request.body["type"];

        if (parametersInvalid([business_id, item_group_id, title, image_url, type])) {
            throw paramError;
        } else {

            doesBusinessExist(business_id, function (exists) {
                if (exists){
                   var group_updated = false;
                   var hasTypes_updated = false;
                   var algolia_updated = false;

                   var has_all_stores = false;
                   var has_algolia_object = false;

                   var allStoreIDS = [];
                   // Will only use this if existing data needs to be updated on Algolia
                   var algoliaItemObject = {};

                   function sendResponse (){
                       if (group_updated && hasTypes_updated && algolia_updated){
                           response.status(200).send(success200);
                       }
                   }

                   function _updateAlgolia() {
                       if(has_all_stores && has_algolia_object){

                           updateAlgolia(allStoreIDS, algoliaItemObject, function (error) {

                               if (error === null) {
                                   algolia_updated = true;
                                   sendResponse();
                               } else {
                                   response.status(500).send(failure500);
                               }

                           });

                       }
                   }

                   //get the store manifest
                   firebase.database().ref("businesses/" +business_id+ "/store_manifest").once("value").then(function (snapshot) {
                       if (snapshot.exists()){

                           allStoreIDS = Object.keys(snapshot.val());
                           has_all_stores = true;
                           _updateAlgolia();

                           // check for item existing
                           firebase.database().ref("businesses/" +business_id+ "/" +type+ "/" +item_group_id+ "/title").once("value").then(function(snapshot){

                               var _algoliaObject = {
                                   objectID: allStoreIDS[0]
                               };

                               // if its an existing object and the titles dont match - update reference
                               if (snapshot.exists() && snapshot.val() != title) {
                                   algolia_index.getObject(allStoreIDS[0], ["items"] , function(error, content){


                                       var obj = content["items"];

                                       for(var i = 0; i < obj.length; i++){
                                           if (obj[i] == snapshot.val()){
                                               obj[i] = title;
                                               break;
                                           }
                                       }

                                       _algoliaObject["items"] = obj;

                                       algoliaItemObject = _algoliaObject;
                                       has_algolia_object = true;
                                       _updateAlgolia();

                                   });

                                   //if its a new object create a new reference
                               } else if (!snapshot.exists()){

                                   _algoliaObject["items"] = {
                                       value: title,
                                       _operation: "Add"
                                   };

                                   algoliaItemObject = _algoliaObject;
                                   has_algolia_object = true;
                                   _updateAlgolia();

                               } else {
                                   algolia_updated = true;
                                   sendResponse();
                               }

                               //update the item in the database
                               firebase.database().ref("businesses/" +business_id+ "/" +type+ "/" +item_group_id+ "/").update({
                                   title: title,
                                   image: image_url
                               }).then(function(){
                                   group_updated = true;
                                   sendResponse();
                               }, function(error){
                                   if (error){
                                       console.log(error);
                                       response.status(500).send(failure500);
                                   }
                               });

                           });

                       } else {

                           console.log("ERROR: Store manifest not found");
                           response.status(500).send(failure500);

                       }
                   });

                   //update has types
                   firebase.database().ref("businesses/" +business_id+ "/has_types/").update(function () {

                       var obj = {};
                       obj[type] = true;
                       return obj;

                   }()).then(function () {

                       hasTypes_updated = true;
                       sendResponse();

                   }, function (error) {
                       console.log(error);
                       response.status(500).send(failure500);
                   });

               } else {
                   response.status(404).send(failure404);
               }
            });

        }
    } catch (e) {
        console.log(e);
        response.status(403).send(failure403);
    }
});

app.post("/item_group/get", function(request, response){
    try {

        var business_id = request.body["business_id"];
        var store_id = request.body["store_id"];
        var type = request.body["type"];

        if (parametersInvalid([business_id, store_id, type])){

            throw paramError;

        } else {

            var store_group_keys = undefined;
            var all_products = undefined;
            var response_obj = {};

            function buildResponse() {
                if (store_group_keys != undefined && all_products != undefined) {

                    store_group_keys.forEach(function (key) {
                        response_obj[key] = all_products[key];
                    });

                    response_obj["code"] = 200;

                    response.status(200).send(response_obj);
                }
            }

            //get all products from store
            firebase.database().ref("businesses/" +business_id+ "/stores/" +store_id+ "/" +type+ "/").once("value").then(function (snapshot) {
                if (snapshot.exists()) {

                    store_group_keys = Object.keys(snapshot.val());
                    buildResponse();

                } else {
                    response.status(200).send(success200);
                }
            });

            //get all products from business
            firebase.database().ref("businesses/" +business_id+ "/" +type+ "/").once("value").then(function (snapshot) {
                if (snapshot.exists()){

                    all_products = snapshot.val();
                    buildResponse();

                } else {
                    response.status(200).send(success200);
                }
            });
        }

    } catch (e){
        console.log(e);
        response.status(403).send(failure403);
    }
});

app.post("/item", function(request, response){
    try {

        var business_id = request.body["business_id"];
        var item_group_id = request.body["item_group_id"];
        var title = request.body["title"];
        var image_url = request.body["image_url"];
        var type = request.body["item_type"];
        var item_id = request.body["item_id"];
        var price = request.body["price"];
        var description = request.body["description"];

        if (parametersInvalid([business_id, item_group_id, title, image_url, type, item_id, price, description])){
            throw paramError;
        } else {

            doesItemGroupExist(business_id, type, item_group_id, function(exists){
               if (exists){
                   var count_updated = false;
                   var manifest_updated = false;
                   var algoliaUpdated = false;
                   var type_count_updated = false;

                   var has_algolia_object = false;
                   var has_store_manifest = false;

                   var storeIDS = [];
                   var algoliaObject = {};


                   function sendResponse() {
                       if (count_updated && manifest_updated && algoliaUpdated && type_count_updated) {
                           response.status(200).send(success200);
                       }
                   }

                   function _updateAlgolia() {
                       if(has_algolia_object && has_store_manifest){

                           updateAlgolia(storeIDS, algoliaObject, function (error) {
                               if(error === null){
                                   algoliaUpdated = true;
                                   sendResponse();
                               } else {
                                   response.status(500).send(failure500);
                               }
                           });

                       }
                   }

                   // Get the store manifest
                   firebase.database().ref("businesses/" +business_id+ "/store_manifest").once("value").then(function (snapshot) {

                       if(snapshot.exists()){

                           storeIDS = Object.keys(snapshot.val());
                           has_store_manifest = true;
                           _updateAlgolia();

                           //check if item exists already
                           firebase.database().ref("businesses/" +business_id+ "/" +type+ "_manifest/" +item_group_id+ "/" +item_id+ "/title").once("value").then(function(snapshot){

                               //update title if they don't match
                               if (snapshot.exists() && snapshot.val() != title) {
                                   count_updated = true;
                                   type_count_updated = true;

                                   algolia_index.getObject(storeIDS[0], ["items"], function (error, content) {
                                       if (error){
                                           response.status(500).send(failure500);
                                       } else {

                                           var obj = content["items"];

                                           for(var i = 0; i < obj.length; i++){
                                               if (obj[i] == snapshot.val()){
                                                   obj[i] = title;
                                                   break;
                                               }
                                           }

                                           var _algoliaObject = {
                                               objectID: store_id
                                           };

                                           _algoliaObject["items"] = obj;


                                           algoliaObject = _algoliaObject;
                                           has_algolia_object = true;
                                           _updateAlgolia();

                                       }
                                   });
                                   //create new object if snapshot doesnt exist
                               } else if (!snapshot.exists()){

                                   //add new title to items in algolia index
                                   var _algoliaObject = {
                                       objectID: storeIDS[0]
                                   };

                                   _algoliaObject["items"] = {
                                       value: title,
                                       _operation: "Add"
                                   };

                                   algoliaObject = _algoliaObject;
                                   has_algolia_object = true;
                                   _updateAlgolia();

                                   //increase item count in group
                                   firebase.database().ref("businesses/" +business_id+ "/" +type+ "/" +item_group_id+ "/items").once("value").then(function (snapshot) {
                                       var count = 1;
                                       if (snapshot.exists()){

                                           count = snapshot.val() + 1;

                                       }

                                       firebase.database().ref("businesses/" +business_id+ "/" +type+ "/" +item_group_id).update({
                                           items: count
                                       }).then(function () {
                                           count_updated = true;
                                           sendResponse();
                                       }, function (error) {
                                           console.log(error);
                                           response.status(500).send(failure500)
                                       })
                                   });

                                   //update item type count
                                   firebase.database().ref("businesses/" +business_id+ "/" +type+ "_item_count/").once("value").then(function (snapshot) {

                                       var count = 1;

                                       if (snapshot.exists()){

                                           count = snapshot.val() + 1;

                                       }

                                       firebase.database().ref("businesses/" +business_id+ "/").update(function () {
                                           var obj = {};
                                           obj[type + "_item_count"] = count;
                                           return obj;
                                       }()).then(function () {

                                           type_count_updated = true;
                                           sendResponse();

                                       }, function (error) {
                                           console.log(error);
                                           response.status(500).send(failure500);
                                       });

                                   });

                                   //snapshot does exist and titles match. Dont change anything on Algolia or item count.
                               } else {
                                   count_updated = true;
                                   algolia_updated = true;
                                   type_count_updated = true;
                                   sendResponse();
                               }

                               //update item manifest
                               firebase.database().ref("businesses/" + business_id + "/" + type + "_manifest/" + item_group_id + "/" + item_id + "/").update({
                                   title: title,
                                   image: image_url,
                                   price: price,
                                   description: description
                               }).then(function () {
                                   manifest_updated = true;
                                   sendResponse();
                               }, function (error) {
                                   if (error) {
                                       console.log(error);
                                       response.status(500).send(failure500);
                                   }
                               });


                           });

                       } else {
                           response.status(500).send(failure500);
                       }

                   });

               } else {
                   response.status(404).send(failure404);
               }
            });

        }

    } catch (e) {
        console.log(e);
        response.status(403).send(failure403);
    }
});

app.post("/item_group/delete", function(request, response){
    try {

        var business_id = request.body["business_id"];
        var item_group_id = request.body["item_group_id"];
        var type = request.body["item_type"];

        if (parametersInvalid([business_id, item_group_id, type])){

            throw paramError;

        } else {

            var group_updated = false;
            var manifest_updated = false;
            var algolia_updated = false;
            var likes_registry_updated = false;
            var item_type_counted = false;

            var newItemsIndex = [];
            var storeIDS = [];

            var hasAlgoliaData = false;
            var hasStoreManifest = false;

            var storesToUpdate;

            function sendResponse() {
                if (group_updated && manifest_updated && algolia_updated && likes_registry_updated && item_type_counted) {
                    response.status(200).send(success200);

                    firebase.database().ref("permanent_delete/item_group").update({
                        "business_id": business_id,
                        "type": type,
                        "item_group_id": item_group_id
                    });

                }
            }

            //update algolia
            function _updateAlgolia() {
                if (hasAlgoliaData && hasStoreManifest) {

                    storesToUpdate = storeIDS.length;

                    storeIDS.forEach(function (store_id) {
                        algolia_index.partialUpdateObject({
                            objectID: store_id,
                            items: newItemsIndex
                        }, function (error, cont) {
                            if (error) {
                                response.status(500).send(failure500);
                                console.log(error);
                            } else {
                                storesToUpdate -= 1;
                                setAlgoliaUpdated();
                            }
                        });
                    });
                }
            }

            // set algolia updated
            function setAlgoliaUpdated(){
                if (storesToUpdate == 0) {
                    algolia_updated = true;
                    sendResponse();
                }
            }

            //update likes registry
            firebase.database().ref("likes_registry/businesses/" +business_id+ "/" +type+ "/" +item_group_id).remove().then(function () {

                likes_registry_updated = true;
                sendResponse();

            }, function (error) {
                console.log(error);
                response.status(500).send(failure500);
            });

            //get store manifest
            firebase.database().ref("businesses/" +business_id+ "/store_manifest").once("value").then(function (snapshot) {
                if(snapshot.exists()){

                    storeIDS = Object.keys(snapshot.val());
                    hasStoreManifest = true;
                    _updateAlgolia();

                    // build new item search index
                    // get group title
                    firebase.database().ref("businesses/" +business_id+ "/" +type+ "/" +item_group_id+ "/title").once("value").then(function (snapshot) {

                            if (snapshot.exists()) {

                                // get item titles
                                firebase.database().ref("businesses/" + business_id + "/" + type + "_manifest/" +
                                    item_group_id).once("value").then(function (item_snapshot) {

                                    if (item_snapshot.exists()) {

                                        //count the number of items to remove from type counter

                                        var itemCount = Object.keys(item_snapshot.val()).length;

                                        firebase.database().ref("businesses/" +business_id+ "/" +type+"_item_count").once("value").then(function (snapshot) {
                                            var count = 0;

                                            if (snapshot.exists()){

                                                count = snapshot.val() - itemCount;

                                                if (count < 0) {
                                                    count = 0;
                                                }

                                            }

                                            firebase.database().ref("businesses/" +business_id+ "/").update(function () {

                                                var obj = {};
                                                obj[type+ "_item_count"] = count;
                                                return obj;

                                            }()).then(function () {

                                                item_type_counted = true;
                                                sendResponse();

                                            }, function (error) {
                                                console.log(error);
                                                response.status(500).send(failure500);
                                            });


                                        });


                                        //get items in the existing index
                                        algolia_index.getObject(storeIDS[0], ["items"], function (error, content) {
                                            if (error) {
                                                console.log(error);
                                                response.status(500).send("Internal Error");
                                            } else if (content["items"] !== undefined){

                                                var algoliaItems = content["items"];

                                                var itemsToRemove = [snapshot.val()];
                                                Object.keys(item_snapshot.val()).forEach(function (itemKey) {
                                                    itemsToRemove.push(item_snapshot.val()[itemKey]["title"]);
                                                });

                                                for (var i = 0; i < algoliaItems.length; i++){
                                                    if(!itemsToRemove.includes(algoliaItems[i])){
                                                        newItemsIndex.push(algoliaItems[i]);
                                                    } else {
                                                        itemsToRemove[itemsToRemove.indexOf(algoliaItems[i])] = null;
                                                    }
                                                }

                                                hasAlgoliaData = true;
                                                _updateAlgolia();

                                            }
                                        });

                                    } else {

                                        item_type_counted = true;

                                        algolia_index.getObject(storeIDS[0], ["items"], function (error, content) {
                                            if (error) {
                                                console.log(error);
                                                response.status(500).send("Internal Error");
                                            } else if (content["items"] !== undefined){

                                                var algoliaItems = content["items"];

                                                var itemsToRemove = [snapshot.val()];

                                                for (var i = 0; i < algoliaItems.length; i++){
                                                    if(!itemsToRemove.includes(algoliaItems[i])){
                                                        newItemsIndex.push(algoliaItems[i]);
                                                    } else {
                                                        itemsToRemove[0] = null;
                                                    }
                                                }

                                                hasAlgoliaData = true;
                                                _updateAlgolia();

                                            }
                                        });

                                    }

                                    // update manifest
                                    firebase.database().ref("businesses/" +business_id+ "/" +type+ "_manifest/" +item_group_id).remove().then(function () {

                                        manifest_updated = true;
                                        sendResponse();

                                    }, function (error) {
                                        console.log(error);
                                        response.status(500).send(failure500);
                                    });

                                });

                                // update group
                                firebase.database().ref("businesses/" +business_id+ "/" +type+ "/" +item_group_id).remove().then(function () {

                                    group_updated = true;
                                    sendResponse();

                                }, function (error) {
                                    console.log(error);
                                    response.status(500).send(failure500);
                                });

                            } else {
                                item_type_counted = true;
                                hasAlgoliaData = true;
                                _updateAlgolia();

                                manifest_updated = true;
                                group_updated = true;

                                sendResponse();
                            }

                        }
                    );

                } else {
                    response.status(500).send(failure500);
                }
            });

        }

    } catch (e) {
        response.status(403).send(failure403);
    }
});

app.post("/item/delete", function(request, response){
    try {

        var business_id = request.body["business_id"];
        var item_group_id = request.body["item_group_id"];
        var type = request.body["item_type"];
        var item_id = request.body["item_id"];

        if (parametersInvalid([business_id, item_group_id, type, item_id])){
            throw paramError;
        } else {

            var count_updated = false;
            var manifest_updated = false;
            var algolia_updated = false;
            var likes_registry_updated = false;
            var item_type_count_updated = false;

            var storeIDS = [];
            var algoliaObject = {};

            var has_store_manifest = false;
            var has_algolia_object = false;

            function sendResponse() {
                if (count_updated && manifest_updated && algolia_updated && likes_registry_updated && item_type_count_updated) {
                    response.status(200).send(success200);

                    firebase.database().ref("permanent_delete/item").update({
                        "business_id": business_id,
                        "type": type,
                        "item_id": item_id,
                        "item_group_id": item_group_id
                    });
                }
            }

            function _updateAlgolia() {
                if(has_store_manifest && has_algolia_object){

                    updateAlgolia(storeIDS, algoliaObject, function (error) {
                       if(error === null){

                           algolia_updated = true;
                           sendResponse();

                       } else {
                         response.status(500).send(failure500);
                       }
                    });

                }
            }

            firebase.database().ref("businesses/" +business_id+ "/" +type+ "_item_count").once("value").then(function (snapshot) {

                var count = 0;

                if (snapshot.exists()){
                    count = snapshot.val() - 1;

                    if (count < 0){
                        count = 0;
                    }
                }

                firebase.database().ref("businesses/" +business_id+ "/").update(function () {
                    var obj = {};
                    obj[type + "_item_count"] = count;
                    return obj;
                }()).then(function () {

                    item_type_count_updated = true;
                    sendResponse();

                }, function (error) {
                    console.log(error);
                    response.status(500).send(failure500);
                });

            });

            // get store IDS
            firebase.database().ref("businesses/" +business_id+ "/store_manifest").once("value").then(function (snapshot) {

                if(snapshot.exists()){

                    storeIDS = Object.keys(snapshot.val());
                    has_store_manifest = true;
                    _updateAlgolia();

                    // get title of item
                    firebase.database().ref("businesses/" +business_id+ "/" +type+ "_manifest/" +item_group_id+ "/" +item_id+ "/title" +
                        "").once("value").then(function (snapshot) {

                        if (snapshot.exists()){

                            // algolia updated
                            algolia_index.getObject(storeIDS[0], ["items"], function (error, content) {
                                if (error){
                                    response.status(500).send(failure500);
                                    console.log(error);
                                } else {

                                    var _obj = content["items"];
                                    var obj = [];

                                    var indexOfEle = _obj.indexOf(snapshot.val());

                                    for(var i = 0; i < _obj.length; i++){
                                        if (i != indexOfEle){
                                            obj.push(_obj[i]);
                                        } else {
                                            indexOfEle = null;
                                        }
                                    }

                                    var _algoliaObject = {
                                        objectID: storeIDS[0]
                                    };

                                    _algoliaObject["items"] = obj;


                                    algoliaObject = _algoliaObject;
                                    has_algolia_object = true;
                                    _updateAlgolia();

                                }
                            });
                        } else {
                            algolia_updated = true;
                        }

                        // remove from item manifest
                        firebase.database().ref("businesses/" +business_id+ "/" +type+ "_manifest/" +item_group_id+ "/" +item_id).remove().then(function () {
                            manifest_updated = true;
                            sendResponse();
                        }, function (error) {
                            console.log(error);
                            response.status(500).send(failure500);
                        });

                    });

                } else {
                    response.status(500).send(failure500);
                }

            });

            //update likes registry
            firebase.database().ref("likes_registry/businesses/" +business_id+ "/" +type+ "/" +item_group_id+ "/items/" +item_id).remove().then(function () {
                likes_registry_updated = true;
                sendResponse();
            }, function (error) {
                console.log(error);
                response.status(500).send(failure500);
            });

            // update count
            firebase.database().ref("businesses/" +business_id+ "/" +type+ "/" +item_group_id+ "/items").once("value").then(function (snapshot) {
                
                if (snapshot.exists()){
                    var count = 0;
                    
                    if (snapshot.val() != 0) {
                        count = snapshot.val() - 1
                    }
                    
                    firebase.database().ref("businesses/" +business_id+ "/" +type+ "/" +item_group_id).update({
                        items: count
                    }).then(function () {
                        count_updated = true;
                        sendResponse();
                    }, function (error) {
                        console.log(error);
                        response.status(500).send(failure500);
                    });
                } else {
                    response.status(500).send(failure500);
                }
            }, function (error) {
                console.log(error);
                response.status(500).send(failure500);
            });


        }

    } catch (e) {
        response.status(403).send(failure403);
    }
});

app.post("/contact_details/store", function(request, response){

    try {

        var title = request.body["title"];
        var email = request.body["email"];
        var phone = request.body["phone"];
        var facebook = request.body["facebook"];
        var google = request.body["google"];
        var website = request.body["website"];
        var business_id = request.body["business_id"];
        var store_id = request.body["store_id"];

        if (parametersInvalid([title, business_id, store_id]) || title != "Store"){
            throw paramError;
        } else {

            doesStoreExist(business_id, store_id, function (exists) {
               if (exists){
                   firebase.database().ref("businesses/" +business_id+ "/stores/" +store_id+ "/contact_details/" +title+ "/").update({

                       email: email,
                       phone: phone,
                       facebook: facebook,
                       google: google,
                       website: website

                   }).then(function(){

                       response.status(200).send(success200);

                   }, function(error){

                       response.status(500).send(failure500);

                   });
               } else {
                   response.status(404).send(failure404);
               }
            });



        }

    } catch (e) {
        response.status(403).send(failure403);
    }

});

app.post("/contact_details/business", function(request, response){
    try{

        var business_id = request.body["business_id"];
        var title = request.body["title"];
        var email = request.body["email"];
        var phone = request.body["phone"];
        var facebook = request.body["facebook"];
        var google = request.body["google"];
        var website = request.body["website"];

        if (parametersInvalid([business_id, title])){
            throw paramError;
        } else {

            doesBusinessExist(business_id, function (exists) {
               if (exists){
                   firebase.database().ref("businesses/" +business_id+ "/contact_details/" +title+ "/").update({

                       email: email,
                       phone: phone,
                       facebook: facebook,
                       google: google,
                       website: website

                   }).then(function () {

                       response.status(200).send(success200);

                   }, function (error) {
                       console.log("Error adding - updating business contact details: \n" + error);
                       response.status(500).send(failure500);
                   });
               } else {
                   response.status(404).send(failure404);
               }
            });

        }

    } catch (e){
        response.status(403).send(failure403);
    }
});

app.post("/contact_details/remove", function(request, response){

    try {

        var title = request.body["title"];
        var business_id = request.body["business_id"];

        if (parametersInvalid([title, business_id])){

            throw paramError;

        } else {

            firebase.database().ref("businesses/" + business_id + "/contact_details/" + title).remove().then(function () {

                response.status(200).send(success200);

            }, function (error) {

                console.log("Error removing contact details from business: \n" + error);
                response.status(500).send(failure500);

            });

        }
    } catch (e) {
        response.status(403).send(failure403);
    }
});

app.post("/business_hours/standard", function(request, response){

    try{

        var business_id = request.body["business_id"];

        var monday_open = request.body["monday_open"];
        var tuesday_open = request.body["tuesday_open"];
        var wednesday_open = request.body["wednesday_open"];
        var thursday_open = request.body["thursday_open"];
        var friday_open = request.body["friday_open"];
        var saturday_open = request.body["saturday_open"];
        var sunday_open = request.body["sunday_open"];

        var monday_close = request.body["monday_close"];
        var tuesday_close = request.body["tuesday_close"];
        var wednesday_close = request.body["wednesday_close"];
        var thursday_close = request.body["thursday_close"];
        var friday_close = request.body["friday_close"];
        var saturday_close = request.body["saturday_close"];
        var sunday_close = request.body["sunday_close"];

        if(parametersInvalid([business_id, monday_close, monday_open, tuesday_close, tuesday_open, wednesday_close, wednesday_open, thursday_close, thursday_open, friday_close, friday_open, saturday_close, saturday_open, sunday_close, sunday_open])){
            throw paramError;
        } else {

            doesBusinessExist(business_id, function (exists) {
               if (exists){
                   firebase.database().ref("businesses/" +business_id+ "/business_hours/standard/").update({

                       monday_open : monday_open,
                       tuesday_open : tuesday_open,
                       wednesday_open : wednesday_open,
                       thursday_open : thursday_open,
                       friday_open : friday_open,
                       saturday_open : saturday_open,
                       sunday_open : sunday_open,

                       monday_close : monday_close,
                       tuesday_close : tuesday_close,
                       wednesday_close : wednesday_close,
                       thursday_close : thursday_close,
                       friday_close : friday_close,
                       saturday_close : saturday_close,
                       sunday_close : sunday_close

                   }).then(function(){

                       response.status(200).send(success200);

                   }, function (error) {
                       console.log("Error updating standard business hours: \n" + error);
                       response.status(500).send(failure500);
                   });
               } else {
                   response.status(404).send(failure404);
               }
            });

        }

    } catch (e){
        response.status(403).send(failure403);
    }

});

app.post("/business_hours/holiday", function(request, response){

    try {

        var business_id = request.body["business_id"];
        var start_date = request.body["start_date"];
        var end_date = request.body["end_date"];
        var open_hour = request.body["open_hour"];
        var close_hour = request.body["close_hour"];
        var business_hours_id = request.body["business_hours_id"];

        if(parametersInvalid([business_id, start_date, end_date, open_hour, close_hour, business_hours_id])){
            throw paramError;
        } else {

            doesBusinessExist(business_id, function (exists) {
                if(exists){
                    firebase.database().ref("businesses/" +business_id+ "/business_hours/holiday/" +business_hours_id+ "/").update({

                        start_date: start_date,
                        end_date: end_date,
                        open_hour: open_hour,
                        close_hour: close_hour

                    }).then(function () {

                        response.status(200).send(success200);

                    }, function (error) {
                        console.log("Error adding - updating business holiday hours: \n" + error);
                        response.status(500).send(failure500);
                    });
                } else {
                    response.status(404).send(failure404);
                }
            });

        }

    } catch (e) {
        response.status(403).send(failure403);
    }
});

app.post("/business_hours/holiday/remove", function(request, response){
    try{

        var business_id = request.body["business_id"];
        var business_hours_id = request.body["business_hours_id"];

        if(parametersInvalid([business_id, business_hours_id])){
            throw paramError;
        } else {

            firebase.database().ref("businesses/" +business_id+ "/business_hours/holiday/" +business_hours_id).remove().then(function(){

                response.status(200).send(success200);

            }, function(error){

                console.log("Error removing business holiday hours: \n" + error);
                response.status(500).send(failure500);

            });
        }

    } catch (e){
        response.status(403).send(failure403);
    }
});

app.post("/category", function(request, response){
    try {

        var business_id = request.body["business_id"];
        var category_name = request.body["title"];
        var subcategory_name = request.body["subtitle"];

        if(parametersInvalid([business_id, category_name, subcategory_name])){
            throw paramError;
        } else {

            doesBusinessExist(business_id, function (exists) {
               if (exists){
                   var updated_categories = false;
                   var updated_algolia = false;
                   var categories_counted = false;

                   var id = subcategory_name+""+category_name;

                   function sendResponse() {
                       if (updated_algolia && updated_categories && categories_counted){
                           response.status(200).send(success200);
                       }
                   }


                   firebase.database().ref("businesses/" +business_id+ "/categories/" +id).once("value").then(function (snapshot) {
                       if (!snapshot.exists()){

                           // Get a list of all the stores
                           firebase.database().ref("businesses/" +business_id+ "/store_manifest").once("value").then(function (snapshot) {
                               if(snapshot.exists()){

                                   var storeIDS = Object.keys(snapshot.val());

                                   algolia_index.getObject(storeIDS[0], ["category"], function (error, content) {

                                       if (error !== null){

                                       } else {

                                           var obj = {};

                                           if (content["category"] === undefined || !content["category"].includes(subcategory_name)){

                                               obj = {
                                                   objectID: "",
                                                   category: {
                                                       value: subcategory_name,
                                                       _operation: "Add"
                                                   }
                                               };

                                               updateAlgolia(storeIDS, obj, function (error) {
                                                   if (error === null){
                                                       updated_algolia = true;
                                                       sendResponse();
                                                   } else {
                                                       console.log(error);
                                                       response.status(500).send(failure500);
                                                   }
                                               });

                                           } else {
                                               updated_algolia = true;
                                               sendResponse();
                                           }
                                       }

                                   });

                               } else {
                                   updated_algolia = true;
                                   sendResponse();
                               }


                           });

                           //update the business
                           firebase.database().ref("businesses/" +business_id+ "/categories/" +id+ "/").update({

                               title: category_name,
                               subtitle: subcategory_name

                           }).then(function(){
                               updated_categories = true;
                               sendResponse();
                           }, function(error){
                               console.log("Error add category to business: \n" + error);
                               response.status(500).send(failure500);
                           });

                           //get the current category count
                           firebase.database().ref("businesses/" +business_id+ "/category_count").once("value").then(function (snapshot) {

                               var count = 1;

                               if (snapshot.exists()){
                                   count = snapshot.val() + 1;
                               }

                               //update the category count
                               firebase.database().ref("businesses/" +business_id+ "/").update(function () {

                                   var obj = {};
                                   obj["category_count"] = count;
                                   return obj;

                               }()).then(function () {

                                   categories_counted = true;
                                   sendResponse();

                               }, function (error) {
                                   console.log(error);
                                   response.status(500).send(failure500);
                               });

                           }, function (error) {
                               console.log(error);
                               response.status(500).send(failure500);
                           });
                       } else {
                           categories_counted = true;
                           updated_categories = true;
                           updated_algolia = true;
                           sendResponse();
                       }
                   });
               } else {
                   response.status(404).send(failure404);
               }
            });

        }

    } catch (e) {
        response.status(403).send(failure403);
    }
});

app.post("/category/remove", function(request, response){
    try {

        var business_id = request.body["business_id"];
        var category_id = request.body["category_id"];
        var subcategory_name = request.body["subtitle"];

        if(parametersInvalid([business_id, category_id, subcategory_name])){
            throw paramError;
        } else {

            var categories_updated = false;
            var algolia_updated = false;
            var categories_counted = false;

            function sendResponse() {
                if(categories_updated && algolia_updated && categories_counted) {
                    response.status(200).send(success200);
                }
            }

            //get current category count
            firebase.database().ref("businesses/" +business_id+ "/category_count").once("value").then(function (snapshot) {

                var count = 0;

                if (snapshot.exists()){
                    count = snapshot.val() - 1;

                    if (count < 0) {
                        count = 0;
                    }
                }

                firebase.database().ref("businesses/" +business_id+ "/").update(function () {

                    var obj = {};
                    obj["category_count"] = count;
                    return obj;

                }()).then(function () {

                    categories_counted = true;
                    sendResponse();

                }, function (error) {
                    console.log(error);
                    response.status(500).send(failure500);
                });
            });

            // update algolia store objects
            firebase.database().ref("businesses/" +business_id+ "/store_manifest").once("value").then(function (snapshot) {

                if(snapshot.exists()){

                    var storeIDS = Object.keys(snapshot.val());
                    var obj = {
                        objectID: "",
                        category: {
                            value: subcategory_name,
                            _operation: "Remove"
                        }
                    };

                    updateAlgolia(storeIDS, obj, function (error) {
                       if (error === null){
                           algolia_updated = true;
                           sendResponse();
                       } else {
                           console.log(error);
                           response.status(500).send(failure500);
                       }
                    });

                } else {
                    algolia_updated = true;
                    sendResponse();
                }

            });

            //update business categories
            firebase.database().ref("businesses/" +business_id+ "/categories/" +category_id+ "/").remove().then(function(){
                categories_updated = true;
                sendResponse();
            }, function(error){
                console.log("Error deleting category from business: \n" + error);
                response.status(500).send(failure500);
            });

        }

    } catch (e) {
        response.status(403).send(failure403);
    }
});

app.post("/description", function(request, response){
    try {

        var business_id = request.body["business_id"];
        var description = request.body["description"];

        if(parametersInvalid([business_id, description])){
            throw paramError;
        } else {

            doesBusinessExist(business_id, function (exists) {
               if (exists){
                   firebase.database().ref("businesses/" +business_id+ "/").update({
                       description: description
                   }).then(function(){
                       response.status(200).send(success200);
                   }, function(error){
                       response.status(500).send(failure500);
                   });
               } else {
                   response.status(404).send(failure404);
               }
            });

        }

    } catch (e) {
        response.status(403).send(failure403);
    }
});

app.post("/name", function(request, response){
    try {

        var business_id = request.body["business_id"];
        var name = request.body["name"];

        if(parametersInvalid([business_id, name])){
            throw paramError;
        } else {

            doesBusinessExist(business_id, function (exists) {
               if (exists){
                   firebase.database().ref("businesses/" +business_id+ "/").update({
                       name: name
                   }).then(function(){
                       response.status(200).send(success200);
                   }, function(error){
                       response.status(500).send(failure500);
                   });
               } else {
                   response.status(404).send(failure404);
               }
            });

        }

    } catch (e) {
        response.status(403).send(failure403);
    }
});

app.post("/location/update", function (request, response) {

   try {

       /**
        * update_coords and update_location_name are boolean values.
        * If there are set to true then they will be updated accordingly
       */

       var business_id = request.body["business_id"];
       var store_id = request.body["store_id"];
       var lat = request.body["lat"];
       var long = request.body["long"];
       var location_name = request.body["location_name"];
       var updateCoords = request.body.update_coords;
       var updateLocationName = request.body.update_location_name;

       if (parametersInvalid([business_id, store_id, lat, long, location_name, updateCoords, updateLocationName])){

           throw paramError;

       } else {

           doesStoreExist(business_id, store_id, function (exists) {
              if (exists){
                  if (updateCoords && updateLocationName){

                      var manifest_updated = false;
                      var algolia_updated = false;
                      var location_name_updated = false;
                      var geo_address_updated = false;
                      var coordinates_updated = false;

                      function sendResponse() {
                          if (manifest_updated && algolia_updated && location_name_updated && geo_address_updated && coordinates_updated) {
                              response.status(200).send(success200);
                          }
                      }

                      var geo_address;

                      function continueRequest() {

                          //update the store manifest
                          updateStoreManifest(business_id, store_id, location_name + ", " + geo_address, function (error) {
                              if (error == null){
                                  manifest_updated = true;
                                  sendResponse();
                              } else {
                                  console.log(error);
                                  response.status(500).send(failure500);                           }
                          });

                          //update algolia
                          algolia_index.partialUpdateObject({
                              geo_address: geo_address,
                              location_name: location_name,
                              _geoloc: {
                                  lat: Number(lat),
                                  lng: Number(long)
                              },
                              objectID: store_id
                          }, function (error, content) {
                              if (error == null){

                                  algolia_updated = true;
                                  sendResponse();

                              } else {
                                  console.log(error);
                                  response.status(500).send(failure500);
                              }
                          });

                          //update the location name in the store entity
                          updateStoreLocationName(business_id, store_id, location_name, function (error) {
                              if (error == null){
                                  location_name_updated = true;
                                  sendResponse();
                              } else {
                                  console.log(error);
                                  response.status(500).send(failure500);
                              }
                          });

                          // update the coordinates for that store
                          updateStoreCoords(business_id, store_id, lat, long, function (error) {
                              if (error == null){
                                  coordinates_updated = true;
                                  sendResponse();
                              } else {
                                  console.log(error);
                                  response.status(500).send(failure500);
                              }
                          });

                          //update the geoaddress for the store
                          updateStoreGeoAddress(business_id, store_id, geo_address, function (error) {

                              if (error == null){
                                  geo_address_updated = true;
                                  sendResponse();
                              } else {
                                  console.log(error);
                                  response.status(500).send(failure500);
                              }

                          });

                      }



                      //first get the new geoaddress...
                      var geocodeURL = "https://maps.googleapis.com/maps/api/geocode/json?" +
                          "latlng="+lat+","+long+
                          "&key=" + geocodeAPIKEY;

                      performRequest(geocodeURL, function (error, res, body) {
                          if (!error && res.statusCode == 200){

                              var body = JSON.parse(body);
                              geo_address = body.results[2].formatted_address;
                              continueRequest();

                          } else {
                              console.log(error);
                              response.status(500).send(failure500);
                          }
                      });

                  } else if (updateCoords){

                      var manifest_updated = false;
                      var coordinates_updated = false;
                      var geo_address_updated = false;
                      var algolia_updated = false;

                      function sendResponse() {
                          if(manifest_updated && coordinates_updated && geo_address_updated && algolia_updated){
                              response.status(200).send(success200);
                          }
                      }

                      var geo_address;

                      function continueRequest() {
                          //update the store manifest
                          firebase.database().ref("businesses/" +business_id+ "/stores/" +store_id+ "/location_name").once("value").then(function (snapshot) {

                              if (snapshot.exists()){

                                  var location_name = snapshot.val() + ", " +geo_address;

                                  //update the store manifest
                                  updateStoreManifest(business_id, store_id, location_name, function (error) {
                                      if (error == null){
                                          manifest_updated = true;
                                          sendResponse();
                                      } else {
                                          console.log(error);
                                          response.status(500).send(failure500);                           }
                                  });

                              }

                          });

                          //update algolia

                          algolia_index.partialUpdateObject({

                              geo_address: geo_address,
                              _geoloc: {
                                  lat: Number(lat),
                                  lng: Number(long)
                              },
                              objectID: store_id

                          }, function (error, content) {
                              if (error == null){

                                  algolia_updated = true;
                                  sendResponse();

                              } else {
                                  response.status(500).send(failure500);
                                  console.log(error);
                              }
                          });

                          //update the geoaddress for the store
                          updateStoreGeoAddress(business_id, store_id, geo_address, function (error) {

                              if (error == null){
                                  geo_address_updated = true;
                                  sendResponse();
                              } else {
                                  console.log(error);
                                  response.status(500).send(failure500);
                              }

                          });

                          // update the coordinates for that store
                          updateStoreCoords(business_id, store_id, lat, long, function (error) {
                              if (error == null){
                                  coordinates_updated = true;
                                  sendResponse();
                              } else {
                                  console.log(error);
                                  response.status(500).send(failure500);
                              }
                          });

                      }

                      //first get the new geoaddress...
                      var geocodeURL = "https://maps.googleapis.com/maps/api/geocode/json?" +
                          "latlng="+lat+","+long+
                          "&key=" + geocodeAPIKEY;

                      performRequest(geocodeURL, function (error, res, body) {
                          if (!error && res.statusCode == 200){

                              var body = JSON.parse(body);
                              geo_address = body.results[2].formatted_address;
                              continueRequest();

                          } else {
                              console.log(error);
                              response.status(500).send(failure500);
                          }
                      });

                  } else if (updateLocationName){

                      var store_updated = false;
                      var manifest_updated = false;
                      var algolia_updated = false;

                      function  sendResponse() {
                          if (store_updated && manifest_updated && algolia_updated){
                              response.status(200).send(success200);
                          }
                      }

                      //update algolia
                      algolia_index.partialUpdateObject({

                          location_name: location_name,
                          objectID: store_id

                      }, function (error, content) {

                          if (error == null) {

                              algolia_updated = true;
                              sendResponse();

                          } else {
                              response.status(500).send(failure500);
                              console.log(error);
                          }

                      });

                      //update the location name in the store entity
                      updateStoreLocationName(business_id, store_id, location_name, function (error) {
                          if (error == null){
                              store_updated = true;
                              sendResponse();
                          } else {
                              console.log(error);
                              response.status(500).send(failure500);
                          }
                      });

                      //get the geo address so that we can update the store manifest
                      firebase.database().ref("businesses/" +business_id+ "/stores/" +store_id+ "/geo_address").once("value").then(function (snapshot) {

                          if (snapshot.exists()){

                              var location = location_name + ", " + snapshot.val();

                              //update the store manifest
                              updateStoreManifest(business_id, store_id, location, function (error) {
                                  if (error == null){
                                      manifest_updated = true;
                                      sendResponse();
                                  } else {
                                      console.log(error);
                                      response.status(500).send(failure500);                           }
                              });

                          }

                      });

                  } else {
                      response.status(200).send(success200);
                  }
              } else {
                   response.status(404).send(failure404);
               }
           });


       }

   } catch (e) {
       console.log(e);
       response.status(403).send(failure403);
   }

    //******** convenience functions for this call ************

    function updateStoreGeoAddress(business_id, store_id, geo_address, callback) {
        firebase.database().ref("businesses/" +business_id+ "/stores/" +store_id+ "/").update({
            geo_address: geo_address
        }).then(function () {
            callback(null);
        }, function (error) {
            callback(error);
        });
    }

    function updateStoreCoords(business_id, store_id, lat, long, callback) {
        firebase.database().ref("businesses/" +business_id+ "/stores/" +store_id+ "/-geoloc/").update({
            lat:lat,
            long:long
        }).then(function(){
            callback(null);
        }, function(error){
            callback(error);
        });
    }

    function updateStoreLocationName(business_id, store_id, location_name, callback){
        firebase.database().ref("businesses/" +business_id+ "/stores/" +store_id+ "/").update({
            location_name: location_name
        }).then(function(){

            callback(null);

        }, function(error){
            callback(error);
        });
    }

    function updateStoreManifest(business_id, store_id, location, callback){
        firebase.database().ref("businesses/" +business_id+ "/store_manifest/").update(function(){

            var _obj = {};
            _obj[store_id] = location;
            return _obj;

        }()).then(function () {

            callback(null)

        }, function (error) {
            callback(error);
        });
    }

});

app.post("/member/add", function(request, response){
    try {

        var business_id = request.body["business_id"];
        var store_id = request.body["store_id"];
        var user_email = request.body["user_email"];
        var business_name = request.body["business_name"];
        var user_role = request.body["user_role"];

        if(parametersInvalid([business_id, store_id, user_email, business_name, user_role])){
            throw paramError;
        } else {

            doesStoreExist(business_id, store_id, function (exists) {
               if (exists){
                   var mail_sent = false;
                   var confirmation_link_updated = false;

                   var confirmationID = getLinkExpiryPeriod();

                   function sendResponse() {
                       if (mail_sent && confirmation_link_updated){
                           response.status(200).send(success200);
                       }
                   }



                   firebase.database().ref("confirmation_links/add_member/" +business_id+"_"+confirmationID).update(function(){

                       var _obj = {};

                       _obj[confirmationID] = {

                           store_id: store_id,
                           user_email: user_email,
                           user_role: user_role,
                           expires: confirmationID

                       };

                       return _obj;

                   }()).then(function () {

                       confirmation_link_updated = true;
                       sendResponse();

                   }, function (error) {
                       console.log(error);
                       response.status(500).send(failure500);
                   });

                   var link = "http://www.stackle.co.za/member/confirmation/" + confirmationID+ "/" +business_id;

                   var mail = {
                       from: "Stackle <noreply@stackle.co.za>",
                       to: user_email,
                       subject: "Membership Request",
                       text:

                       "Hi,\n\n" +
                       "" +
                       "You have been sent a membership request to join " +business_name+ " as: " +user_role+ ".\n\n" +
                       "" +
                       "" +
                       "To confirm your membership of " +business_name+" please visit this link "+link+ "\n\n " +
                       "" +
                       "If you do not wish to confirm membership of this business please ignore this message.\n\n" +
                       "" +
                       "This link will expire in 24hours.\n\n" +
                       "" +
                       "Sincerely\n" +
                       "" +
                       "Stackle Pty Ltd"

                   };

                   ejs.renderFile(__dirname +"/views/emails/MemberInvitation.ejs", {businessName: business_name, memberRole: user_role, link: link}, function (error, body) {
                       error ? console.log(error): mail["html"] = body;

                       mailgun.messages().send(mail, function (error, body) {
                           if (error == null){
                               mail_sent = true;
                               sendResponse();
                           } else {
                               console.log(body);
                               response.status(500).send(failure500);
                           }
                       });
                       
                   });

               } else {
                   response.status(404).send(failure404);
               }
            });

        }

    } catch (e) {
        console.log(e);
        response.status(403).send(failure403);
    }
});

app.post("/member/confirmed", function(request, response){

    try {

        var businessID =  request.body["business_id"];
        var confirmationID = request.body["confirmation_id"];
        var userID = request.body["user_id"];
        var userEmail = request.body["user_email"];

        if (parametersInvalid([businessID, confirmationID, userID])){
            throw paramError;
        } else {

            var business_updated = false;
            var user_updated = false;
            var store_updated = false;

            function sendResponse() {
                if(business_updated && user_updated && store_updated){
                    response.status(200).send(success200);
                }
            }

            //get confirmation information

            firebase.database().ref("confirmation_links/add_member/" +confirmationID).once("value").then(function (confirmationSnapshot) {
                if(confirmationSnapshot.exists() && userEmail == confirmationSnapshot.val()["user_email"]){

                    var storeID = confirmationSnapshot.val()["store_id"];
                    var userRole = confirmationSnapshot.val()["user_role"];

                    //update store -> members manifest
                    //only store personal get placed into store members
                    if (userRole == "store admin" || userRole == "store member") {

                        doesStoreExist(businessID, storeID, function (exists) {
                           if (exists){
                               addUserToStoreRole(businessID, storeID, userID, userRole, function (error, success) {
                                   if (error !== null){
                                       console.log(error);
                                       response.status(500).send(failure500);
                                   } else {
                                       user_updated = true;
                                       store_updated = true;
                                       sendResponse();
                                   }
                               });
                           } else {
                               response.status(404).send(failure404);
                           }
                        });

                    } else {

                        doesBusinessExist(businessID, function (exists) {
                           if (exists){
                               store_updated = true;

                               addUserToBusinessRole(businessID, userID, userRole, function (error, success) {
                                   if (error !== null) {
                                       response.status(500).send(failure500);
                                       console.log(error);
                                   }  else {
                                       user_updated = true;
                                       sendResponse();
                                   }
                               });
                           } else {
                               response.status(404).send(failure404);
                           }
                        });

                    }

                } else {
                    response.status(404).send(failure404);
                }
            });
        }

    } catch (error) {
        console.log(error);
        response.status(403).send(failure403);
    }

});

app.post("/member/remove_store", function(request, response){
    try {

        var business_id = request.body["business_id"];
        var store_id = request.body["store_id"];
        var member_id = request.body["member_id"];

        if (parametersInvalid([business_id, store_id, member_id])){
            throw paramError;
        } else {

            var store_updated = false;
            var user_updated = false;

            function sendResponse(){
                if(store_updated && user_updated){
                    response.status(200).send(success200);
                }
            }

            firebase.database().ref("businesses/" +business_id+ "/stores/" +store_id+ "/members/" +member_id).remove().then(function(){
                store_updated = true;
                sendResponse();
            }, function(error){
                console.log("Error removing user from store manifest: \n" + error);
                response.status(500).send(failure500);
            });

            firebase.database().ref("users/" +member_id+ "/businesses/" +business_id+ "/" +store_id).remove().then(function () {
                user_updated = true;
                sendResponse();
            }, function (error) {
                console.log("Error removing store from user manifest: \n" + error);
                response.status(500).send(failure500);
            });

        }

    } catch (e){
        response.status(403).send(failure403);
    }
});

app.post("/member/remove_business", function(request, response){
    try{

        var business_id = request.body["business_id"];
        var user_id = request.body["user_id"];
        var user_role = request.body["user_role"].toLowerCase();

        if (parametersInvalid([business_id, user_id, user_role])){
            throw paramError;
        } else {

            var business_updated = false;
            var user_updated = false;

            function sendResponse(){
                if (business_updated && user_updated){
                    response.status(200).send(success200);
                }
            }

            if (user_role != "owner") {
                firebase.database().ref("businesses/" + business_id + "/members/" + user_id).remove().then(function () {
                    business_updated = true;
                    sendResponse();
                }, function (error) {
                    console.log("Error removing user from store manifest: \n" + error);
                    response.status(500).send(failure500);
                });

                firebase.database().ref("users/" + user_id + "/businesses/" + business_id).remove().then(function () {
                    user_updated = true;
                    sendResponse();
                }, function (error) {
                    console.log("Error removing business from user manifest: \n" + error);
                    response.status(500).send(failure500);
                });
            } else {
                response.status(400).send(failure400);
            }


        }

    } catch (e){
        response.status(403).send(failure403);
    }
});

app.post("/member/update_role", function (request, response) {

    try {

        var businessID = request.body["business_id"];
        var storeID = request.body["store_id"];
        var previous_role = request.body["previous_role"].toLowerCase();
        var new_role = request.body["new_role"].toLowerCase();
        var userID = request.body["user_id"];

        if (parametersInvalid([businessID, storeID, previous_role, new_role, userID])){
            throw paramError;
        } else {

            doesStoreExist(businessID, storeID, function (exists) {
               if (exists){
                   var business_updated = false;
                   var user_updated = false;
                   var store_updated = false;

                   function sendResponse() {
                       if (business_updated && user_updated && store_updated){
                           response.status(200).send(success200);
                       }
                   }

                   if(new_role == previous_role || new_role == "owner" || previous_role == "owner"){
                       response.status(200).send(success200);
                       //check for demotion
                   } else if ((new_role == "store admin" || new_role == "store member") && (previous_role == "admin" || previous_role == "member")){

                       store_updated = true;

                       console.log("here");
                       //remove all stores from user -> business
                       firebase.database().ref("users/" +userID+ "/businesses/" +businessID).remove().then(function () {

                           addUserToStoreRole(businessID, storeID, userID, new_role,function (error) {
                               if (error !== null){
                                   console.log(error);
                                   response.status(500).send(failure500);
                               } else {
                                   business_updated = true;
                                   sendResponse();
                               }

                           });

                       }, function (error) {
                           console.log(error);
                           response.status(500).send(failure500);
                       });

                       // remove user id from business members
                       firebase.database().ref("businesses/" +businessID+ "/members/" +userID).remove().then(function () {

                           user_updated = true;
                           sendResponse();

                       }, function (error) {
                           console.log(error);
                           response.status(500).send(failure500);
                       });

                       //check for promotion
                   } else if ((previous_role == "store admin" || previous_role == "store member") && (new_role == "admin" || new_role == "member")) {

                       addUserToBusinessRole(businessID, userID, new_role,function (error) {
                           if (error !== null){
                               console.log(error);
                               response.status(500).send(failure500);
                           } else {
                               user_updated = true;
                               sendResponse();
                           }
                       });

                       setUserRole(businessID, userID, new_role, function (error) {
                           if (error !== null){
                               console.log(error);
                               response.status(500).send(failure500);
                           } else {
                               business_updated = true;
                               sendResponse();
                           }
                       });

                       firebase.database().ref("businesses/" +businessID+ "/stores/"+storeID+"/members/" +userID).remove().then(function () {

                           store_updated = true;
                           sendResponse();

                       }, function(error){

                           console.log(error);
                           response.status(500).send(failure500);

                       });


                   } else if (new_role == "store admin" || new_role == "store member"){
                       firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/members/").update(function () {

                           var obj = {};
                           obj[userID] = new_role;
                           return obj;

                       }()).then(function () {
                           response.status(200).send(success200);
                       }, function (error) {
                           console.log(error);
                           response.status(500).send(failure500);
                       });

                   } else if (new_role == "admin" || new_role == "member"){

                       firebase.database().ref("businesses/" +businessID+ "/members/").update(function () {

                           var obj = {};
                           obj[userID] = new_role;
                           return obj;

                       }()).then(function () {
                           response.status(200).send(success200);
                       }, function (error) {
                           console.log(error);
                           response.status(500).send(failure500);
                       });

                   } else {
                       response.status(200).send(success200);
                   }

               } else {
                   response.status(404).send(failure404);
               }
            });

        }

    } catch (error) {
        console.log(error);
        response.status(403).send(failure403);
    }

});

app.post("/verification/submit", function (request, response) {

    /**
     * Posted when a user wants to have us verify their business
    */

    try {

        var business_id = request.body["business_id"];
        var registration_number = request.body["registration_id"];
        var certificate_url = request.body["url"];

        if (parametersInvalid([business_id, registration_number, certificate_url])){
            throw paramError;
        } else {

            doesBusinessExist(business_id, function (exists) {
               if (exists){
                   var business_updated = false;
                   var verification_requested = false;

                   function sendResponse () {
                       if (business_updated && verification_requested){
                           response.status(200).send(success200);
                       }
                   }

                   firebase.database().ref("businesses/" +business_id+ "/verification/").update({
                       registration_id: registration_number,
                       url: certificate_url,
                       status: "pending"
                   }).then(function () {
                       business_updated = true;
                       sendResponse();
                   }, function (error) {
                       console.log("Error adding - updating verification details: \n" +error);
                       response.status(500).send(failure500);
                   });

                   firebase.database().ref("verification_requests/businesses/" +business_id+ "/").update({
                       registration_id: registration_number,
                       url: certificate_url
                   }).then(function () {

                       verification_requested = true;
                       sendResponse();

                   }, function (error) {
                       console.log(error);
                       response.status(500).send(failure500);
                   });
               } else {
                   response.status(404).send(failure404);
               }
            });

        }

    } catch (e) {
        console.log(e);
        response.status(403).send(failure403);
    }

});

app.post("/promotion/create_update", function (request, response) {
    try {

        var businessID = request.body["business_id"];
        var promotion_text = request.body["text"];
        var begin_date = request.body["begin"];
        var end_date = request.body["end"];
        var products = request.body["products"];
        var services = request.body["services"];
        var accommodation = request.body["accommodation"];
        var userID = request.body["user_id"];

        if(parametersInvalid([businessID, promotion_text, begin_date, end_date, userID])){

            throw paramError;

        } else {

            doesBusinessExist(businessID, function (exists) {
               if (exists){
                   var promotion_updated = false;
                   var products_updated = false;
                   var services_updated = false;
                   var accommodation_updated = false;
                   var users_notified = false;

                   function sendResponse(){
                       if (promotion_updated && products_updated && services_updated && accommodation_updated && users_notified){

                           // console.log("PROMOTION UPDATED " + promotion_updated);
                           // console.log("Products UPDATED " + products_updated);
                           // console.log("Services UPDATED " + services_updated);
                           // console.log("accommodation UPDATED " + accommodation_updated);
                           // console.log("users notified UPDATED " + users_notified);

                           response.status(200).send(success200);
                       }
                   }

                   if (!isEmptyObject(products) || !isEmptyObject(services) || !isEmptyObject(accommodation)){

                       // console.log("HERE");

                       var type = {
                           text: promotion_text,
                           begin: begin_date,
                           end: end_date,
                           type: "promotion"
                       };

                       notifyAllUsers(businessID, type, function (error) {
                           if (error == null) {
                               users_notified = true;
                               sendResponse();
                           } else {
                               console.log(error);
                               response.status(500).send(failure500);
                           }
                       });

                   } else {
                       // console.log("USERS NOTIFIED...");

                       users_notified = true;
                       sendResponse();
                   }

                   firebase.database().ref("businesses/" +businessID+ "/promotion/").once("value").then(function (snapshot) {

                       if (snapshot.exists()){

                           // filter the items to set to true and false
                           var oldProducts = snapshot.val()["Products"];
                           var oldServices = snapshot.val()["Services"];
                           var oldAccommodation = snapshot.val()["Accommodation"];

                           performPromotionUpdate(businessID, products, oldProducts, "Products", function (error) {
                               if(error === null){
                                   products_updated = true;
                                   sendResponse();
                               } else {
                                   response.status(500).send(failure500);
                               }
                           });

                           performPromotionUpdate(businessID, services, oldServices, "Services", function (error) {
                               if(error === null){
                                   services_updated = true;
                                   sendResponse();
                               } else {
                                   response.status(500).send(failure500);
                               }
                           });

                           performPromotionUpdate(businessID, accommodation, oldAccommodation, "Accommodation", function (error) {
                               if(error === null){
                                   accommodation_updated = true;
                                   sendResponse();
                               } else {
                                   response.status(500).send(failure500);
                               }
                           });


                       } else {

                           // console.log("snapshot doesnt exist... performing generic update");

                           //Update all the products, services and accommodation
                           updateGroup(businessID, products, "Products", true, function (error) {
                               if(error === null){
                                   products_updated = true;
                                   sendResponse();
                               } else {
                                   response.status(500).send(failure500);
                               }
                           });

                           updateGroup(businessID, services, "Services", true, function (error) {
                               if(error === null){
                                   services_updated = true;
                                   sendResponse();
                               } else {
                                   response.status(500).send(failure500);
                               }
                           });

                           updateGroup(businessID, accommodation, "Accommodation", true, function (error) {
                               if(error === null){
                                   accommodation_updated = true;
                                   sendResponse();
                               } else {
                                   response.status(500).send(failure500);
                               }
                           });

                       }

                       //update the promotion object
                       firebase.database().ref("businesses/" +businessID+ "/promotion/").update({

                           information: {
                               start: begin_date,
                               end: end_date,
                               text: promotion_text
                           },
                           Products: products,
                           Services: services,
                           Accommodation: accommodation

                       }).then(function () {

                           promotion_updated = true;
                           sendResponse();

                       }, function (error) {

                           console.log(error);
                           response.status(500).send(failure500);

                       });

                   });

                   // performs the necessary promotion update.
                   // will compare new and old obj to determine if an update, removal or both is needed.
                   function performPromotionUpdate(businessID, newObj, oldObj, type, callback) {

                       // console.log("Started promotion update... " +type);

                       if (oldObj !== undefined && !isEmptyObject(newObj)){
                           // console.log("old obj defined && newobj has values... comparative update");

                           var updated = false;
                           var removed = false;

                           var itemsForUpdate = filterForUniqueValues(newObj, oldObj);
                           var itemsForRemoval = filterForUniqueValues(oldObj, newObj);


                           function sendCallback() {
                               if (updated && removed){
                                   callback(null);
                               }
                           }

                           updateGroup(businessID, itemsForRemoval, type, false, function (error) {
                               if (error === null){
                                   removed = true;

                                   // Update needs to occur after removal. Due to the filter algorithm these two functions
                                   // cannot be executed simultanously with accurate results.
                                   // In order to do both simultanously one would need to combine the results of filtering into
                                   // a single algorithm. And perform a different update cycle.
                                   updateGroup(businessID, itemsForUpdate, type, true, function (error) {

                                       if (error === null){
                                           updated = true;
                                           sendCallback();
                                       } else {
                                           callback(error);
                                       }
                                   });

                               } else {
                                   callback(error);
                               }
                           });



                       } else if (oldObj === undefined && !isEmptyObject(newObj)){
                           // console.log("old obj undefined && newobj has values... performing update");
                           updateGroup(businessID, newObj, type, true, function (error) {
                               if (error === null){
                                   callback(null);
                               } else {
                                   callback(error);
                               }
                           });

                       } else if (oldObj !== undefined && isEmptyObject(newObj)){
                           // console.log("old obj defined && newobj is empty... removing");
                           updateGroup(businessID, oldObj, type, false, function (error) {
                               if (error === null){
                                   callback(null);
                               } else {
                                   callback(error);
                               }
                           });

                       } else {
                           callback(null);
                       }
                   }


                   // Will compare newObj to oldObj and return all the unique entries of newObj.
                   function filterForUniqueValues(newObj, oldObj) {

                       var uniqueObj = {};
                       var newGroupKeys = Object.keys(newObj);
                       var oldGroupKeys = Object.keys(oldObj);

                       newGroupKeys.forEach(function (groupKey) {

                           if (oldGroupKeys.includes(groupKey)){

                               var temp = [];

                               newObj[groupKey].forEach(function (itemKey) {
                                   if (!oldObj[groupKey].includes(itemKey)){
                                       temp.push(itemKey);
                                   }
                               });

                               uniqueObj[groupKey] = temp;

                           } else {
                               uniqueObj[groupKey] = newObj[groupKey];
                           }

                       });

                       return uniqueObj;

                   }
               } else {
                   response.status(404).send(failure404);
               }
            });

        }

    } catch (error) {
        console.log(error);
        response.status(403).send(failure403);
    }
});

app.post("/promotion/remove", function (request, response) {

    try {

        var businessID = request.body["business_id"];
        var userID = request.body.user_id;

        if (parametersInvalid([businessID, userID])){
            throw paramError;
        } else {

            var promotion_removed = false;
            var products_updated = false;
            var services_updated = false;
            var accommodation_updated = false;

            function sendResponse (){
                if (promotion_removed && products_updated && services_updated && accommodation_updated){
                    response.status(200).send(success200);
                }
            }

            //get promotion

            firebase.database().ref("businesses/" +businessID+ "/promotion/").once("value").then(function (snapshot) {

                if (snapshot.exists()){

                    var oldProducts = snapshot.val()["Products"];
                    var oldServices = snapshot.val()["Services"];
                    var oldAccommodation = snapshot.val()["Accommodation"];

                    updateGroup(businessID, oldProducts, "Products", false, function (error) {
                        if(error === null){
                            products_updated = true;
                            sendResponse();
                        } else {
                            response.status(500).send(failure500);
                        }
                    });

                    updateGroup(businessID, oldServices, "Services", false, function (error) {
                        if(error === null){
                            services_updated = true;
                            sendResponse();
                        } else {
                            response.status(500).send(failure500);
                        }
                    });

                    updateGroup(businessID, oldAccommodation, "Accommodation", false, function (error) {
                        if(error === null){
                            accommodation_updated = true;
                            sendResponse();
                        } else {
                            response.status(500).send(failure500);
                        }
                    });

                    firebase.database().ref("businesses/" +businessID+ "/promotion/").remove().then(function () {
                        promotion_removed = true;
                        sendResponse();
                    }, function (error) {
                        console.log(error);
                        response.status(500).send(failure500);
                    });

                } else {
                    response.status(200).send(success200);
                }

            });

        }

    } catch (error){
        console.log(error);
        response.status(403).send(failure403);
    }

});

// Deprecated...
app.post("/promotion/get", function (request, response) {
    try {

        var businessID = request.body["business_id"];
        var storeID = request.body["store_id"];

        if(parametersInvalid([businessID, storeID])){
            throw paramError;
        } else {

            var promotions_downloaded = false;
            var products_downloaded = false;
            var accommodation_downloaded = false;
            var services_downloaded = false;

            var products_built = false;
            var services_built = false;
            var accommodation_built = false;

            var promotions = {};
            var promotion_products = {};
            var promotion_services = {};
            var promotion_accommodation = {};

            var products = {};
            var services = {};
            var accommodation = {};
            var _obj = {};

            function sendResponse () {

                if(products_built && services_built && accommodation_built){
                    response.status(200).send(_obj);
                }

            }

            function buildProduct(){
                if (promotions_downloaded && products_downloaded) {

                    _obj["Products"] = buildPromotionObject(promotion_products, products);
                    products_built = true;
                    sendResponse();

                }
            }

            function buildServices(){
                if (promotions_downloaded && services_downloaded) {

                    _obj["Services"] = buildPromotionObject(promotion_services, services);
                    services_built = true;
                    sendResponse();

                }
            }

            function buildAccommodation(){
                if (promotions_downloaded && accommodation_downloaded) {

                    _obj["Accommodation"] = buildPromotionObject(promotion_accommodation, accommodation);
                    accommodation_built = true;
                    sendResponse();
                }
            }

            getItemURLS(businessID, storeID, "Products", function (content) {

                products_downloaded = true;
                products = content;
                buildProduct();

            });

            getItemURLS(businessID, storeID, "Services", function (content) {

                services = content;
                services_downloaded = true;
                buildServices();

            });

            getItemURLS(businessID, storeID, "Accommodation", function (content) {

                accommodation = content;
                accommodation_downloaded = true;
                buildAccommodation();

            });

            //Get promotion ID for particular Store
            firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/promotion").once("value").then(function (snapshot) {
                if (snapshot.exists()){

                    //Get promotion object using promotionID
                    firebase.database().ref("businesses/" +businessID+ "/promotions/" +snapshot.val()).once("value").then(function (_promotion_snapshot) {
                        if (_promotion_snapshot.exists()){

                            // get promotion info, get ready to pass back to user
                            _obj["details"] = {

                                begin_date: _promotion_snapshot.val()["begin_date"],
                                end_date: _promotion_snapshot.val()["end_date"],
                                text: _promotion_snapshot.val()["text"],
                                id: snapshot.val()

                            };

                            promotion_products = _promotion_snapshot.val()["Products"];
                            promotion_services = _promotion_snapshot.val()["Services"];
                            promotion_accommodation = _promotion_snapshot.val()["Accommodation"];

                            promotions_downloaded = true;

                            buildServices();
                            buildAccommodation();
                            buildProduct();



                        } else {
                            response.status(500).send("Internal Error");
                        }

                    });

                } else {
                    promotions_downloaded = true;

                    promotion_products = {};
                    promotion_accommodation = {};
                    promotion_services = {};

                    buildServices();
                    buildAccommodation();
                    buildProduct();
                }
            });

        }

    } catch (error){
        console.log(error);
        response.status(403).send(paramError);
    }
});

// Deprecated...
app.post("/promotion", function (request, response) {

    try {

        var businessID = request.body["business_id"];
        var storeID = request.body["store_id"];

        if (parametersInvalid([businessID, storeID])){
            throw paramError;
        } else {

            var promotion_downloaded = false;
            var products_downloaded = false;
            var services_downloaded = false;
            var accommodation_downloaded = false;

            var products_filtered = false;
            var services_filtered = false;
            var accommodation_filtered = false;

            var promotion = {};
            var promotion_products = {};
            var promotion_services = {};
            var promotion_accommodation = {};

            var products = {};
            var services = {};
            var accommodation = {};

            function sendResponse () {
                if (products_filtered && services_filtered && accommodation_filtered){
                    response.status(200).send(promotion);
                }
            }

            function filterProducts (){
                if (products_downloaded && promotion_downloaded){
                    promotion["Products"] = filterItems(products, promotion_products);
                    products_filtered = true;
                    sendResponse();
                }
            }

            function filterServices () {
                if (services_downloaded && promotion_downloaded){
                    promotion["Services"] = filterItems(services, promotion_services);
                    services_filtered = true;
                    sendResponse();
                }
            }

            function filterAccommodation () {
                if (accommodation_downloaded && promotion_downloaded){
                    promotion["Accommodation"] = filterItems(accommodation, promotion_services);
                    accommodation_filtered = true;
                    sendResponse();
                }
            }

            getItemURLS(businessID, storeID, "Products", function (content) {

                products = content;
                products_downloaded = true;
                filterProducts();
            });

            getItemURLS(businessID, storeID, "Services", function (content) {
                services = content;
                services_downloaded = true;
                filterServices();
            });

            getItemURLS(businessID, storeID, "Accommodation", function (content) {
                accommodation = content;
                accommodation_downloaded = true;
                filterAccommodation();
            });

            // get promotion ID
            firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/promotion").once("value").then(function (_snapshot) {
                if (_snapshot.exists()){

                    var promotionID = _snapshot.val();

                    //get promotion object
                    firebase.database().ref("businesses/" +businessID+ "/promotions/" +promotionID).once("value").then(function (_object) {
                        if (_object.exists()){

                            promotion["details"] = {
                                begin_date: _object.val()["begin_date"],
                                end_date: _object.val()["end_date"],
                                text: _object.val()["text"],
                                id: promotionID
                            };

                            promotion_products = _object.val()["Products"];
                            promotion_services = _object.val()["Services"];
                            promotion_accommodation = _object.val()["Accommodation"];

                            promotion_downloaded = true;

                            filterAccommodation();
                            filterServices();
                            filterProducts();

                        } else {
                            response.status(500).send("Internal Error");
                            console.log("ERROR: DATA OUT OF SYNC: - BID: " +businessID+ " - SID: " +storeID);
                        }
                    });

                } else {
                    response.status(200).send({});
                }
            });

        }


    } catch (error) {
        console.log(error);
        response.status(403).send(paramError);
    }

});

// MARK: Interactions

// These requests are all user -> user or user -> business based.
// EG: reporting, reviewing, commenting and liking.

//  -   Review: CD

//  App is responsible for pulling data as needed.

// the posted rating should be an object of type Int
// images should an array of image ids. The server will be responsible for building the url
app.post("/review/create", function(request, response){
    try {

        var businessID = request.body["business_id"];
        var storeID = request.body["store_id"];
        var image = request.body["image_urls"];
        var rating = request.body["rating"];
        var text = request.body["text"];
        var userID = request.body["user_id"];

        if(parametersInvalid([businessID, storeID, image, rating, text, userID]) || typeof rating !== "number" || !Array.isArray(image)){
            throw paramError;
        } else {

            var review_updated = false;
            var store_updated = false;
            var image_updated = false;
            var count_updated = false;
            var review_images_counted = false;
            var final_delete_request_removed = false;

            var timeStamp = Date.now();

            function sendResponse() {

                if (review_images_counted && store_updated && count_updated && image_updated && review_updated && final_delete_request_removed){
                    response.status(200).send(success200);
                }

            }

            // there might be a brief overlap where a user deletes their review and then creates a new one.
            // due to reviewIDS being indexed on userID this could cause the review count to go out of sync.
            var finalDeleteID = userID.substring(0, 4) + businessID(0, 4) + storeID;
            firebase.database().ref("permanent_delete/reviews/" + finalDeleteID).remove().then(function () {
                final_delete_request_removed = true
            }, function (error) {
                console.log(error);
            });


            doesStoreExist(businessID, storeID, function(storeDoesExist){

                if (storeDoesExist){

                    var hasUserName = false;
                    var hasUserSurname = false;

                    var name = "Variann";
                    var surname = "Wrynn";

                    userNameForUserID(userID, function (_name) {
                        if (name) {
                            name = _name;
                            hasUserName = true;
                            createReview();
                        } else {
                            response.status(500).send(failure500);
                        }
                    });

                    userSurnameForUserID(userID, function(_surname){
                        if (surname){
                            surname = _surname;
                            hasUserSurname = true;
                            createReview();
                        } else {
                            response.status(500).send(failure500);
                        }
                    });

                    function createReview(){
                        if (hasUserName && hasUserSurname){
                            //Read Existing review if there is any
                            firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/reviews/" +userID).once("value").then(function (snapshot) {

                                //if the previous review exists delete it then update
                                if (snapshot.exists()) {

                                    var options = {
                                        method: "post",
                                        body: {
                                            user_id: userID,
                                            business_id: businessID,
                                            store_id: storeID,
                                            image_ids: image
                                        },
                                        json: true
                                    };

                                    deleteReview(businessID, storeID, userID, image, function (error) {

                                        error ? response.status(500).send(failure500) : createReview();
                                        
                                    });

                                } else {
                                    createReview()
                                }

                            });

                            var imageURLS = {};
                            var imageObject = {};

                            for (var i = 0; i < image.length; i++){
                                imageURLS[i] = "businesses/" +businessID+ "/stores/" +storeID+ "/reviews/" +image[i]+".jpg";
                                imageObject[image[i]] = {
                                    created: timeStamp + i,
                                    image_url: imageURLS[i],
                                    uploader: userID,
                                    likes: 0
                                }
                            }

                            function createReview(){

                                //update review image count
                                firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/images_review").once("value").then(function (snapshot) {
                                    var count = image.length;

                                    if (snapshot.exists()){
                                        count = snapshot.val() + image.length
                                    }

                                    firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/").update(function () {

                                        var obj = {};
                                        obj["images_review"] = count;
                                        return obj;

                                    }()).then(function () {

                                        review_images_counted = true;
                                        sendResponse();

                                    }, function (error) {
                                        console.log(error);
                                        response.status(500).send(failure500);
                                    });

                                });


                                //update store rating
                                firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/rating").once("value").then(function (ratingSnapshot) {
                                    if(ratingSnapshot.exists()){

                                        var _rating = ratingSnapshot.val();
                                        var num = _rating["numerator"] + rating;
                                        var denom = _rating["denominator"] + 5;

                                        firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/rating").update({
                                            numerator: num,
                                            denominator: denom
                                        }).then(function () {

                                            store_updated = true;
                                            sendResponse();

                                        }, function (error) {
                                            console.log(error);
                                            response.status(500).send("Internal Error");
                                        });

                                    } else {
                                        console.log("ERROR: store does not have rating. StoreID: " +storeID + " BusinessID: " +businessID);
                                        response.status(500).send("Internal Error");
                                    }
                                });

                                //update review count
                                firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/review_count").once("value").then(function (snapshot) {

                                    var count = 1;

                                    if(snapshot.exists()){
                                        count = snapshot.val() + 1;
                                    }

                                    firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/").update({
                                        "review_count": count
                                    }).then(function () {

                                        count_updated = true;
                                        sendResponse();

                                    }, function (error) {
                                        console.log(error);
                                        response.status(500).send("Internal Error");
                                    });

                                });

                                // update image manifest
                                firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID + "/images/reviews/" +
                                    "").update(imageObject).then(function () {

                                    image_updated = true;
                                    sendResponse();

                                }, function (error) {
                                    console.log(error);
                                    response.status(500).send("Internal Error");
                                });

                                //add review to store
                                firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/reviews/" +userID).update({

                                    image: imageURLS,
                                    rating: rating,
                                    rating_neg: -rating,
                                    text: text,
                                    age: timeStamp,
                                    age_neg: -timeStamp,
                                    name: name,
                                    surname: surname

                                }).then(function () {

                                    review_updated = true;
                                    sendResponse();

                                }, function (error) {
                                    console.log(error);
                                    response.status(500).send("Internal Error");
                                });
                            }
                        }
                    }

                } else {
                    response.status(404).send(failure404);
                }

            });

        }

    } catch (error){
        console.log(error);
        response.status(403).send(paramError);
    }
});

// posted imageIDS should be an array of imageIDS

// remove the review and associated comments and images
// updates the store rating as needed
app.post("/review/remove", function(request, response){
    try {

        var userID = request.body.user_id;
        var businessID = request.body.business_id;
        var storeID = request.body.store_id;
        var imageIDS = request.body.image_ids;

        if(parametersInvalid([userID, businessID, storeID, imageIDS]) || !Array.isArray(imageIDS)){
            throw paramError;
        } else {

            var review_removed = false;
            var final_remove_added = false;

            function sendResponse() {
                if (final_remove_added && review_removed){
                    response.status(200).send(success200);
                }
            }

            var currentDate = Date.now();

            var finalDeleteID = userID.substring(0, 4) + businessID.substring(0, 4) + storeID;

            // FINAL REMOVAL:
            firebase.database().ref("permanent_delete/reviews/" +finalDeleteID).update({
                review_id: userID,
                business_id: businessID,
                store_id: storeID,
                image_ids: imageIDS
            }).then(function(){
                final_remove_added = true;
                sendResponse();
            }, function(error){
                console.log(error);
                response.status(500).send(failure500);
            });

            deleteReview(businessID, storeID, userID, imageIDS, function (error) {
               if (error){
                    response.status(500).send(failure500);
               } else {
                    review_removed = true;
                    sendResponse();
               }
            });


        }

    } catch (error){
        console.log(error);
        response.status(403).send(paramError);
    }
});

//  -   Comment: CD

app.post("/comment/create", function(request, response){

    try {

        var businessID = request.body.business_id;
        var storeID = request.body.store_id;
        var text = request.body.text;
        var userID = request.body.user_id;
        var reviewID = request.body.review_id;
        var reviewerID = request.body.reviewer_id;

        if(parametersInvalid([businessID, storeID, text, userID, reviewID, reviewerID])){
            throw paramError;
        } else {

            var comment_uploaded = false;
            var review_updated = false;
            var reviewer_notified = false;

            var currentTime = Date.now();

            function sendResponse(){
                if (comment_uploaded && review_updated && reviewer_notified){
                    response.status(200).send(success200);
                }
            }

            doesReviewExist(businessID, storeID, reviewID, function(reviewDoesExist){

                if (reviewDoesExist){

                    var businessNameDownloaded = false;
                    var nameDownloaded = false;
                    var surnameDownloaded = false;

                    var businessName = "Stormwind";
                    var name = "Varian";
                    var surname = "Wrynn";

                    function createComment(){
                        if (businessNameDownloaded && nameDownloaded && surnameDownloaded){
                            //upload comment
                            firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/comments/" +reviewID+ "/" +currentTime+ "/").update({
                                user_id: userID,
                                age: currentTime,
                                name: name,
                                surname: surname,
                                text: text,
                                replies: 0,
                                likes: 0,
                                likes_neg: 0
                            }).then(function (){

                                comment_uploaded = true;
                                sendResponse();

                            }, function(error){
                                console.log(error);
                                response.status(500).send(failure500);
                            });

                            //get current review comment count
                            firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/reviews/" +reviewID+ "/comments").once("value").then(function (snapshot) {

                                var newCommentCount = 1;

                                if (snapshot.exists()){
                                    newCommentCount = snapshot.val() + 1;
                                }

                                var type = {
                                    type: "new_comment",
                                    text: text,
                                    comment_count: newCommentCount,
                                    review_id: reviewerID,
                                    store_id: storeID,
                                    comment_id: currentTime
                                };

                                if (reviewerID != userID){
                                    notifyUser(reviewerID, userID, businessName, businessID, storeID, currentTime, name, surname, type, 0, {}, function (error) {
                                        if (error == null) {
                                            reviewer_notified = true;
                                        } else {
                                            response.status(500).send(failure500);
                                            console.log(error);
                                        }
                                    });
                                } else {
                                    reviewer_notified = true;
                                    sendResponse();
                                }

                                //update review count

                                firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/reviews/" +reviewID+ "/").update({
                                    comments: newCommentCount
                                }).then(function () {

                                    review_updated = true;
                                    sendResponse();

                                }, function (error) {
                                    console.log(error);
                                    response.status(500).send(failure500);
                                });
                            });
                        }
                    }

                    // get business name
                    businessNameForBusinessID(businessID, function (_businessName){

                        if (_businessName){
                            businessName = _businessName;
                            businessNameDownloaded = true;
                            createComment();
                        } else {
                            response.status(500).send(failure500);

                        }

                    });

                    userNameForUserID(userID, function(_name){
                        if(_name == null){
                            response.status(500).send(failure500);
                        } else {
                            nameDownloaded = true;
                            name = _name;
                            createComment();
                        }
                    });

                    userSurnameForUserID(userID, function(_surname){
                        if (_surname == null){
                            response.status(500).send(failure500);
                        } else {
                            surnameDownloaded = true;
                            surname = _surname;
                            createComment();
                        }
                    });

                } else {
                    response.status(404).send(failure404);
                }

            });

        }

    } catch (error) {
        console.log(error);
        response.status(403).send(failure403);
    }


});

app.post("/comment/remove", function(request, response){

    try {

        var businessID = request.body.business_id;
        var storeID = request.body.store_id;
        var reviewID = request.body.review_id;
        var commentID = request.body.comment_id;

        if(parametersInvalid([businessID, storeID, reviewID, commentID])){
            throw paramError;
        } else {

            var count_updated = false;
            var comment_removed = false;
            var final_remove_requested = false;

            function sendResponse() {
                if(final_remove_requested && count_updated && comment_removed){
                    response.status(200).send(success200);
                }
            }

            deleteComment(businessID, storeID, reviewID, commentID, "", function (error, id) {
                if (error){
                    response.status(500).send(failure500);
                } else {
                    comment_removed = true;
                    sendResponse();
                }
            });

            //final remove request
            firebase.database().ref("permanent_delete/comments").update({
                business_id: businessID,
                store_id: storeID,
                review_id: reviewID,
                comment_id: commentID
            }).then(function(){
                final_remove_requested = true;
                sendResponse();
            }, function(error){
                console.log(error);
                response.status(500).send(failure500);
            });

            // update count
            firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/reviews/"+reviewID+"/comments").once("value").then(function (snapshot) {

                var newCounts = 0;

                if(snapshot.exists()){
                    newCounts = snapshot.val() - 1;
                }

                if (newCounts < 0){
                    newCounts = 0;
                }

                firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/reviews/" +reviewID).update({
                    comments: newCounts
                }).then(function () {
                    count_updated = true;
                    sendResponse();
                }, function (error) {
                    console.log(error);
                    response.status(500).send(failure500);
                });

            });
        }

    } catch (error){
        console.log(error);
        response.status(403).send(failure403);
    }
});

//  -   Reply: CD

///When using this call do not enter the @ username usersurname parameter into text from the app. The server will handle this.
app.post("/reply/create", function (request, response) {

    try {

        var businessID = request.body.business_id;
        var storeID = request.body.store_id;
        var reviewID = request.body.review_id;
        var commentID = request.body.comment_id;
        var text = request.body.text;
        var userReplying = request.body.user_id;
        var usersToNotify = request.body.users_to_notify;

        if (parametersInvalid([businessID, storeID, reviewID, commentID, text, usersToNotify, userReplying, usersToNotify]) || !Array.isArray(usersToNotify)) {
            throw paramError;
        } else {

            var replyCreated = false;
            var repliesCounted = false;
            var usersNotified = 0;

            function sendResponse() {

                if(replyCreated && repliesCounted && usersNotified == usersToNotify.length){
                    response.status(200).send(success200);
                }
            }

            doesCommentExist(businessID, storeID, reviewID, commentID, function (commentDoesExist) {

                if (commentDoesExist && usersToNotify.length != 0){

                    var replyID = Date.now();

                    //NOTIFY USERS:
                    var name;
                    var surname;
                    var businessName;
                    var replyUserName;
                    var replyUserSurname;

                    var hasNotifyUserName = false;
                    var hasNotifyUserSurname = false;
                    var hasBusinessName = false;

                    var hasUserName = false;
                    var hasUserSurname = false;

                    function sendNotification() {
                        if (hasNotifyUserName && hasNotifyUserSurname && hasBusinessName && hasUserName && hasUserSurname) {

                            //CREATE REPLY ENTITY:
                            firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/replies/" +reviewID+ "/" +commentID+ "/" +replyID).update({
                                text: "@ " +name.capitalize()+ " " +surname.capitalize()+ ": " + text,
                                likes: 0,
                                user_id: userReplying,
                                name: replyUserName,
                                surname: replyUserSurname,
                                age: replyID
                            }).then(function () {

                                replyCreated = true;
                                sendResponse();

                            }, function (error) {
                                console.log(error);
                                response.status(500).send(failure500);
                            });

                            usersToNotify.forEach(function(userID) {

                                notifyUser(userID, userReplying, businessName, businessID, storeID, commentID+"_"+replyID, replyUserName, replyUserSurname, {

                                    type: "reply_comment",
                                    text: "@ " +name.capitalize()+ " " +surname.capitalize()+ ": "+text,
                                    review_id: reviewID,
                                    comment_id: commentID,
                                    store_id: storeID

                                }, {}, {}, function (error) {
                                    if(error == null){
                                        usersNotified += 1;
                                        sendResponse();
                                    }
                                })

                            });

                        }
                    }

                    //  read business name
                    firebase.database().ref("businesses/" +businessID+ "/name").once("value").then(function (snapshot) {

                        if (snapshot.exists()){

                            businessName = snapshot.val();
                            hasBusinessName = true;
                            sendNotification();

                        } else {
                            response.status(403).send(failure403);
                        }

                    });

                    userNameForUserID(usersToNotify[0], function (_name) {
                        name = _name;
                        hasNotifyUserName = true;
                        sendNotification();
                    });

                    userSurnameForUserID(usersToNotify[0], function (_surname) {
                        surname = _surname;
                        hasNotifyUserSurname = true;
                        sendNotification();
                    });

                    userNameForUserID(userReplying, function (_name) {
                        replyUserName = _name;
                        hasUserName = true;
                        sendNotification();
                    });

                    userSurnameForUserID(userReplying, function (_surname) {
                        replyUserSurname = _surname;
                        hasUserSurname = true;
                        sendNotification();
                    });

                    //UPDATE REPLY COUNT:
                    //  read current reply count
                    firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/comments/" +reviewID+ "/" +commentID+ "/replies").once("value").then(function (snapshot) {

                        var count = 1;

                        if (snapshot.exists()){

                            count = snapshot.val() + 1;

                        }

                        //update reply count
                        firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/comments/" +reviewID+ "/" +commentID+ "/").update({
                            replies: count
                        }).then(function () {

                            repliesCounted = true;
                            sendResponse();

                        }, function (error) {

                            console.log(error);
                            response.status(500).send(failure500);

                        });

                    });

                } else {
                    response.status(404).send(failure404);
                }

            });

        }

    } catch (e) {
        console.log(e);
        response.status(403).send(failure403);
    }

});

app.post("/reply/remove", function (request, response) {

    try {

        var businessID = request.body.business_id;
        var storeID = request.body.store_id;
        var reviewID = request.body.review_id;
        var commentID = request.body.comment_id;
        var replyID = request.body.reply_id;

        if (parametersInvalid([businessID, storeID, reviewID, commentID, replyID])) {
            throw paramError;
        } else {


            var replyDeleted = false;
            var final_remove_requested = false;
            var countUpdated = false;

            function sendResponse() {
                if(replyDeleted && final_remove_requested && countUpdated){
                    response.status(200).send(success200);
                }
            }

            // final remove request
            // used by the server to catch any orphan writes
            firebase.database().ref("permanent_delete/replies/" +Date.now()).update({
                business_id: businessID,
                store_id: storeID,
                review_id: reviewID,
                comment_id: commentID,
                reply_id: replyID
            }).then(function(){
                final_remove_requested = true;
                sendResponse();
            }, function (error) {
                console.log(error);
                response.status(500).send(failure500);
            });

            deleteReply(businessID, storeID, reviewID, commentID, replyID, "", function (error) {
                if (error) {
                    response.status(500).send();
                } else {
                    replyDeleted = true;
                    sendResponse();
                }
            });

            //UPDATE COUNT:
            //  read current reply count
            firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/comments/" +reviewID+ "/" +commentID+ "/replies").once("value").then(function (snapshot) {
                var count = 0;

                if(snapshot.exists()){
                    count = snapshot.val() - 1;
                }

                if (count < 0){
                    count = 0
                }

                //update reply count
                firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/comments/" +reviewID+ "/" +commentID+ "/").update({
                    replies: count
                }).then(function () {

                    countUpdated = true;
                    sendResponse();

                }, function (error) {
                    console.log(error);
                    callback(error);
                });
            });


        }

    } catch (e) {
        console.log(e);
        response.status(403).send(failure403);
    }

});

//  -   Report: Create
app.post("/report/store", function (request, response) {
    try {

        var businessID = request.body["business_id"];
        var storeID = request.body["store_id"];
        var reporterID = request.body["reporter_id"];
        var reportID = request.body["report_id"];
        var text = request.body["text"];
        var tags = request.body["tags"];

        if (parametersInvalid([businessID, storeID, reporterID, reportID, text, tags])){
            throw paramError;
        } else {

            var report_created = false;
            var user_updated = false;

            function sendResponse() {
                if (report_created && user_updated){
                    response.status(200).send(success200);
                }
            }

            var currentTime = Date.now();

            //Update the user
            firebase.database().ref("users/" + reporterID+ "/reports/businesses/" +businessID+ "/stores/" +storeID).update(function (){
                var obj ={};
                obj[currentTime] = reportID;
                return obj;
            }()).then(function () {

                user_updated = true;
                sendResponse();

            }, function (error) {
                console.log(error);
                response.status(500).send(failure500);
            });

            // Create the report
            firebase.database().ref("reports/" +reportID+ "/").update({

                reporter_uid: reporterID,
                text: text,
                tags: tags,
                age: currentTime,
                business_id: businessID,
                store_id: storeID,
                type: "business"

            }).then(function () {

                report_created = true;
                sendResponse();

            }, function (error) {
                console.log(error);
                response.status(500).send(failure500);
            });

        }

    } catch (error) {
        console.log(error);
        response.status(403).send(failure403);
    }
});

app.post("/report/review", function(request, response){

    try {

        var businessID = request.body["business_id"];
        var storeID = request.body["store_id"];
        var reporterID = request.body["reporter_id"];
        var reportID = request.body["report_id"];
        var reporteeID = request.body["reportee_id"];
        var text = request.body["text"];
        var tags = request.body["tags"];
        var reviewID = request.body["review_id"];

        if (parametersInvalid([businessID, storeID, reporteeID, reporterID, reportID, text, tags, reviewID]) && userMayReport(reporteeID, reporterID)){
            throw paramError;
        } else {

            var report_created = false;
            var user_updated = false;

            function sendResponse() {
                if (report_created && user_updated){
                    response.status(200).send(success200);
                }
            }

            var currentTime = Date.now();

            //Update the user
            firebase.database().ref("users/" + reporterID+ "/reports/reviews/" +reviewID+ "/").update(function (){
                var obj ={};
                obj[currentTime] = reportID;
                return obj;
            }()).then(function () {

                user_updated = true;
                sendResponse;

            }, function (error) {
                console.log(error);
                response.status(500).send(failure500);
            });

            firebase.database().ref("reports/" +reportID+ "/").update({

                reporter_uid: reporterID,
                reportee_uid: reporteeID,
                text: text,
                tags: tags,
                business_id: businessID,
                store_id: storeID,
                age: currentTime,
                review_id: reviewID,
                type: "review"

            }).then(function () {

                report_created = true;
                sendResponse();

            }, function (error) {
                console.log(error);
                response.status(500).send(failure500);
            });

        }

    } catch (error){
        console.log(error);
        response.status(403).send(failure403);
    }

});

app.post("/report/comment", function (request, response) {
    try {

        var businessID = request.body["business_id"];
        var storeID = request.body["store_id"];
        var reporterID = request.body["reporter_id"];
        var reportID = request.body["report_id"];
        var reporteeID = request.body["reportee_id"];
        var text = request.body["text"];
        var tags = request.body["tags"];
        var reviewID = request.body["review_id"];
        var commentID = request.body["comment_id"];

        if (parametersInvalid([businessID, storeID, reporteeID, reporterID, reportID, text, tags, reviewID, commentID]) && userMayReport(reporteeID, reporterID)){
            throw paramError;
        } else {

            var report_created = false;
            var user_updated = false;

            function sendResponse() {
                if (report_created && user_updated){
                    response.status(200).send(success200);
                }
            }

            var currentTime = Date.now();

            //Update the user
            firebase.database().ref("users/" + reporterID+ "/reports/comments/" +commentID+ "/").update(function (){
                var obj ={};
                obj[currentTime] = reportID;
                return obj;
            }()).then(function () {

                user_updated = true;
                sendResponse;

            }, function (error) {
                console.log(error);
                response.status(500).send(failure500);
            });

            firebase.database().ref("reports/" +reportID+ "/").update({

                reporter_uid: reporterID,
                reportee_uid: reporteeID,
                text: text,
                tags: tags,
                business_id: businessID,
                store_id: storeID,
                review_id: reviewID,
                age: currentTime,
                comment_id: commentID,
                type: "comment"

            }).then(function () {

                report_created = true;
                sendResponse();

            }, function (error) {
                console.log(error);
                response.status(500).send(failure500);
            });

        }

    } catch (error){
        console.log(error);
        response.status(403).send(failure403);
    }
});

app.post("/report/reply", function (request, response) {

    try {

        var businessID = request.body["business_id"];
        var storeID = request.body["store_id"];
        var reporterID = request.body["reporter_id"];
        var reportID = request.body["report_id"];
        var reporteeID = request.body["reportee_id"];
        var replyID = request.body.reply_id;
        var text = request.body["text"];
        var tags = request.body["tags"];
        var reviewID = request.body["review_id"];
        var commentID = request.body["comment_id"];

        if (parametersInvalid([businessID, storeID, reporteeID, reporterID, reportID, text, tags, reviewID, commentID, replyID]) && userMayReport(reporteeID, reporterID)){
            throw paramError;
        } else {

            var report_created = false;
            var user_updated = false;

            function sendResponse() {
                if (report_created && user_updated){
                    response.status(200).send(success200);
                }
            }

            var currentTime = Date.now();

            //Update the user
            firebase.database().ref("users/" + reporterID+ "/reports/replies/" +replyID+ "/").update(function (){
                var obj ={};
                obj[currentTime] = reportID;
                return obj;
            }()).then(function () {

                user_updated = true;
                sendResponse;

            }, function (error) {
                console.log(error);
                response.status(500).send(failure500);
            });

            firebase.database().ref("reports/" +reportID+ "/").update({

                reporter_uid: reporterID,
                reportee_uid: reporteeID,
                text: text,
                tags: tags,
                business_id: businessID,
                store_id: storeID,
                review_id: reviewID,
                comment_id: commentID,
                age: currentTime,
                reply_id: replyID,
                type: "reply"

            }).then(function () {

                report_created = true;
                sendResponse();

            }, function (error) {
                console.log(error);
                response.status(500).send(failure500);
            });

        }

    } catch (e) {
        console.log(e);
        response.status(403).send(failure403);
    }

});

app.post("/report/image", function (request, response) {

    try {

        var businessID = request.body["business_id"];
        var storeID = request.body["store_id"];
        var reporterID = request.body["reporter_id"];
        var reportID = request.body["report_id"];
        var reporteeID = request.body["reportee_id"];
        var text = request.body["text"];
        var tags = request.body["tags"];
        var isOfficial = request.body.isOfficial;
        var imageID = request.body.image_id;

        if(parametersInvalid([businessID, storeID, reporterID, reportID, reporteeID, text, tags, isOfficial, imageID]) && userMayReport(reporteeID, reporterID)){
            throw paramError;
        } else {

            var report_created = false;
            var user_updated = false;

            function sendResponse() {
                if (report_created && user_updated){
                    response.status(200).send(success200);
                }
            }

            var currentTime = Date.now();

            //Update the user
            firebase.database().ref("users/" + reporterID+ "/reports/images/" +imageID+ "/").update(function (){
                var obj ={};
                obj[currentTime] = reportID;
                return obj;
            }()).then(function () {

                user_updated = true;
                sendResponse;

            }, function (error) {
                console.log(error);
                response.status(500).send(failure500);
            });

            firebase.database().ref("reports/" +reportID+ "/").update({

                reporter_uid: reporterID,
                reportee_uid: reporteeID,
                text: text,
                tags: tags,
                business_id: businessID,
                store_id: storeID,
                isOfficial: isOfficial,
                age: currentTime,
                image_id: imageID,
                type: "image"


            }).then(function () {

                report_created = true;
                sendResponse();

            }, function (error) {
                console.log(error);
                response.status(500).send(failure500);
            });

        }

    } catch (e){
        console.log(e);
        response.status(403).send(failure403);
    }

});

app.post("/report/item", function (request, response) {

    try {

        var businessID = request.body["business_id"];
        var reporterID = request.body["reporter_id"];
        var reportID = request.body["report_id"];
        var text = request.body["text"];
        var tags = request.body["tags"];
        var itemID = request.body.item_id;
        var itemGroupID = request.body.item_group_id;
        var itemType = request.body.item_type;

        if (parametersInvalid([businessID, reporterID, reportID, text, tags, itemID, itemGroupID, itemType])){
            throw paramError;
        } else {

            var report_created = false;
            var user_updated = false;

            function sendResponse() {
                if (report_created && user_updated){
                    response.status(200).send(success200);
                }
            }

            var currentTime = Date.now();

            //Update the user
            firebase.database().ref("users/" + reporterID+ "/reports/items/" +itemGroupID+ "/" + itemID+ "/").update(function (){
                var obj ={};
                obj[currentTime] = reportID;
                return obj;
            }()).then(function () {

                user_updated = true;
                sendResponse;

            }, function (error) {
                console.log(error);
                response.status(500).send(failure500);
            });

            firebase.database().ref("reports/" +reportID+ "/").update({

                business_id: businessID,
                reporter_uid: reporterID,
                text: text,
                tags: tags,
                item_id: itemID,
                item_group_id: itemGroupID,
                item_type: itemType,
                age: currentTime,
                type: "item"

            }).then(function () {

                report_created = true;
                sendResponse();


            }, function (error) {
                console.log(error);
                response.status(500).send(failure500);
            });

        }

    } catch (e){
        console.log(e);
        response.status(403).send(failure403);
    }

});

//  -   Likes: CD

app.post("/like/comment", function (request, response) {

    try {

        var businessID = request.body.business_id;
        var storeID = request.body.store_id;
        var reviewID = request.body.review_id;
        var commentID = request.body.comment_id;
        var userID = request.body.user_id;
        var commenterID = request.body.commenter_id;
        var userName = request.body.name;
        var userSurname = request.body.surname;
        var businessName = request.body.business_name;
        var commentText = request.body.comment_text;

        if (parametersInvalid([businessID, storeID, reviewID, commentID, userID, commenterID, userName, userSurname, businessName, commentText])){
            throw paramError;
        } else {

            var currentTime = Date.now();

            var registry_updated = false;
            var likes_count_updated = false;
            var notification_sent = false;

            function sendResponse() {
                if (registry_updated && likes_count_updated && notification_sent){
                    response.status(200).send(success200);
                }
            }

            doesCommentExist(businessID, storeID, reviewID, commentID, function (exists) {
               if (exists){

                   //check likes registry for userID
                   firebase.database().ref("likes_registry/businesses/" +businessID+ "/stores/" +storeID+ "/reviews/" +reviewID+ "/" +
                       "/comments/" +commentID+ "/" +userID).once("value").then(function (snapshot) {

                       // get the likes for this object
                       likesForPath("businesses/" + businessID+ "/stores/" +storeID+ "/comments/" +
                           "" +reviewID+ "/" +commentID+ "/likes", function (totalLikes) {

                           // console.log("CURRENT LIKES ARE: " + totalLikes);

                           //if user has liked this comment. Unlike it.
                           if (snapshot.exists()){

                               notification_sent = true;

                               //get the number of likes for a given path and decrement
                               if (totalLikes != 0) {
                                   var newLikes = totalLikes - 1;

                                   // console.log("NEW LIKES ARE: "+newLikes);

                                   setLikesForPath("businesses/" +businessID+ "/stores/" +storeID+ "/comments/" +reviewID+ "/" +
                                       "" +commentID +"/", newLikes, function (content) {

                                       //success
                                       if (content == null){
                                           likes_count_updated = true;
                                           sendResponse();
                                           //fail
                                       } else {
                                           console.log(error);
                                           response.status(500).send("Internal Error");
                                       }

                                   });
                               } else {
                                   likes_count_updated = true;
                                   sendResponse();
                               }

                               //remove user id from registry
                               removeUserIDFromRegistryWithPath("likes_registry/businesses/" +businessID+ "/stores/" +storeID+ "/reviews/" +reviewID+ "/" +
                                   "/comments/" +commentID+ "/" +userID, function (content) {

                                   // success
                                   if (content == null){
                                       registry_updated = true;
                                       sendResponse();
                                       //fail
                                   } else {
                                       console.log(content);
                                       response.status(500).send("Internal Error");
                                   }

                               });

                               //need to like this comment
                           } else {

                               var newLikes = totalLikes + 1;
                               // console.log("NEW LIKES ARE: "+newLikes);
                               // set new likes amount
                               setLikesForPath("businesses/" +businessID+ "/stores/" +storeID+ "/comments/" +reviewID+ "/" +
                                   "" +commentID+ "/", newLikes, function (content) {

                                   if (content == null){
                                       likes_count_updated = true;
                                       sendResponse();
                                   } else {
                                       console.log(content);
                                       response.status(500).send("Internal Error");
                                   }

                               });

                               // add userID to registry
                               addUserIDToRegistryWithPath("likes_registry/businesses/" +businessID+ "/stores/" +storeID+ "/reviews/" +reviewID+ "/" +
                                   "/comments/" +commentID+ "/", userID, function (content) {
                                   if (content == null) {
                                       registry_updated = true;
                                       sendResponse();
                                   } else {
                                       console.log(content);
                                       response.status(500).send("Internal Error");
                                   }
                               });

                               var type = {
                                   type: "comment_liked",
                                   text: commentText,
                                   store_id: storeID,
                                   review_id: reviewID
                               };

                               // notify commenter
                               notifyUser(commenterID, userID, businessName, businessID, storeID, commentID, userName, userSurname,
                                   type, totalLikes, {}, function (content) {
                                       if (content == null){
                                           notification_sent = true;
                                           sendResponse();
                                       } else {
                                           response.status(500).send("Internal Error");
                                       }
                                   });
                           }

                       });
                   });

               } else {
                    response.status(404).send(failure404);
               }
            });

        }

    } catch (error) {
        console.log(error);
        response.status(403).send(paramError);
    }

});

app.post("/like/review", function (request, response) {

    try {

        var businessID = request.body.business_id;
        var storeID = request.body.store_id;
        var reviewID = request.body.review_id;
        var userID = request.body.user_id;
        var userName = request.body.name;
        var userSurname = request.body.surname;
        var businessName = request.body.business_name;
        var reviewerID = request.body.reviewer_id;
        var reviewText = request.body.review_text;
        var reviewImages = request.body.review_images;

        if (parametersInvalid([businessID, storeID, reviewID, userID, userName, userSurname, businessName, reviewerID, reviewText,
            reviewImages]) || !Array.isArray(reviewImages)){
            throw paramError;
        } else {

            var registry_updated = false;
            var likes_count_updated = false;
            var notification_sent = false;

            function sendResponse() {
                if(registry_updated && likes_count_updated && notification_sent){
                    response.status(200).send(success200);
                }
            }

            doesReviewExist(businessID, storeID, reviewID, function (exists) {
               if (exists){
                   var currentTime = Date.now();

                   var registryPath = "likes_registry/businesses/" +businessID+ "/stores/" +storeID+ "/reviews/" +
                       "" + reviewID+ "/likes/";
                   var objectPath = "businesses/" +businessID+ "/stores/" +storeID+ "/reviews/" +reviewID+ "/";

                   // Check registry for userID
                   firebase.database().ref(registryPath +""+ userID).once("value").then(function (snapshot) {

                       // get the total likes for this object
                       likesForPath(objectPath + "likes", function (totalLikes) {

                           // yes they have - remove userID from registry and decrement like count
                           if(snapshot.exists()){

                               notification_sent = true;

                               //remove the user from the registry
                               removeUserIDFromRegistryWithPath(registryPath +""+ userID, function (content) {
                                   if (content == null){

                                       registry_updated = true;
                                       sendResponse();

                                   } else {

                                       console.log(content);
                                       response.status(500).send("Internal Error");

                                   }
                               });

                               var newLikes = totalLikes - 1;
                               //decrement the objects likes
                               if (totalLikes != 0) {
                                   setLikesForPath(objectPath, newLikes, function (content) {

                                       if (content == null) {
                                           likes_count_updated = true;
                                           sendResponse();
                                       } else {
                                           console.log(content);
                                           response.status(500).send("Internal Error");
                                       }

                                   });
                               } else {
                                   likes_count_updated = true;
                                   sendResponse();
                               }

                           } else {

                               var newLikes = totalLikes + 1;
                               // update like counts
                               setLikesForPath(objectPath, newLikes, function (content) {
                                   if(content == null){
                                       likes_count_updated = true;
                                       sendResponse();
                                   } else {
                                       console.log(content);
                                       response.status(500).send("Internal Error");
                                   }
                               });

                               // update registry
                               addUserIDToRegistryWithPath(registryPath, userID, function (content) {

                                   if (content == null){
                                       registry_updated = true;
                                       sendResponse();
                                   } else {
                                       console.log(content);
                                       response.status(500).send("Internal Error");
                                   }

                               });

                               var type = {
                                   type: "review_liked",
                                   text: reviewText,
                                   image_urls: reviewImages,
                                   store_id: storeID
                               };

                               // send notification
                               notifyUser(reviewerID, userID, businessName, businessID, storeID, reviewID, userName,
                                   userSurname, type, totalLikes, {}, function (content) {
                                       if(content == null){

                                           notification_sent = true;
                                           sendResponse();

                                       } else {
                                           console.log(content);
                                           response.status(500).send("Internal Error");
                                       }
                                   });

                           }

                       });

                   });
               } else {
                   response.status(404).send(failure404);
               }
            });

        }

    } catch (error) {
        console.log(error);
        response.status(403).send(paramError);
    }

});

app.post("/like/official_image", function (request, response) {

    try {

        var businessID = request.body.business_id;
        var storeID = request.body.store_id;
        var imageID = request.body.image_id;
        var userID = request.body.user_id;

        if (parametersInvalid([businessID, storeID, imageID, userID])){
            throw paramError;
        } else {

            var likes_count_updated = false;
            var registry_updated = false;

            function sendResponse() {
                if(likes_count_updated && registry_updated) {
                    response.status(200).send(success200);
                }
            }

            doesOfficialImageExist(businessID, storeID, imageID, function (exists) {
               if (exists){
                   var objectURL = "businesses/" +businessID+ "/stores/" +storeID+ "/images/official/" +
                       "" +imageID;

                   // check if user has liked this thing
                   firebase.database().ref("likes_registry/" +objectURL+ "/likes/" +userID).once("value").then(function (snapshot) {

                       //get number of likes for this thing
                       likesForPath(objectURL + "/likes", function (totalLikes) {

                           var newLikes = 0;
                           if (snapshot.exists()){

                               //decrement total likes
                               if (totalLikes != 0) {
                                   newLikes = totalLikes - 1;
                               }

                               //remove userID from registry
                               removeUserIDFromRegistryWithPath("likes_registry/" +objectURL+ "/likes/" +userID, function (content) {
                                   if(content == null){

                                       registry_updated = true;
                                       sendResponse();

                                   } else {
                                       console.log(content);
                                       response.status(500).send("Internal Error");
                                   }
                               });

                           } else {

                               newLikes = totalLikes + 1;

                               //add user to registry
                               addUserIDToRegistryWithPath("likes_registry/" +objectURL+ "/likes/", userID, function (content) {
                                   if(content == null){
                                       registry_updated = true;
                                       sendResponse();
                                   }
                               });

                           }

                           setLikesForPath(objectURL + "/", newLikes, function (content) {
                               if (content == null){
                                   likes_count_updated = true;
                                   sendResponse();
                               } else {
                                   console.log(content);
                                   response.status(500).send("Internal Error");
                               }
                           });

                       });

                   });
               } else {
                   response.status(404).send(failure404);
               }
            });

        }

    } catch (error) {
        console.log(error);
        response.status(403).send(paramError);
    }

});

app.post("/like/review_image", function (request, response) {

    try {

        var businessID = request.body.business_id;
        var storeID = request.body.store_id;
        var imageID = request.body.image_id;
        var userID = request.body.user_id;
        var userName = request.body.name;
        var userSurname = request.body.surname;
        var businessName = request.body.business_name;
        var uploaderID = request.body.uploader_id;
        var imageURL = request.body.url;

        if (parametersInvalid([businessID, storeID, imageID, userID, userName, userSurname, businessName, uploaderID, imageURL])){
            throw paramError;
        } else {

            var currentTime = Date.now();

            var registry_updated = false;
            var like_count_updated = false;
            var notification_sent = false;

            var objectURL = "businesses/" +businessID+ "/stores/" +storeID+ "/images/reviews/" +
                "" +imageID;

            function sendResponse() {
                if(registry_updated && like_count_updated && notification_sent){
                    response.status(200).send(success200);
                }
            }

            doesReviewImageExist(businessID, storeID, imageID, function (exists) {

                if (exists){
                    firebase.database().ref("likes_registry/" +objectURL+ "/likes/" +userID).once("value").then(function (snapshot) {

                        likesForPath(objectURL + "/likes", function (totalLikes) {

                            var newLikes = 0;

                            if (snapshot.exists()){

                                notification_sent = true;

                                if (totalLikes != 0){
                                    newLikes = totalLikes - 1;
                                }


                                //remove userID from registry
                                removeUserIDFromRegistryWithPath("likes_registry/" +objectURL+ "/likes/" +userID, function (content) {
                                    if(content == null){

                                        registry_updated = true;
                                        sendResponse();

                                    } else {
                                        console.log(content);
                                        response.status(500).send("Internal Error");
                                    }
                                });

                            } else {

                                newLikes = totalLikes + 1;

                                //add user to registry
                                addUserIDToRegistryWithPath("likes_registry/" +objectURL+ "/likes/", userID, function (content) {
                                    if(content == null){
                                        registry_updated = true;
                                        sendResponse();
                                    } else {
                                        console.log(content);
                                        response.status(500).send("Internal Error");
                                    }
                                });

                                var type = {
                                    type: "image_liked",
                                    url: imageURL,
                                    store_id: storeID
                                };

                                // console.log("variables");
                                // console.log(uploaderID);

                                //send notification
                                notifyUser(uploaderID, userID, businessName, businessID, storeID, imageID, userName, userSurname, type, totalLikes, {}, function (content) {
                                    if (content == null){
                                        notification_sent = true;
                                        sendResponse();
                                    }  else {
                                        console.log(content);
                                        response.status(500).send("Internal Error");
                                    }
                                });
                            }

                            setLikesForPath(objectURL + "/", newLikes, function (content) {
                                if (content == null){
                                    like_count_updated = true;
                                    sendResponse();
                                } else {
                                    console.log(content);
                                    response.status(500).send("Internal Error");
                                }
                            });

                        });

                    });
                } else {
                    response.status(404).send(failure404);
                }

            });

        }

    } catch (error) {
        console.log(error);
        response.status(403).send(paramError);
    }

});

app.post("/like/store", function (request, response) {

    try {

        var businessID = request.body.business_id;
        var storeID = request.body.store_id;
        var userID = request.body.user_id;

        if (parametersInvalid([businessID, storeID, userID])){
            throw paramError;
        } else {

            doesStoreExist(businessID, storeID, function (exists) {
                if (exists){
                    var registry_updated = false;
                    var like_count_updated = false;

                    var objectURL = "businesses/" +businessID+ "/stores/" +storeID;

                    function sendResponse() {
                        if (registry_updated && like_count_updated) {
                            response.status(200).send(success200);
                        }
                    }

                    firebase.database().ref("likes_registry/" +objectURL+ "/likes/" +userID).once("value").then(function (snapshot) {

                        likesForPath(objectURL + "/likes", function (totalLikes) {

                            var newLikes = 0;

                            if (snapshot.exists()){

                                if (totalLikes != 0) {
                                    newLikes = totalLikes - 1;
                                }

                                removeUserIDFromRegistryWithPath("likes_registry/" +objectURL+ "/likes/" +userID, function (content) {
                                    if (content == null){
                                        registry_updated = true;
                                        sendResponse();
                                    } else {
                                        console.log(content);
                                        response.status(500).send("Internal Error");
                                    }
                                });

                            } else {

                                newLikes = totalLikes + 1;

                                addUserIDToRegistryWithPath("likes_registry/" +objectURL+ "/likes/", userID, function (content) {
                                    if(content == null){
                                        registry_updated = true;
                                        sendResponse();
                                    } else {
                                        console.log(content);
                                        response.status(500).send("Internal Error");
                                    }
                                });

                            }

                            setLikesForPath(objectURL+ "/", newLikes, function (content) {
                                if(content == null){
                                    like_count_updated = true;
                                    sendResponse();
                                } else {
                                    console.log(content);
                                    response.status(500).send("Internal Error");
                                }
                            });

                        });

                    }, function (error) {
                        console.log(error);
                        response.status(500).send("Internal Error");
                    });

                } else {
                    response.status(404).send(failure404);
                }
            });

        }

    } catch (error) {
        console.log(error);
        response.status(403).send(paramError);
    }

});

app.post("/like/item", function (request, response) {

    try {

        var businessID = request.body.business_id;
        var storeID = request.body.store_id;
        var itemGroupID = request.body.item_group_id;
        var itemID = request.body.item_id;
        var type = request.body.type;
        var userID = request.body.user_id;

        if (parametersInvalid([businessID, storeID, itemGroupID, itemID])){
            throw paramError;
        } else {

            doesItemExist(businessID, storeID, type, itemGroupID, itemID, function (exists) {
                if(exists){
                    var registry_updated = false;
                    var like_count_updated = false;

                    var objectURL = "businesses/" +businessID+ "/" +type+ "/" +itemGroupID+ "/items/" +itemID;
                    var itemURL = "businesses/" +businessID+ "/" +type+ "_manifest/" +itemGroupID+ "/" +itemID;

                    function sendResponse() {
                        if(registry_updated && like_count_updated){
                            response.status(200).send(success200);
                        }
                    }

                    firebase.database().ref("likes_registry/" +objectURL+ "/likes/" +userID).once("value").then(function (snapshot) {

                        likesForPath(itemURL +"/likes", function (totalLikes) {

                            var newLikes = 0;

                            if (snapshot.exists()){

                                if (totalLikes != 0){
                                    newLikes = totalLikes - 1;
                                }

                                removeUserIDFromRegistryWithPath("likes_registry/" +objectURL+ "/likes/" +userID, function (content) {
                                    if (content == null){

                                        registry_updated = true;
                                        sendResponse();

                                    } else {
                                        console.log(error);
                                        response.status(500).send("Internal Error");
                                    }

                                });

                            } else {

                                newLikes = totalLikes + 1;

                                addUserIDToRegistryWithPath("likes_registry/"+objectURL+"/likes/", userID, function (content) {
                                    if(content == null){
                                        registry_updated = true;
                                        sendResponse();
                                    } else {
                                        console.log(error);
                                        response.status(500).send("Internal Error");
                                    }
                                });

                            }

                            setLikesForPath(itemURL+ "/", newLikes, function (content) {
                                if(content == null){
                                    like_count_updated = true;
                                    sendResponse();
                                } else {
                                    console.log(error);
                                    response.status(500).send("Internal Error");
                                }
                            });

                        });

                    });

                } else {
                    response.status(404).send(failure404);
                }
            });

        }

    } catch (error) {
        console.log(error);
        response.status(403).send(paramError);
    }

});

app.post("/like/reply", function (request, response) {

    try {

        var businessID = request.body.business_id;
        var storeID = request.body.store_id;
        var reviewID = request.body.review_id;
        var commentID = request.body.comment_id;
        var replyID = request.body.reply_id;

        var userID = request.body.user_id;
        var commenterID = request.body.commenter_id;
        var userName = request.body.name;
        var userSurname = request.body.surname;
        var businessName = request.body.business_name;
        var commentText = request.body.comment_text;

        if (parametersInvalid([businessID, storeID, reviewID, commentID, replyID, userID, commenterID, userName, userSurname, businessName, commentText])) {
            throw paramError;
        } else {

            doesReplyExist(businessID, storeID, reviewID, commentID, replyID, function(exists){
                if(exists){
                    var currentTime = Date.now();

                    var registry_updated = false;
                    var likes_count_updated = false;
                    var notification_sent = false;

                    function sendResponse() {
                        if (registry_updated && likes_count_updated && notification_sent){
                            response.status(200).send(success200);
                        }
                    }

                    //DETERMINE IF USER HAS LIKED THIS OBJECT
                    firebase.database().ref("likes_registry/businesses/" +businessID+ "/stores/" +storeID+ "/reviews/" +reviewID+ "/comments/" +commentID+ "/replies/" +replyID+ "/"+ userID).once("value").then(function (snapshot) {

                        likesForPath("businesses/" + businessID+ "/stores/" +storeID+ "/replies/" +
                            "" +reviewID+ "/" +commentID+ "/" +replyID+ "/likes", function (totalLikes) {

                            //if user has liked, unlike it
                            if (snapshot.exists()){

                                notification_sent = true;

                                if (totalLikes != 0){

                                    var newLikes = totalLikes - 1;

                                    setLikesForPath("businesses/" +businessID+ "/stores/" +storeID+ "/replies/" +reviewID+ "/" +
                                        "" +commentID +"/" +replyID+ "/", newLikes, function (content) {

                                        //success
                                        if (content == null){
                                            likes_count_updated = true;
                                            sendResponse();
                                            //fail
                                        } else {
                                            console.log(error);
                                            response.status(500).send("Internal Error");
                                        }

                                    });

                                } else {
                                    likes_count_updated = true;
                                    sendResponse();
                                }

                                //remove user id from registry
                                removeUserIDFromRegistryWithPath("likes_registry/businesses/" +businessID+ "/stores/" +storeID+ "/reviews/" +reviewID+ "/comments/" +
                                    "/" +commentID+ "/replies/" +replyID+ "/" +userID, function (content) {

                                    // success
                                    if (content == null){
                                        registry_updated = true;
                                        sendResponse();
                                        //fail
                                    } else {
                                        console.log(content);
                                        response.status(500).send("Internal Error");
                                    }

                                });

                                //user has not liked, so like it
                            } else {

                                var newLikes = totalLikes + 1;
                                // console.log("NEW LIKES ARE: "+newLikes);
                                // set new likes amount
                                setLikesForPath("businesses/" +businessID+ "/stores/" +storeID+ "/replies/" +reviewID+ "/" +
                                    "" +commentID+ "/" + replyID+ "/", newLikes, function (content) {

                                    if (content == null){
                                        likes_count_updated = true;
                                        sendResponse();
                                    } else {
                                        console.log(content);
                                        response.status(500).send("Internal Error");
                                    }

                                });

                                // add userID to registry
                                addUserIDToRegistryWithPath("likes_registry/businesses/" +businessID+ "/stores/" +storeID+ "/reviews/" +reviewID+ "/comments/" +
                                    "/" +commentID+ "/replies/" + replyID+ "/", userID, function (content) {
                                    if (content == null) {
                                        registry_updated = true;
                                        sendResponse();
                                    } else {
                                        console.log(content);
                                        response.status(500).send("Internal Error");
                                    }
                                });

                                var type = {
                                    type: "comment_liked",
                                    text: commentText,
                                    store_id: storeID,
                                    review_id: reviewID
                                };

                                // notify commenter
                                notifyUser(commenterID, userID, businessName, businessID, storeID, commentID, userName, userSurname,
                                    type, totalLikes, {}, function (content) {
                                        if (content == null){
                                            notification_sent = true;
                                            sendResponse();
                                        } else {
                                            response.status(500).send("Internal Error");
                                        }
                                    });
                            }


                        });


                    });

                } else {
                    response.status(404).send(failure404);
                }
            });

        }

    } catch (e){
        console.log(e);
        response.status(403).send(failure403);
    }

});

// MARK: Create Business

app.get("/create_business/verify_user", function(request, response){

    response.status(200);
    response.send("success");

});

app.post("/create_business", function(request, response){
    try {
        var name = request.body["business_name"];
        var logo = request.body["logo"];
        var lat = request.body["lat"];
        var long = request.body["long"];
        var user_id = request.body["user_id"];
        var business_id = request.body["business_id"];
        var store_id = request.body["store_id"];
        var store_email = request.body["store_email"];
        var store_phone = request.body["store_phone"];
        var home_image = request.body["store_image"];
        var location_name = request.body["location_name"];

        var monday_open = request.body["monday_open"];
        var tuesday_open = request.body["tuesday_open"];
        var wednesday_open = request.body["wednesday_open"];
        var thursday_open = request.body["thursday_open"];
        var friday_open = request.body["friday_open"];
        var saturday_open = request.body["saturday_open"];
        var sunday_open = request.body["sunday_open"];

        var monday_close = request.body["monday_close"];
        var tuesday_close = request.body["tuesday_close"];
        var wednesday_close = request.body["wednesday_close"];
        var thursday_close = request.body["thursday_close"];
        var friday_close = request.body["friday_close"];
        var saturday_close = request.body["saturday_close"];
        var sunday_close = request.body["sunday_close"];

        if (parametersInvalid([name, logo, lat, long, user_id, business_id, store_id, store_email, store_phone, home_image,
                monday_open, monday_close, tuesday_open, tuesday_close, wednesday_open, wednesday_close, thursday_open,
                thursday_close, friday_open, friday_close, saturday_open, saturday_close, sunday_open, sunday_close, location_name])) {

            throw paramError;

        } else {

            userMayAddBusiness(user_id, function(userMayAdd){

                if (userMayAdd){

                    var businessPost_complete = false;
                    var userPost_complete = false;
                    var storePost_complete = false;
                    var algolia_updated = false;
                    var daily_limit_updated = false;

                    var geo_address;
                    var uct_offset;

                    var completed_geocode_query = false;
                    var completed_timezone_query = false;

                    var geocodeURL = "https://maps.googleapis.com/maps/api/geocode/json?" +
                        "latlng="+lat+","+long+
                        "&key=" + geocodeAPIKEY;
                    var timezoneURL = "https://maps.googleapis.com/maps/api/timezone/json?" +
                        "location="+lat+","+long+"&timestamp=1331161200&key=" +timezoneAPIKEY;

                    function sendResponse() {
                        if (daily_limit_updated && businessPost_complete && userPost_complete && storePost_complete && algolia_updated) {
                            console.log("CREATED: business_name: " + request.body.business_name + " business_id: " + request.body.business_id + " Date: " +Date.now());
                            response.status(200);
                            response.send(success200);

                            emailUser(user_id, EmailTemplate_BusinessAdded, name, function(error){
                                if(error != null){
                                    console.log(error);
                                }
                            });
                        }
                    }

                    // performs the geocode query
                    performRequest(geocodeURL, function (error, res, body) {
                        if (!error && res.statusCode == 200){

                            var body = JSON.parse(body);
                            geo_address = body.results[2].formatted_address;
                            completed_geocode_query = true;
                            continueRequest();

                        } else {
                            console.log(error);
                            response.status(500).send("Internal Error");
                        }
                    });

                    // performs the timezone query
                    performRequest(timezoneURL, function (error, res, body) {
                        if (!error && res.statusCode == 200){

                            var body = JSON.parse(body);
                            uct_offset = Number(body.rawOffset) / 3600;
                            completed_timezone_query = true;
                            continueRequest();

                        } else {
                            console.log(error);
                            response.status(500).send("Internal Error");
                        }
                    });

                    function continueRequest() {

                        if (completed_geocode_query && completed_timezone_query){

                            //adds the business ID to daily limit
                            firebase.database().ref("daily_budget/").update(function(){

                                var obj = {};
                                obj[business_id] = 0;
                                return obj;

                            }()).then(function(){
                                daily_limit_updated = true;
                                sendResponse();
                            }, function(error){
                                console.log(error);
                                response.status(500).send(failure500);
                            });

                            // builds the business into firebase
                            firebase.database().ref("businesses/" + business_id + "/").update(buildBusiness()).then(function () {
                                businessPost_complete = true;
                                sendResponse();
                            }, function (error) {
                                console.log(error);
                                response.status(500).send("Internal Error");

                            });


                            // adds the business id to the users registry
                            firebase.database().ref("users/" + user_id + "/businesses/" + business_id + "/").update(updateUser(store_id)).then(function () {
                                userPost_complete = true;
                                sendResponse();
                            }, function (error) {
                                console.log(error);
                                response.status(500).send("Internal Error");

                            });

                            var title = "Store";
                            //adds a store to the business
                            firebase.database().ref("businesses/" + business_id + "/stores/" + store_id + "/").update(buildStore(home_image, lat, long, store_email, store_phone, title, location_name, geo_address, uct_offset)).then(function () {
                                storePost_complete = true;
                                sendResponse();
                            }, function (error) {
                                console.log(error);
                                response.status(500).send("Internal Error");
                            });

                            //adds the store to algolia
                            algolia_index.addObject({
                                business_name: name,
                                storeID: store_id,
                                businessID: business_id,
                                location_name: location_name,
                                geo_address: geo_address,
                                _geoloc: {
                                    lat: Number(lat),
                                    lng: Number(long)
                                }

                            }, store_id, function(error, constant){
                                if (error){
                                    print(error)
                                } else {
                                    algolia_updated = true;
                                    sendResponse();
                                }
                            });
                        }

                    }

                    function buildBusiness() {
                        var obj = {
                            name: name,
                            logo: logo,
                            is_verified: false,

                            business_hours: {},

                            permissions: {
                                owner: {
                                    read: {
                                        verification: true,
                                        advertising: true
                                    },
                                    write: {
                                        logo: true,
                                        description: true,
                                        items: true,
                                        business_hours: true,
                                        verification: true,
                                        contact_details_business: true,
                                        contact_details_store: true,
                                        categories: true,
                                        members_business: true,
                                        members_store: true,
                                        stores: true,
                                        store_home_image: true,
                                        advertising: true,
                                        promotion: true,
                                        images: true,
                                        reply: true,
                                        location: true
                                    }
                                },
                                admin: {
                                    read: {
                                        logo: true,
                                        description: true,
                                        single_promotion_status: true,
                                        products: true,
                                        services: true,
                                        rent: true,
                                        business_hours: true,
                                        verification: true,
                                        contact_details_store: true,
                                        contact_details_business: true,
                                        members: true,
                                        stores: true
                                    },
                                    write: {
                                        logo: true,
                                        description: true,
                                        single_promotion_status: true,
                                        products: true,
                                        services: true,
                                        rent: true,
                                        business_hours: true,
                                        verification: true,
                                        contact_details_store: true,
                                        contact_details_business: true,
                                        members: true,
                                        stores: true
                                    }
                                },
                                member: {
                                    read: {
                                        logo: true,
                                        description: true,
                                        single_promotion_status: true,
                                        products: true,
                                        services: true,
                                        rent: true,
                                        business_hours: true,
                                        verification: true,
                                        contact_details_store: true,
                                        contact_details_business: true,
                                        members: true,
                                        stores: true
                                    },
                                    write: {
                                        logo: true,
                                        description: true,
                                        single_promotion_status: true,
                                        products: true,
                                        services: true,
                                        rent: true,
                                        business_hours: true,
                                        verification: true,
                                        contact_details_store: true,
                                        contact_details_business: true,
                                        members: true,
                                        stores: true
                                    }
                                },
                                store_admin: {
                                    read: {
                                        logo: true,
                                        description: true,
                                        single_promotion_status: true,
                                        products: true,
                                        services: true,
                                        rent: true,
                                        business_hours: true,
                                        verification: true,
                                        contact_details_store: true,
                                        contact_details_business: true,
                                        members: true,
                                        stores: true
                                    },
                                    write: {
                                        logo: true,
                                        description: true,
                                        single_promotion_status: true,
                                        products: true,
                                        services: true,
                                        rent: true,
                                        business_hours: true,
                                        verification: true,
                                        contact_details_store: true,
                                        contact_details_business: true,
                                        members: true,
                                        stores: true
                                    }
                                },
                                store_member: {
                                    read: {
                                        logo: true,
                                        description: true,
                                        single_promotion_status: true,
                                        products: true,
                                        services: true,
                                        rent: true,
                                        business_hours: true,
                                        verification: true,
                                        contact_details_store: true,
                                        contact_details_business: true,
                                        members: true,
                                        stores: true
                                    },
                                    write: {
                                        logo: true,
                                        description: true,
                                        single_promotion_status: true,
                                        products: true,
                                        services: true,
                                        rent: true,
                                        business_hours: true,
                                        verification: true,
                                        contact_details_store: true,
                                        contact_details_business: true,
                                        members: true,
                                        stores: true
                                    }
                                }
                            }
                        };

                        var membersObj = {};
                        membersObj[user_id] = "owner";

                        var storeList = {};
                        if (location_name !== ""){
                            storeList[store_id] = location_name +", "+ geo_address;
                        } else {
                            storeList[store_id] = geo_address;
                        }

                        obj["members"] = membersObj;
                        obj["store_manifest"] = storeList;

                        var standardHours = {
                            monday_open: monday_open,
                            tuesday_open: tuesday_open,
                            wednesday_open: wednesday_open,
                            thursday_open: thursday_open,
                            friday_open: friday_open,
                            saturday_open: saturday_open,
                            sunday_open: sunday_open,

                            monday_close: monday_close,
                            tuesday_close: tuesday_close,
                            wednesday_close: wednesday_close,
                            thursday_close: thursday_close,
                            friday_close: friday_close,
                            saturday_close: saturday_close,
                            sunday_close: sunday_close
                        };

                        var standard_hours = {};

                        standard_hours["standard"] = standardHours;

                        obj["business_hours"] = standard_hours;

                        return obj;
                    }

                } else {

                    response.status(400).send(businessLimitReachedError);

                }

            });

        }

    } catch (e){
        console.log(e);
        response.status(403).send(paramError);
    }

});

app.post("/create_business/user_info", function(request, response){

    try {

        var user_id = request.body["user_id"];
        var user_email = request.body["user_email"];
        var user_phone = request.body["user_phone"];
        var user_id_number = request.body["user_id_number"];

        if (parametersInvalid([user_email, user_id, user_id_number, user_phone])){
            throw paramError;
        } else {

            firebase.database().ref("users/" +user_id+ "/details").update({
                email: user_email,
                phone: user_phone,
                id_number: user_id_number
            }).then(function () {
                response.status(200).send(success200);
            }, function (error) {
                response.status(500).send("Internal Error");
            });

        }

    } catch (e) {
        response.status(403).send(e);
    }

});

// MARK: Admin

app.post("/change_business_owner", function(request, response){
    try {

        var businessID = request.body.business_id;
        var newOwnerEmail = request.body.new_owner_email;

        if (parametersInvalid([businessID, newOwnerEmail])){
            throw paramError;
        } else {

            doesBusinessExist(businessID, function (exists) {
               if (exists){
                   var oldOwnerRemoved = false;
                   var newOwnerAdded = false;

                   function sendResponse(){
                       if (oldOwnerRemoved && newOwnerAdded){
                           response.status(200).send(success200);
                       }
                   }

                   firebase.database().ref("users_lookup/").orderByValue().equalTo(newOwnerEmail).on("child_added", function(snapshot){

                       firebase.database().ref("users_lookup/").off();

                       if (snapshot.exists()){

                           addUserToBusinessRole(businessID, snapshot.key, "owner", function(error){
                               if (error == null){
                                   newOwnerAdded = true;
                                   sendResponse();
                               } else {
                                   console.log(error);
                                   response.status(500).send(failure500);
                               }
                           })

                       }

                   });

                   firebase.database().ref("businesses/" +businessID+ "/members/").orderByValue().equalTo("owner").on("child_added", function(snapshot){

                       firebase.database().ref("businesses/" +businessID+ "/members/").off();

                       if (snapshot.exists()){

                           addUserToBusinessRole(businessID, snapshot.key, "admin", function(error){
                               if (error == null){
                                   oldOwnerRemoved = true;
                                   sendResponse();
                               } else {
                                   console.log(error);
                                   response.status(500).send(failure500);
                               }
                           });

                       }

                   });
               } else {
                   response.status(404).send(failure404);
               }
            });

        }

    } catch (e){
        console.log(e);
        response.status(403).send(failure403);
    }
});

app.post("/notifyAllUsers", function (request, response) {

    // console.log("here");
    // console.log(request.body);

    try {

        var userIDS = request.body.user_ids;
        var businessID = request.body.business_id;
        var business_name = request.body.business_name;
        var type = request.body.type;
        var storeID = request.body.store_id;
        var key = request.body.key;

        if(parametersInvalid([userIDS, businessID, storeID, business_name, type, key]) || key != internalKey){
            throw paramError;
        } else {

            var toNotify = userIDS;
            var currentTime = Date.now();
            var temp = [];

            function sendResponse(){
                if (toNotify.length == temp.length){
                    response.status(200).send(success200);
                    clearTimeout(timer);
                }
            }

            var obj = request.body;

            var timer = setTimeout(function breakExecution () {

                var newUserIDS = [];
                var newTempArray = temp;

                for (var i = 0; i < userIDS.length; i++){
                    if (newTempArray.indexOf(userIDS[i]) == -1){
                        newUserIDS.push(userIDS[i]);
                    }
                }

                obj["user_ids"] = newUserIDS;

                var options = {
                    method: "post",
                    body: obj,
                    json: true
                };

                performRequest("https://stackle-live.appspot.com/notifyAllUsers", options, function (error, response, body) {
                    if (error == null){
                        response.status(200).send(success200);
                    } else {
                        console.log(error);
                    }
                });

                // performRequest.post("https://stackle-1278.appspot.com/notifyAllUsers", { json: {
                //
                //     "user_ids": toNotify,
                //     "business_id": businessID,
                //     "business_name": business_name,
                //     "type": type,
                //     "store_id": storeID,
                //     "key": key
                //
                // }});

                response.status(200).send(success200);

            }, 50000);

            var userObj = obj;
            userObj["key"] = null;
            userObj["user_ids"] = null;

            userIDS.forEach(function (userID) {

                firebase.database().ref("users/" +userID+ "/notifications/" +businessID).update(userObj).then(function () {

                    temp.push(userID);
                    sendResponse();

                }, function (error) {
                    console.log(error);
                    response.status(500).send(failure500);
                });

            });

        }

    } catch (error) {
        console.log(error);
        response.status(403).send(failure403);
    }

});

app.post("/createNewCategory", function (request, response) {

    try {

        var id = request.body.category_id;
        var title = request.body.title;
        var imageURL = request.body.image_url;
        var is_available = request.body.is_available;
        var subcategories = request.body.subcategories;

        if (parametersInvalid([id, title, imageURL, is_available, subcategories]) || !Array.isArray(subcategories)){

            throw paramError;

        } else {

            firebase.database().ref("category_list/" +id.toLowerCase()).update({
                title: title,
                image: imageURL,
                is_available: is_available,
                last_updated: Date.now(),
                subcategories: subcategories
            }).then(function () {
                response.status(200).send("Category successfully created / updated");
            }, function (error) {
                response.status(500).send("An Internal Error occurred. Check the logs for more details.");
                console.log(error);
            });

        }

    } catch (error){
        console.log(error);
        response.status(403).send(failure403);
    }

});

app.post("/verification/set", function (request, response) {

    try {

        var businessID = request.body.business_id;
        var name = request.body.name;
        var surname = request.body.surname;
        var approved = request.body.approval;
        var reason = request.body.reason;

        if (parametersInvalid([businessID, name, surname, approved, reason])){
            throw paramError;
        } else {

            var business_updated = false;
            var verification_approved = false;
            var request_removed = false;

            approved = approved.toLowerCase();

            function sendResponse() {
                if (business_updated && verification_approved && request_removed){
                    response.status(200).send(success200);
                }
            }

            firebase.database().ref("verification_requests/businesses/" +businessID).remove().then(function(){

                request_removed = true;
                sendResponse();

            }, function(error){
                console.log(error);
                response.status(500).send(failure500);
            });

            firebase.database().ref("businesses/" +businessID).update(function(){
                is_verified: approved == "approved";
            }()).then(function(){
                verification_approved = true;
                sendResponse();
            }, function (error) {
                console.log(error);
                response.status(500).send(failure500);
            });

            firebase.database().ref("businesses/" +businessID+ "/verification/").update(function () {
                var obj = {};

                if (approved) {
                    obj["status"] = "approved";
                } else {
                    obj["status"] = "rejected";
                    obj["reason"] = reason;
                }

                obj["handled_by"] = name + " " + surname;
                obj["date_of_approval"] = Date.now();

                return obj;
            }()).then(function () {

                business_updated = true;
                sendResponse();

            }, function (error) {
                console.log(error);
                sendResponse();
            });

        }

    } catch (error){

        console.log(error);
        response.status(403).send(failure403);

    }

});

app.post("/email", function (request, response) {

    try {

        var emailAddress = request.body.email_address;
        var subject = request.body.subject;
        var message = request.body.message;

        if (parametersInvalid([emailAddress, message, subject])){
            throw paramError;
        } else {

            mailgun.messages().send({

                to: emailAddress,
                from: "Stackle <admin@stackle.co.za>",
                subject: subject,
                html: message

            }, function (error, body){

                if (error){

                    console.log(error);

                } else {
                    response.status(200).send(success200);
                }

            });

        }

    } catch (e) {
        console.log(e);
        response.status(403).send(failure403);
    }

});

app.get("/daily_tasks", function (request, response) {

    response.status(200).send();

    //perform final deletes
    performFinalDelete(function(error){
        if (error == null){

            techEmails.forEach(function (email){

                // console.log({from: "engine@stackle.co.za",
                //     to: email,
                //     subject: "Final Delete Requests",
                //     text: "All final delete requests completed successfully."});

                mailgun.messages().send({
                    from: "engine@stackle.co.za",
                    to: email,
                    subject: "Final Delete Requests",
                    text: "All final delete requests completed successfully."
                }, function(error, body){
                    if (error){
                        console.log(error);
                    }
                });

            });

        } else {
            techEmails.forEach(function (email) {

                // console.log({
                //     from: "engine@stackle.co.za",
                //     to: email,
                //     subject: "Final Delete Requests",
                //     text: error
                // });

                mailgun.messages().send({
                    from: "engine@stackle.co.za",
                    to: email,
                    subject: "Final Delete Requests",
                    text: error
                }, function(error, body){
                    if (error){
                        console.log(error);
                    }
                });
            });

        }
    });

    //remove all expired confirmation links
    removeOldConfirmationLinks(function(error){
        if (error == null){

            techEmails.forEach(function (email){

                // console.log({
                //     from: "engine@stackle.co.za",
                //     to: email,
                //     subject: "Out of Date Confirmation Links",
                //     text: "All old confirmation links for remove stores and add members have been removed."
                // });

                mailgun.messages().send({
                    from: "engine@stackle.co.za",
                    to: email,
                    subject: "Out of Date Confirmation Links",
                    text: "All old confirmation links for remove stores and add members have been removed."
                }, function(error, body){
                    if (error){
                        console.log(error);
                    }
                });

            });

        } else {



            techEmails.forEach(function (email) {

                // console.log({
                //     from: "engine@stackle.co.za",
                //     to: email,
                //     subject: "Out of Date Confirmation Links",
                //     text: error
                // });

                mailgun.messages().send({
                    from: "engine@stackle.co.za",
                    to: email,
                    subject: "Out of Date Confirmation Links",
                    text: error
                }, function(error, body){
                    if (error){
                        console.log(error);
                    }
                });
            });

        }
    });

    //reset the daily budget for applicable businesses
    resetDailyBudget(function (error){
        if (error == null){

            techEmails.forEach(function (email){

                // console.log({
                //     from: "engine@stackle.co.za",
                //     to: email,
                //     subject: "Daily Budget Reset Successfully",
                //     text: "The daily budget for all businesses was reset successfully."
                // });

                mailgun.messages().send({
                    from: "engine@stackle.co.za",
                    to: email,
                    subject: "Daily Budget Reset Successfully",
                    text: "The daily budget for all businesses was reset successfully."
                }, function(error, body){
                    if (error){
                        console.log(error);
                    }
                });

            });

        } else {

            techEmails.forEach(function (email) {

                // console.log({
                //     from: "engine@stackle.co.za",
                //     to: email,
                //     subject: "Daily Budget Reset Error",
                //     text: error
                // });

                mailgun.messages().send({
                    from: "engine@stackle.co.za",
                    to: email,
                    subject: "Daily Budget Reset Error",
                    text: error
                }, function(error, body){
                    if (error){
                        console.log(error);
                    }
                });
            });

        }
    });

});

app.get("/weekly_tasks", function(request, response){

    response.status(200).send();
    var currentWeek = getWeek() - 1;

    if (currentWeek < 0){
        currentWeek = 52
    }

    // email the weekly sales to admin
    firebase.database().ref("stackle_account/bidwords/" +currentWeek).once("value").then(function(snapshot){
        if(snapshot.exists()){

            // console.log({from: "engine@stackle.co.za",
            //     to: "wesley@stackle.co.za",
            //     subject: "Weekly Sales",
            //     text: "Income for week: " + currentWeek + " : " + snapshot.val()});

            mailgun.messages().send({
                from: "engine@stackle.co.za",
                to: "wesley@stackle.co.za",
                subject: "Weekly Sales",
                text: "Income for week: " + currentWeek + " : " + snapshot.val()
            }, function(error, body){
                if (error){
                    console.log(error);
                }
            });
            //
            // mailgun.messages().send({
            //     from: "engine@stackle.co.za",
            //     to: "dane-s@telkomsa.net",
            //     subject: "Weekly Sales",
            //     text: "Income for week: " + currentWeek + " : " + snapshot.val()
            // }, function(error, body){
            //     if (error){
            //         console.log(error);
            //     }
            // });
        }
    });

    //email the weekly statistics to business owners & admin
    emailStatistics(function(error){
        if (error == null){
            // console.log({from: "engine@stackle.co.za",
            //         to: "wesley@stackle.co.za",
            //         subject: "Weekly Stats",
            //         text: "Weekly statistics successfully emailed to all business owners"});

            mailgun.messages().send({
                from: "engine@stackle.co.za",
                to: "wesley@stackle.co.za",
                subject: "Weekly Stats",
                text: "Weekly statistics successfully emailed to all business owners"
            }, function(error, body){
                if (error){
                    console.log(error);
                }
            });
        }
    });



});

// MARK: Website
app.get("/faq", function(request, response){
    response.render("FAQ.html");
});

app.get("/privacy", function(request, response){
    response.render("privacy.html");
});

app.get("/careers", function(request, response){
    response.render("careers.html");
});

app.get("/team", function(request, response){
    response.render("team.html");
});

app.get("/business-docs", function(request, response){
    response.render("underConstruction.html");
});

app.get("/terms-conditions", function(request, response){
    response.render("T&C.html");
});

app.get("/", function(request, response){
    response.render("index.html");
});

// MARK: Bad Requests

app.get("*", function(request, response){
    response.status(404).render("404.html");
});

app.post("*", function(request, response){
    response.status(404).send(pathNotFound);
});

app.delete("*", function(request, response){
    response.status(404).send(pathNotFound);
});

app.put("*", function (request, response) {

    response.status(404).send(pathNotFound);

});

// MARK: CRON JOBS

function emailStatistics(callback){

    var timer = setTimeout(function(){closeSession()}, halfHour);

    function closeSession(){
        firebase.database().ref("daily_budget/").off();
        firebase.database().ref("unowned_businesses/").off();
        // console.log("session cleared");
        callback(null);
    }

    // get each owned business ID
    firebase.database().ref("daily_budget/").on("child_added", function(childSnapshot, previousSnapshot){

        if (childSnapshot.exists()){

            var hasBusinessOwnerEmail = false;
            var hasWeeklyStatistics = false;
            var hasBusinessAdminEmail = false;

            var emailAddress = [];
            var ownerEmail = [];
            var adminEmail = [];
            var weeklyStats;

            function sendEmail(){
                if (hasBusinessOwnerEmail && hasWeeklyStatistics && hasBusinessAdminEmail){

                    emailAddress = ownerEmail.concat(adminEmail);

                    emailAddress.forEach(function(address){

                        var email = {
                            from: "Stackle Stats<noreply@stackle.co.za>",
                            to: address,
                            subject: "Weekly Round Up"
                        };

                        var text = "Hi,\n\n" +
                            "" +
                            "Here is the weekly round up for your business.\n\n" +
                            "" +
                            "Total Searches: " + weeklyStats["total_views"] +"\n" +
                            "Total Clicks: " + weeklyStats["total_clicks"] +"\n" +
                            "Expenditure this week: " +weeklyStats["weekly_expenditure"] + "\n\n" +
                            "Details:\n\n";

                        if (weeklyStats["searches"] != undefined){
                            var searches = Object.keys(weeklyStats["searches"]);

                            var clicks = Object.keys(weeklyStats["clicks"]);

                            for (var i = 0; i < searches.length && searches.length > 0; i++){

                                if (i == 0){
                                    text += "Mostly found with:\n";
                                }

                                text += searches[i] + " - " + weeklyStats["searches"][searches[i]] + "\n";

                            }

                            text += "\n";
                        }

                        if (weeklyStats["clicks"] != undefined) {
                            var clicks = Object.keys(weeklyStats["clicks"]);

                            for (var i = 0; i < clicks.length && clicks.length > 0; i++){

                                if (i == 0){
                                    text += "Most clicks / interactions with:\n";
                                }

                                text += clicks[i] + " - " + weeklyStats["clicks"][clicks[i]] + "\n";

                            }
                        }

                        text += "\nShould you have any queries please get in contact with us at:\n" +
                            "email: info@stackle.co.za\n" +
                            "phone: 021 851 2820\n\n" +
                            "" +
                            "We trust having your business on Stackle has been a rewarding experience.\n\n" +
                            "" +
                            "Sincerely,\n" +
                            "The Stackle Team";

                        email["text"] = text;

                        mailgun.messages().send(email, function(error, body){
                            if(error){
                                console.log(error);
                            }
                        });

                    });

                }
            }



            getOwnerEmailForBusinessID(childSnapshot.key, function(email){
               if (email != null) {
                   ownerEmail.push(email);
               }

                hasBusinessOwnerEmail = true;
                sendEmail();

            });

            getAdminEmailForBusinessID(childSnapshot.key, function(emails){

                if (emails != null){
                    adminEmail = emails;
                }

                hasBusinessAdminEmail = true;
                sendEmail();

            });

            getWeeklyStatisticsForBusinessID(childSnapshot.key, function(data){

                if (data != null){
                    hasWeeklyStatistics = true;
                    weeklyStats = data;
                    sendEmail();
                } else {
                    console.log("Weekly Task Error: \n Failed to get business statistics for business ID \n businessID: " + childSnapshot.key);
                }

            });

        } else {

        }

    });

    //get each unowned business ID
    firebase.database().ref("unowned_businesses/").on("child_added", function(childSnapshot, previousSnapshot){

        if (childSnapshot.exists()) {

            var members = Object.keys(childSnapshot.val().members);

            if (members.length != 0) {

                getWeeklyStatisticsForBusinessID(childSnapshot.key, function (weeklyStats) {

                    members.forEach(function (member) {

                        var email = {
                            from: "Stackle Stats<noreply@stackle.co.za>",
                            to: member,
                            subject: "Weekly Round Up"
                        };

                        var text = "Hi,\n\n" +
                            "" +
                            "Here is the weekly round up for your business.\n\n" +
                            "" +
                            "Total Searches: " + weeklyStats["total_views"] +"\n" +
                            "Total Clicks: " + weeklyStats["total_clicks"] +"\n\n" +
                            "Details:\n\n";

                        if (weeklyStats["searches"] != undefined){
                            var searches = Object.keys(weeklyStats["searches"]);

                            var clicks = Object.keys(weeklyStats["clicks"]);

                            for (var i = 0; i < searches.length && searches.length > 0; i++){

                                if (i == 0){
                                    text += "Mostly found with:\n";
                                }

                                text += searches[i] + " - " + weeklyStats["searches"][searches[i]] + "\n";

                            }

                            text += "\n";
                        }

                        if (weeklyStats["clicks"] != undefined) {
                            var clicks = Object.keys(weeklyStats["clicks"]);

                            for (var i = 0; i < clicks.length && clicks.length > 0; i++){

                                if (i == 0){
                                    text += "Most clicks / interactions with:\n";
                                }

                                text += clicks[i] + " - " + weeklyStats["clicks"][clicks[i]] + "\n";

                            }
                        }

                        text += "\nShould you have any queries please get in contact with us at:\n" +
                            "email: info@stackle.co.za\n" +
                            "phone: 021 851 2820\n\n" +
                            "" +
                            "If you no longer wish to receive these weekly statistics please click here " +
                            "" +
                            "Sincerely,\n" +
                            "The Stackle Team";

                        email["text"] = text;

                        mailgun.messages().send(email, function(error, body){
                            if(error){
                                console.log(error);
                            }
                        });

                    });

                });
            }

        }

    });

}

function removeOldConfirmationLinks(callback){

    setTimeout(function(){closeSession()}, halfHour);

    function closeSession() {
        firebase.database().ref("confirmation_links/add_member").off();
        firebase.database().ref("confirmation_links/remove_store").off();
        firebase.database().ref("confirmation_links/remove_business").off();
        callback(null);
    }

    //remove add member confirmation links
    firebase.database().ref("confirmation_links/add_member").orderByChild("expires").endAt(Date.now()).on("child_added", function (childSnapshot, previousChildsnapshot) {

        if (childSnapshot.exists()) {

            firebase.database().ref("confirmation_links/add_member/" + childSnapshot.key).remove();

        } else {
            callback("Service was successfully initiated however a callback was received with no data attached. \nPlease check the database for any incorrect values in the daily_budget tree or in the advertising tree.");
        }

    });


    //remove store confirmation links
    firebase.database().ref("confirmation_links/remove_store").orderByValue().endAt(Date.now()).on("child_added", function (childSnapshot, previousChildsnapshot) {

        if (childSnapshot.exists()) {

            firebase.database().ref("confirmation_links/remove_store/" + childSnapshot.key).remove();

        } else {
            callback("Service was successfully initiated however a callback was received with no data attached. \nPlease check the database for any incorrect values in the daily_budget tree or in the advertising tree.");
        }

    });

    //remove business confirmation links
    firebase.database().ref("confirmation_links/remove_business").orderByValue().endAt(Date.now()).on("child_added", function (childSnapshot, previousChildsnapshot) {

        if (childSnapshot.exists()) {

            firebase.database().ref("confirmation_links/remove_business/" + childSnapshot.key).remove();

        } else {
            callback("Service was successfully initiated however a callback was received with no data attached. \nPlease check the database for any incorrect values in the daily_budget tree or in the advertising tree.");
        }

    });

}

function performFinalDelete(callback){

    setTimeout(function(){closeSession()}, halfHour);

    function closeSession() {
        firebase.database().ref("permanent_delete/stores/").off();
        firebase.database().ref("permanent_delete/reviews/").off();
        firebase.database().ref("permanent_delete/comments/").off();
        firebase.database().ref("permanent_delete/replies/").off();
        firebase.database().ref("permanent_delete/images/").off();
        firebase.database().ref("permanent_delete/businesses/").off();
        callback(null);
    }

    //business removals

    firebase.database().ref("permanent_delete/businesses/").on("child_added", function(childSnapshot, previousSnapshot){

        if (childSnapshot.exists()){

            deleteBusiness(childSnapshot.val()["business_id"], childSnapshot.key, function (error, id) {
                if (error){
                    console.log(error);
                } else {
                    firebase.database().ref("permanent_delete/businesses/" + id).remove();
                }
            });

        }

    });

    //store removals

    firebase.database().ref("permanent_delete/stores/").on("child_added", function(childSnapshot, previousSnapshot){

        if (childSnapshot.exists()){

            var store = childSnapshot.val();

            deleteStore(store["business_id"], store["store_id"], childSnapshot.key, function (error, id) {
                if (error == null){
                    firebase.database().ref("permanent_delete/stores/" + id).remove();
                }
            });

        }

    });

    //review removals

    firebase.database().ref("permanent_delete/reviews/").on("child_added", function(childSnapshot, previousSnapshot){

        if (childSnapshot.exists()){

            var review = childSnapshot.val();
            var id = childSnapshot.key;

            deleteOrphanReview(review["business_id"], review["store_id"], review["user_id"], review["image_ids"], id, function (error, id) {
               if (error == null){
                   firebase.database().ref("permanent_delete/reviews/" + id).remove();
               }
            });

        }

    });

    //comment removals
    firebase.database().ref("permanent_delete/comments/").on("child_added", function(childSnapshot, previousSnapshot){

        if (childSnapshot.exists()){

            var comment = childSnapshot.val();

            deleteComment(comment["business_id"], comment["store_id"], comment["review_id"], comment["comment_id"], childSnapshot.key, function (error, id) {
               if (error == null) {
                   firebase.database().ref("permanent_delete/comments/" +id).remove();
               }
            });

        }
    });

    //reply removals
    firebase.database().ref("permanent_delete/replies/").on("child_added", function(childSnapshot, previousSnapshot){

        if (childSnapshot.exists()){

            var reply = childSnapshot.val();

            deleteReply(reply["business_id"], reply["store_id"], reply["review_id"], reply["comment_id"], reply["reply_id"], childSnapshot.key, function (error, id) {
                if (error == null) {
                    firebase.database().ref("permanent_delete/replies/" +id).remove();
                }
            });

        }
    });

    //image removals
    firebase.database().ref("permanent_delete/images/").on("child_added", function(childSnapshot, previousSnapshot){

        if (childSnapshot.exists()){

            var image = childSnapshot.val();

            deleteImage(image["business_id"], image["store_id"], "official", image["image_id"], childSnapshot.key, function (error, id) {
                if (error == null) {
                    firebase.database().ref("permanent_delete/images/" +id).remove();
                }
            });


        }
    });

}

function resetDailyBudget(callback){

    setTimeout(function(){closeSession()}, halfHour);

    //closes the existing function after 60 seconds of no new data being received
    function closeSession(){
        firebase.database().ref("daily_budget").off();
        // console.log("session cleared");
        callback(null);
    }

    firebase.database().ref("daily_budget/").on("child_added", function (childSnapshot, previousChildsnapshot) {

        if (childSnapshot.exists()) {

            var daily_limit;

            var hasDailyLimit = false;

            firebase.database().ref("advertising/" + childSnapshot.key + "/daily_limit").once("value").then(function (snapshot) {

                // if the current daily limit doesnt equal the user defined one we need to update
                if (snapshot.exists()) {
                    //reset the daily budget to the user defined one
                    
                    if (snapshot.val() == childSnapshot.val()){
                        return;
                    }
                    
                    firebase.database().ref("daily_budget/").update(function () {

                        var obj = {};
                        obj[childSnapshot.key] = snapshot.val();
                        return obj;

                    }());

                    var dailyBudget = snapshot.val();

                    firebase.database().ref("advertising/" +childSnapshot.key+ "/deposit_remaining").once("value").then(function (snapshot) {

                        var remainingDeposit = 0;

                        if (snapshot.exists()){
                            remainingDeposit = snapshot.val();
                        }

                        var minValue = remainingDeposit < dailyBudget ? remainingDeposit : dailyBudget;

                        removeBidwordsLessThanValue(childSnapshot.key, minValue, function (error) {

                            if (error){
                                console.log("error resetting daily budget: businessID " + childSnapshot.key);
                            }

                        });

                    });

                }

            });

            firebase.database().ref("advertising/" +childSnapshot.key).once("value").then(function(snapshot){

            });
        } else {
            callback("Service was successfully initiated however a callback was received with no data attached. \nPlease check the database for any incorrect values in the daily_budget tree or in the advertising tree.");
        }

    });

}


// Checks for two userIDS being equal.
// If equal; may not report
function userMayReport(ID1, ID2) {
    return ID1 == ID2
}

function userIsInBusiness(user_id, business_id, UAuth){
    firebase.database().ref("users/" + user_id + "/businesses").once("value").then(function (user_validate) {
        var businessIDS = Object.keys(user_validate.val());

        if (businessIDS.indexOf(business_id) != -1) {
            UAuth(true);
        } else {
            console.log("Bad access attempt - Logo Update - User is not member of business\nUserID: " + user_id + " BusinessID: " + business_id);
            UAuth(false);
        }
    });
}

function userHasPermission(user_id, business_id, read_write, permission_type, UPermission){
    var user_role;

    firebase.database().ref("businesses/" + business_id + "/members/" + user_id).once("value").then(function(role){

        user_role = role.val();

    }).then(function(){
        firebase.database().ref("businesses/"+business_id+"/permissions/"+user_role+"/"+read_write+"/"+permission_type).once("value").then(function(permission){
            if (permission.val()){
                UPermission(true);
            } else {
                UPermission(403);
            }
        });
    }, function(error){
        console.log(error);
        UPermission(500);
    });
}

function buildStore(home_image_url, lat, long, store_email, store_phone, title, location_name, geo_address, gmt_offset){
    var obj = {
        home_image: home_image_url,
        "-geoloc": {
            lat: lat,
            long: long
        },
        location_name: location_name,
        geo_address: geo_address,
        utc_offset: gmt_offset
    };

    var contactDetails = {
        email: store_email,
        phone: store_phone
    };

    var _rating = {
        numerator: 3,
        denominator: 5
    };

    var contactObj = {};
    contactObj[title] = contactDetails;

    obj["rating"] = _rating;
    obj["contact_details"] = contactObj;

    return obj;
}

function updateUser(store_id){
    var obj = {};
    obj[store_id] = Date.now();
    return obj;
}

// Reads the store manifest of a business to determine if the store there still exists.

function doesStoreExist(businessID, storeID, callback){

    firebase.database().ref("businesses/" +businessID+ "/store_manifest/" +storeID).once("value").then(function (snapshot) {

        callback(snapshot.exists());

    });

}

function doesCommentExist (businessID, storeID, reviewID, commentID, callback){

    firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/comments/" +reviewID+ "/" +commentID+ "/age").once("value").then(function(snapshot){
        callback(snapshot.exists());
    });

}

function doesReplyExist (businessID, storeID, reviewID, commentID, replyID, callback){
    firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/replies/" +reviewID+ "/" +commentID+ "/" +replyID+ "/age").once("value").then(function(snapshot){
        callback(snapshot.exists());
    });
}

function doesBusinessExist(businessID, callback){
    firebase.database().ref("businesses/" +businessID+ "/name").once("value").then(function (snapshot) {
        callback(snapshot.exists());
    });
}

function doesOfficialImageExist(businessID, storeID, imageID, callback){
    firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/images/official/" +imageID+ "/created").once("value").then(function (snapshot) {
        callback(snapshot.exists());
    });
}

function doesItemExist(businessID, itemType, itemGroupID, itemID, callback){
    firebase.database().ref("businesses/" +businessID+ "/" +itemType+ "_manifest/" +itemGroupID+ "/" +itemID+"/title").once("value").then(function (snapshot) {
        callback(snapshot.exists());
    });
}

function doesItemGroupExist(businessID, itemType, itemGroupID, callback){
    firebase.database().ref("businesses/" +businessID+ "/" +itemType+ "/" +itemGroupID+ "/title").once("value").then(function (snapshot) {
        callback(snapshot.exists());
    });
}

function doesReviewImageExist(businessID, storeID, imageID, callback){
    firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/images/reviews/" +imageID+ "/created").once("value").then(function (snapshot) {
        callback(snapshot.exists());
    });
}

function doesReviewExist(businessID, storeID, reviewID, callback){

    firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/reviews/" +reviewID+ "/age").once("value").then(function (snapshot) {
       callback(snapshot.exists());
    });

}

function parametersInvalid(obj){
    return (obj.includes(undefined)) || (obj.includes(null));
}

function isEmptyObject(obj){
    return !Object.keys(obj).length;
}

function filterItems(group, promotion){


    var _obj = {};


    if (!isEmptyObject(group) || !isEmptyObject(promotion)) {

        var promotionKeys = Object.keys(promotion);
        promotionKeys.forEach(function (_PromotionGroupKey) {



            var itemKeys = Object.keys(promotion[_PromotionGroupKey]);
            itemKeys.forEach(function (_itemKey) {

                var _temp1 = group[_PromotionGroupKey]["item"][_itemKey];
                var __itemKey = {};
                __itemKey[_itemKey] = _temp1;

                var _temp2 = {
                    "item": __itemKey
                };

                _obj[_PromotionGroupKey] = _temp2;

            });

            _obj[_PromotionGroupKey]["image"] = group[_PromotionGroupKey]["image"];
            _obj[_PromotionGroupKey]["title"] = group[_PromotionGroupKey]["title"];

        });

    }

    return _obj;

}

function buildPromotionObject(promotionObj, obj){

    var _obj = obj;



    var keys = Object.keys(promotionObj);

    for (var i = 0; i < keys.length; i++){

        var itemKeys = Object.keys(promotionObj[keys[i]]);

        if (itemKeys.length > 0){
            _obj[keys[i]]["on_promotion"] = true;
        }

        for(var j = 0; j < itemKeys.length; j++){
            _obj[keys[i]]["item"][itemKeys[j]]["on_promotion"] = true;
        }

    }

    return _obj

}

function getLinkExpiryPeriod(){
    var currentTime = Date.now();
    return currentTime + linkExpiryPeriod;
}

function estimateReviewAge(reviewISOAge){
    var reviewAge = reviewISOAge;
    var currentTime = Date.now();
    var ageDiff = currentTime - reviewAge;

    var hourInMS = 1000 * 60 * 60;
    var dayInMS = 24 * hourInMS;
    var weekInMS = dayInMS * 7;
    var averageMonthInMS = dayInMS * 30.4375;
    var averageYearInMS = dayInMS * 365.25;

    //minutes ago
    if (ageDiff < hourInMS) {
        return Math.round((ageDiff / hourInMS) * 60) + " minutes ago";
        //hours ago
    } else if (ageDiff < dayInMS) {
        return Math.round((ageDiff / dayInMS) * 24) + " hours ago";
        //yesterday
    } else if (ageDiff < dayInMS * 2) {
        return "yesterday";
        //days ago
    } else if (ageDiff < weekInMS) {
        return Math.round((ageDiff / weekInMS) * 7) + " days ago";
        //weeks ago
    } else if (ageDiff < weekInMS * 4) {
        return Math.round((ageDiff / (weekInMS * 4)) * 4) + " weeks ago";
        //1 month ago
    } else if (ageDiff < averageMonthInMS) {
        return "1 month ago";
        // months ago
    } else if (ageDiff < averageYearInMS) {
        return Math.round((ageDiff / averageYearInMS) * 12) + " months ago";
        //years ago
    } else {
        return Math.round((ageDiff / averageYearInMS)) + " years ago";
    }
}

function getDayOfWeek(day) {

    var weekdayDict = {
        0: "sunday",
        1: "monday",
        2: "tuesday",
        3: "wednesday",
        4: "thursday",
        5: "friday",
        6: "saturday"
    };

    return weekdayDict[day];

}

function getItemURLS(businessID, storeID, type, callback) {

    firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/" +type+ "/").once("value").then(function (_storeSnapshot) {
        //check if items exists else return
        if (_storeSnapshot.exists()){

            var _temp = Object.keys(_storeSnapshot.val());
            var groupKeys = [];
            //check for group items having children.
            // if group has children then key value wont be 0
            _temp.forEach(function (element) {
                if (_storeSnapshot.val()[element] !== 0){
                    groupKeys.push(element)
                }
            });

            // if array not empty, get items from business
            if (groupKeys.length !== 0){

                firebase.database().ref("businesses/" +businessID+ "/" +type).once("value").then(function (_businessSnapshot) {
                    if (_businessSnapshot.exists()){

                        var items = {};

                        for (var i = 0; i < groupKeys.length; i++){
                            var url = _businessSnapshot.val()[groupKeys[i]]["image"];
                            var title = _businessSnapshot.val()[groupKeys[i]]["title"];
                            var groupItemData = _businessSnapshot.val()[groupKeys[i]]["items"];
                            var itemKeys = Object.keys(_storeSnapshot.val()[groupKeys[i]]);

                            var _responseObj = {};

                            for (var j = 0; j < itemKeys.length; j++){

                                _responseObj[itemKeys[j]] = {
                                    image: groupItemData[itemKeys[j]]["image"],
                                    title: groupItemData[itemKeys[j]]["title"],
                                    "on_promotion": false,
                                    price: groupItemData[itemKeys[j]]["price"],
                                    description: groupItemData[itemKeys[j]]["description"]
                                } ;

                            }

                            items[groupKeys[i]] = {
                                image: url,
                                title: title,
                                on_promotion: false,
                                "item": _responseObj
                            };
                        }

                        callback(items);

                    } else {

                        callback({});
                        console.log("ERROR: Store and Business items out of sync: BID = " +businessID+ " SID = " +storeID);
                    }
                });

            } else {
                callback({});
            }

        } else {
            callback({});
        }
    });

}

function algoliaAddStore(name, store_id, business_id, location_name, geo_address, lat, long, items, category, callback) {

    algolia_index.addObject({
        business_name: name,
        storeID: store_id,
        businessID: business_id,
        location_name: location_name,
        geo_address: geo_address,
        _geoloc: {
            lat: Number(lat),
            lng: Number(long)
        },
        items: items,
        category: category

    }, store_id, function(error, content){
        if (error){
            print(error);
            callback(error, null);
        } else {
            // algolia_updated = true;
            // sendResponse();
            callback(null, true);
        }
    });



}

// Used when adding a store to a business. This function will search through all the members of a particular business and
// update their business manifest to include the specified storeid. Only members that have the role of member, admin and
// owner will be updated.
function updateMemberManifest(businessID, storeID, callback){

    firebase.database().ref("businesses/" +businessID+ "/members").once("value").then(function (memberSnapshot) {

        if (memberSnapshot.exists()){

            var memberIDS = Object.keys(memberSnapshot.val());
            var count = memberIDS.length;

            function sendCallback(){
                if (count === 0){
                    callback(null, true);
                }
            }

            memberIDS.forEach(function (id) {

                var member = memberSnapshot.val();

                if (member[id] != "store member" || member[id] != "store admin"){
                    firebase.database().ref("users/" +id+ "/businesses/" +businessID).update(function () {

                        var _obj = {};
                        _obj[storeID] = 0;
                        return _obj;

                    }()).then(function () {

                        count--;
                        sendCallback();

                    }, function (error) {
                        callback(error, null);
                    });
                } else {
                    count--;
                    sendCallback();
                }

            });

        } else {
            callback("Internal Error - Member Manifest for Business - " +businessID+ " does not exist", null);
        }

    });
}

// Used when adding a user as either an admin or member.
// Will get all stores of a business and add them to the user -> business manifest
// Will also go to business -> member and add an entry for the user
function addUserToBusinessRole(businessID, userID, userRole, callback) {

    var members_updated = false;
    var user_updated = false;

    function sendCallback() {
        if(members_updated && user_updated){
            callback(null);
        }
    }

    console.log("adding user to business role.... \nVARIABLES: \n"+businessID+ "\n" +userID+ "\n" +userRole);



    firebase.database().ref("businesses/" +businessID+ "/members/").update(function () {
        var obj = {};
        obj[userID] = userRole.toLowerCase();
        return obj;
    }()).then(function () {
        console.log("members updated...");
        members_updated = true;
        sendCallback();
    }, function (error) {
        callback(error);
    });

    //get snapshot of all stores
    firebase.database().ref("businesses/" +businessID+ "/store_manifest" +
        "").once("value").then(function (storeManifestSnapshot) {

        console.log("has snapshot...");
        if (storeManifestSnapshot.exists()){

            var storeIDS = Object.keys(storeManifestSnapshot.val());
            var storeObject = {};

            storeIDS.forEach(function (storeID) {

                storeObject[storeID] = Date.now();

            });

            firebase.database().ref("users/" +userID+ "/businesses/" +businessID+ "/").update(function () {

                return storeObject;

            }()).then(function () {
                user_updated = true;
                sendCallback();
            }, function (error) {
                callback(error);
            });

        } else {
            callback("Internal Error", null);
            console.log("ERROR: BUSINESS AND STORE MANIFEST OUT OF SYNC - BID: " + businessID);
        }

    });
}

// Used when adding a user as either a store admin or store member
// Users that are store personnel only have one storeID in their manifest.
// Users that are business personnel have all storeIDS in their manifest.
// Updates user -> store manifest and store -> member manifest
function addUserToStoreRole(businessID, storeID, userID, userRole, callback) {

    var store_updated = false;
    var user_updated = false;

    function sendCallback() {
        console.log("\n\ntrying to send callback...\n\n");

        if(store_updated && user_updated){
            callback(null);
        }
    }

    console.log(userID);
    console.log(userRole);

// update business -> store -> members
    firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/members/" +
        "").update(function () {

        // console.log("\n\ncreating object1...\n\n");
        var _obj = {};
        _obj[userID] = userRole.toLowerCase();
        return _obj;

    }()).then(function () {

        // console.log("\n\nobject1 created...\n\n");
        store_updated = true;
        sendCallback();

    }, function (error) {
        callback(error);
    });

    //update user -> business
    firebase.database().ref("users/" +userID+ "/businesses/" +businessID+ "" +
        "").update(function () {

        // console.log("\n\ncreating object2...\n\n");

        var obj = {};
        obj[storeID] = Date.now();
        return obj;

    }()).then(function () {

        // console.log("\n\nobject2 created...\n\n");
        user_updated = true;
        sendCallback();

    }, function (error) {
        callback(error);
    });
}

// Used to set a users role
function setUserRole(businessID, userID, userRole, callback){
//update business -> members manifest
    firebase.database().ref("businesses/" +businessID+ "/members/").update(function () {

        var _obj = {};
        _obj[userID] = userRole;
        return _obj;

    }()).then(function () {

        callback(null);

    }, function (error) {
        callback(error);
    });
}

//returns the users name, surname and profile for a given user ID.
// Used in /review/read.
function getUserDetails(userID, callback) {

    var name_downloaded = false;
    var surname_downloaded = false;
    var profile_url_downloaded = false;

    var name;
    var surname;
    var profile_picture;

    function sendCallback() {
        if(name_downloaded && surname_downloaded && profile_url_downloaded){
            callback(name, surname, profile_picture);
        }
    }

//get the users name
    firebase.database().ref("users/" + userID + "/name").once("value").then(function (snapshot) {
        if (snapshot.exists()) {
            name = snapshot.val();
        } else {
            name = "";
        }

        name_downloaded = true;
        sendCallback();

    });

    //get the users surname
    firebase.database().ref("users/" + userID + "/surname").once("value").then(function (snapshot) {
        if (snapshot.exists()) {
            surname = snapshot.val();
        } else {
            surname = "";
        }

        surname_downloaded = true;
        sendCallback();
    });

    //get the users profile
    firebase.database().ref("users/" + userID + "/profile_picture").once("value").then(function (snapshot) {
        if (snapshot.exists()) {
            profile_picture = snapshot.val();
        } else {
            profile_picture = "placeholder.png";
        }

        profile_url_downloaded = true;
        sendCallback();

    });
}

// counts the number of comments for a given review
function countComments(businessID, storeID, review_id, callback) {
    firebase.database().ref("businesses/" + businessID + "/stores/" + storeID + "/comments/" + review_id).once("value").then(function (snapshot) {
        if (snapshot.exists()) {
            callback(snapshot.numChildren());
        } else {
            callback(0);
        }
    });
}

// gets the number of likes for a given path
function likesForPath(url, callback) {
    firebase.database().ref(url).once("value").then(function (snapshot) {

        if (snapshot.exists()){
            callback(snapshot.val());
        } else {
            callback(0);
        }

    });
}

// Updates the specified algolia storeIDS with the given object.
// Will callback null on successful completion
function updateAlgolia(allStoreIDS, algoliaItemObject, callback) {
    var storesToUpdate = allStoreIDS.length;

    function sendResponse() {
        if (storesToUpdate === 0) {
            callback(null);
        }
    }

    allStoreIDS.forEach(function (storeID) {

        algoliaItemObject["objectID"] = storeID;

        algolia_index.partialUpdateObject(algoliaItemObject, function(error, cont){
            if (error){
                console.log(error);
                callback(error)
            } else {
                storesToUpdate -= 1;
                sendResponse();
            }
        });

    });

}

function emailBusinessOwner(businessID, emailType, callback) {

    var hasUserEmail = false;
    var hasBusinessName = false;

    var userEmail;
    var businessName;



    function sendEmail() {
        if (hasUserEmail && hasBusinessName){

            var mail = emailType;
            mail["to"] = userEmail;

            if (mail.subject == "Daily Limit Updated"){

                mail["text"] = "Hi,\n\n" +
                    "The daily advertising limit for " +businessName+" has been updated.\n\n" +
                    "Please note that this update will only take effect within the next 24 hours.\n\n" +
                    "If you did intend to change your daily limit please contact us at info@stackle.co.za.\n\n" +
                    "Thank you for advertising on Stackle.\n\n" +
                    "Sincerely,\n" +
                    "The Stackle Team";

                ejs.renderFile(__dirname +"/views/emails/DailyLimitUpdated.ejs", {businessName: businessName}, function (error, body) {
                    error ? console.log(error) : mail["html"] = body;

                    mailgun.messages().send(mail, function (error, body) {
                        callback(error);
                    });

                });

            } else if (mail.subject == "Deposit Running Low"){

                mail["text"] = "Hi,\n\n" +
                    "The deposit for " +businessName+" has or will soon be running out.\n\n" +
                    "If you wish to continue advertising your business on Stackle please increase your deposit as soon as possible.\n\n" +
                    "Thank you for advertising on Stackle. \n" +
                    "We trust it has been a rewarding experience.\n\n" +
                    "Sincerely\n" +
                    "The Stackle Team";

                ejs.renderFile(__dirname +"/views/emails/DepositRunningLow.ejs", {businessName: businessName}, function (error, body) {
                   error ? console.log(error) : mail["html"] = body;

                    mailgun.messages().send(mail, function (error, body) {
                        callback(error);
                    });

                });

            }

        }
    }

    firebase.database().ref("businesses/" +businessID+ "/name").once("value").then(function (snapshot) {

        if (snapshot.exists()){

            businessName = snapshot.val();
            hasBusinessName = true;
            sendEmail();

        }

    });

    getOwnerEmailForBusinessID(businessID, function(email){

        if (email != null){
            userEmail = email;
            hasUserEmail = true;
            sendEmail();
        } else {
            callback("error retrieving user email from users_lookup/\n" +
                                "description: user email could not be found for the given userID\n\n" +
                                "parameters:\n" +
                                "businessID: " +businessID);
        }

    });

}

function getWeeklyStatisticsForBusinessID(businessID, callback){

    var currentWeek = getWeek();
    currentWeek--;

    if (currentWeek < 0){
        currentWeek = 52;
    }

    // the number of times a businesses bidwords were picked up
    var hasSearches = false;
    // the number of times an particular query led to a click
    var hasClickThrough = false;
    var hasExpenditure = false;
    var hasPositionChanges = 0;
    var _obj = {};

    var callbackData = {};

    function sendCallback(){

        if (hasPositionChanges == 0 && hasClickThrough && hasSearches && hasExpenditure){
            callbackData["position_changes"] = _obj;
            callback(callbackData);
        }
    }

    //get the number of searches
    firebase.database().ref("advertising/" +businessID+ "/adword_searches/" +currentWeek).once("value").then(function(snapshot){

        if (snapshot.exists()){

            var words = Object.keys(snapshot.val());
            words.sort(function(a,b){
                return snapshot.val()[b] - snapshot.val()[a];
            });

            var obj = {};
            var sortedWords = [];
            var totalViews = 0;

            for (var i = 0; i < words.length; i++){

                if (i < 3) {
                    hasPositionChanges++;
                    obj[words[i]] = snapshot.val()[words[i]];
                    sortedWords.push(words[i]);
                }

                totalViews += snapshot.val()[words[i]];

            }

            hasSearches = true;
            callbackData["searches"] = obj;
            callbackData["total_views"] = totalViews;

            //get the before sort value
            sortedWords.forEach(function(word){

                var wordSearches = obj[word];
                var hasBeforeSort = false;
                var hasAfterSort = false;

                var afterSort;
                var beforeSort;

                function getAverageChange(){


                    if (hasAfterSort && hasBeforeSort){
                        var change = afterSort - beforeSort;
                        change = change * -1 / wordSearches;
                        _obj[word] = change;
                        hasPositionChanges--;
                        sendCallback();

                    }
                }

                //get the before sort
                firebase.database().ref("advertising/" +businessID+ "/before_sort/" +currentWeek+ "/" +word).once("value").then(function(snapshot){

                    if (snapshot.exists()){
                        beforeSort = snapshot.val();

                    } else {
                        beforeSort = 0;
                    }

                    hasBeforeSort = true;
                    getAverageChange();

                });

                //get the after sort
                firebase.database().ref("advertising/" +businessID+ "/after_sort/" +currentWeek+ "/" +word).once("value").then(function(snapshot){

                    if (snapshot.exists()){
                        afterSort = snapshot.val();

                    } else {
                        afterSort = 0;
                    }
                    hasAfterSort = true;
                    getAverageChange();

                });

            });


        } else {
            hasSearches = true;
            sendCallback();
        }

    });

    //get the number of clicks
    firebase.database().ref("advertising/" +businessID+ "/click_through/" +currentWeek).once("value").then(function(snapshot){

        if (snapshot.exists()){

            var words = Object.keys(snapshot.val());

            words.sort(function(a,b){
                return snapshot.val()[b] - snapshot.val()[a];
            });

            var obj = {};
            var totalClicks = 0;

            for (var i = 0; i < words.length; i++){

                if (i < 3) {
                    obj[words[i]] = snapshot.val()[words[i]];
                }

                totalClicks += snapshot.val()[words[i]];

            }

            callbackData["clicks"] = obj;
            callbackData["total_clicks"] = totalClicks;
        }

        hasClickThrough = true;
        sendCallback();

    });

    //get the weekly expenditure

    firebase.database().ref("advertising/" +businessID+ "/weekly_expenditure/" +currentWeek).once("value").then(function(snapshot){

        callbackData["weekly_expenditure"] = 0;

        if(snapshot.exists()){
            callbackData["weekly_expenditure"] = snapshot.val();
        }

        hasExpenditure = true;
        sendCallback();

    });
}

function getAdminEmailForBusinessID(businessID, callback){

    var timer = setTimeout(function(){closeSession()}, 60000);
    var adminEmails = null;

    function closeSession(){
        firebase.database().ref("businesses/" +businessID+ "/members").off();
        console.log("session cleared");
        callback(adminEmails);
    }

    function extendSession(){
        clearInterval(timer);
        timer = setTimeout(function(){closeSession()}, 2500);
    }

    firebase.database().ref("businesses/" +businessID+ "/members").orderByValue().equalTo("admin").on("child_added", function(snapshot){

        extendSession();

        if (snapshot.exists()){

            firebase.database().ref("users_lookup/" +snapshot.val()).once("value").then(function(snapshot){

                extendSession();

                if (snapshot.exists()){

                    adminEmails.push(snapshot.val());

                } else {
                    console.log("ERROR: Could not find email address for user ID in business: \nUserID: " + snapshot.val()+ "\nBusinessID: " +businessID);
                }

            });

        }

    });

}

function getOwnerEmailForBusinessID(businessID, callback){
    firebase.database().ref("businesses/" +businessID+ "/members").orderByValue().equalTo("owner").on("child_added", function (snapshot) {

        if (snapshot.exists()){

            var userID = snapshot.key;

            firebase.database().ref("users_lookup/" +userID).once("value").then(function(snapshot){

                if (snapshot.exists()){

                    callback(snapshot.val());

                } else {
                    var error = "error retrieving user email from users_lookup/\n" +
                        "description: user email could not be found for the given userID\n\n" +
                        "parameters:\n" +
                        "userID: " +userID+ "\n" +
                        "businessID: " +businessID;
                    console.log(error);
                }

            });

        } else {
            callback(null)
        }

        firebase.database().ref("businesses/" +businessID+ "/members").off();

    });
}

function emailUser(userID, emailObject, param, callback){

    var userEmail;

    var hasUserEmail = false;

    function sendEmail(){

        if (hasUserEmail){

            var email = emailObject;
            email["to"] = userEmail;

            if (email.subject == "Business Added"){
                email["text"] = "" +
                    "Hi,\n\n" +
                    "" +
                    "This is just to confirm that you have successfully added "+param+" to Stackle.\n\n" +
                    "" +
                    "Now that your business has been added; you can continue setting it up by adding images, products, promotions, a description and more.\n\n" +
                    "" +
                    "To get started with adding these details, navigate to your now available Business Page on the app, slide down and click on 'Manage'.\n" +
                    "From there you will be able to add a variety of information to your page.\n\n" +
                    "" +
                    "The two most important aspects of Stackle are:\n\n" +
                    "" +
                    "Unique Stores:\n" +
                    "You can have many stores. Most importantly each store shares the businesses description, products, promotions, business hours, members and logo.\n" +
                    "Every store has unique reviews, images, ratings, members.\n\n" +
                    "" +
                    "Bidwords and Stats:\n" +
                    "Every week you will be receiving an email breaking down how your store performed: how many views and clicks you received.\n" +
                    "Bidwords place a priority on your stores, it increases your ranking in search results.\n" +
                    "If you have active bidwords you will receive a report on how effective your bidwords were.\n\n" +
                    "" +
                    "If you need any help or additional information please check out: http://stackle.co.za/business-docs \n\n" +
                    "" +
                    "Alternatively if you any unanswered questions please feel free to get in contact with us via facebook, google or:\n" +
                    "email: info@stackle.co.za\n" +
                    "phone: 021 851 2820\n\n" +
                    "" +
                    "Please note that our office hours are 9:00AM to 17:00PM, weekdays only.\n\n" +
                    "" +
                    "We trust you will have a rewarding experience on Stackle.\n\n" +
                    "" +
                    "Sincerely,\n" +
                    "The Stackle Team";
            }

            ejs.renderFile(__dirname +"/views/emails/BusinessAdded.ejs", {businessName: param}, function (error, body) {

                error ? console.log(error) : email["html"] = body;

                mailgun.messages().send(email, function(error, body){

                    if(error){
                        callback(error);
                    } else {
                        callback(null);
                    }

                });

            });

        }

    }

    firebase.database().ref("users_lookup/" +userID).once("value").then(function(snapshot){
        if (snapshot.exists()){
            userEmail = snapshot.val();
            hasUserEmail = true;
            sendEmail();
        }

        callback("User Email Address could not be found");
    })

}

// url needs to path to the object holding likes but not the likes object itself.
function setLikesForPath(url, amount, callback) {

    firebase.database().ref(url).update({
        likes: amount,
        likes_neg: (amount * -1)
    }).then(function () {

        callback(null);

    }, function (error) {

        callback(error);

    });

}

function removeUserIDFromRegistryWithPath(url, callback) {

    firebase.database().ref(url).remove().then(function () {

        callback(null);

    }, function (error) {
        console.log(error);
        callback(error);
    });

}

function addUserIDToRegistryWithPath(url, userID, callback) {

    firebase.database().ref(url).update(function(){
        var obj = {};
        obj[userID] = Date.now();
        return obj;
    }()).then(function () {
        callback(null);
    }, function (error) {
        callback(error);
    });

}

// Drops a notification into a users basket. Used when a user is interacting
// with another user.
function notifyUser(userID_ReceivingNotification, userID_GivingNotification, businessName, businessID, storeID, notificationID, userName, userSurname, type, like, comment, callback) {

    // console.log("notifying user...");

    // Prevents a user from receiving a notification from themself and from a business receiving a notification
    if (userID_GivingNotification != userID_ReceivingNotification && userID_ReceivingNotification != businessID){
        var currentTime = Date.now();

        // console.log("building object");

        // Type is an object built by the calling function
        var obj = type;
        obj["business_name"] = businessName;
        obj["business_id"] = businessID;
        obj["name"] = userName.capitalize();
        obj["surname"] = userSurname.capitalize();
        obj["user_id"] = userID_GivingNotification;
        obj["age"] = currentTime;
        obj["likes"] = like;
        obj["comment"] = comment;

        // console.log("updating firebase...");

        firebase.database().ref("users/" +userID_ReceivingNotification+ "/notifications/" +storeID+ "_" +notificationID).update(obj).then(function () {
            callback(null);
        }, function (error) {
            callback(error);
        });

        // console.log("preparing to send notification...");

        if (!obj["type"].includes("promotion")){

            // console.log("sending push notification");

            var commentCount = 0;
            if (obj["comment_count"] != undefined && Number.isInteger(obj["comment_count"])) {
                commentCount = obj["comment_count"];
            }

            var likeCount = 0;
            if (like != undefined && Number.isInteger(like)) {
                likeCount = like;
            }

            sendPushNotification(userID_ReceivingNotification, userName.capitalize(), userSurname.capitalize(), likeCount, commentCount, type["type"], function(error){

                if (error != null){
                  console.log(error);
                } else {
                    callback(null);
                }

            })
        } else {
            callback(null);
        }

    } else {
        callback(null);
    }


}

function sendPushNotification(userID, name, surname, likes, comments, type, callback){

    var iosNotificationComplete = false;

    function sendCallback(){
        if (iosNotificationComplete){
            callback(null);
        }
    }

    // read the amount of notifications
    firebase.database().ref("users/" +userID+ "/push_notifications").once("value").then(function(snapshot){

        // console.log("firebase read...");
        if (snapshot.exists()){
            var count = 1;
            var _type = snapshot.val()["type"];

            // console.log(_type);

            if (snapshot.exists() && snapshot.val()["badge_count"] != undefined){
                count += snapshot.val()["badge_count"];
            }

            var _devices = snapshot.val()["devices"];
            var iOSDevices = Object.keys(_devices["ios"]);

            if (iOSDevices != undefined || iOSDevices.length != 0){

                var notification = new apn.Notification();
                notification.badge = count;
                notification.expiry = 0;
                notification.priority = 10;
                notification.topic = "com.stackleptyltd.Stackle";

                // console.log("notification packaged");

                if (type === "new_comment" && (_type === undefined || _type["new_comment"] == undefined || _type["new_comment"] == false)){

                    if (comments == 0){
                        notification.alert = name +" " +surname+ " has commented on your review.";
                    } else {
                        notification.alert = name + " " +surname+ " and " +comments+ " others have commented on your review.";
                    }
                } else if (type === "comment_liked" && (_type === undefined || _type["comment_liked"] == undefined || _type["comment_liked"] == false)){
                    if (likes == 0){
                        notification.alert = name +" " +surname+ " has liked your comment.";
                    } else {
                        notification.alert = name + " " +surname+ " and " +likes+ " have liked your comment.";
                    }
                } else if (type === "image_liked" && (_type === (undefined) || _type["image_liked"] == undefined || _type["image_liked"] == false)){
                    if (likes == 0){
                        notification.alert = name +" " +surname+ " has liked your photo.";
                    } else {
                        notification.alert = name + " " +surname+ " and " +likes+ " others have liked your photo.";
                    }
                } else if (type === "review_liked" && (_type === (undefined) || _type["review_liked"] == undefined || _type["review_liked"] == false)){
                    if (likes == 0){
                        notification.alert = name +" " +surname+ " has liked your review.";
                    } else {
                        notification.alert = name + " " +surname+ " and " +likes+ " others have liked your review.";
                    }
                } else if (type === "reply_comment" && (_type === (undefined) || _type["reply_comment"] == undefined || _type["reply_comment"] == false)){
                    notification.alert = name + " " +surname+ " has replied to you.";
                }

                firebase.database().ref("users/"+userID+"/push_notifications/").update(function(){

                    var obj = {};
                    if (_type !== undefined && _type !== "" ){
                        obj = _type;
                    }

                    obj[type] = true;

                    // console.log(obj);

                    var update = {
                        badge_count: count,
                        type: obj
                    };

                    return update;

                }()).then(function (){

                    // console.log("preparing to send notification");
                    // console.log("ios devices:");
                    // console.log(iOSDevices);

                    provider.send(notification, iOSDevices).then(function(response){

                        if (response["failed"].length != 0) {
                            console.log(response);
                        }

                    });

                    iosNotificationComplete = true;
                    sendCallback();

                }, function(error){
                    callback(error);
                });
            }
        } else {
            iosNotificationComplete = true;
            sendCallback();
        }
    });

}

function userMayAddBusiness(userID, callback){

    firebase.database().ref("users/" +userID+ "/businesses").once("value").then(function(snapshot){

        callback(!snapshot.exists());

    });

}

// Used when a store wants to send out a notification to a group of users.
// Typical usage will be for sending out promotions.
function notifyAllUsers(businessID, type, callback){

    var name;
    var storeIDS;
    var currentTime = Date.now();

    var has_name = false;
    var has_storeIDS = false;


    function sendNotification() {
        if (has_name && has_storeIDS){

            // console.log("sending notification...");

            var storeCount = storeIDS.length;

            // console.log("STORE COUNT... " +storeCount);

            function sendCallback() {
                // console.log("STORE COUNT... " +storeCount);

                if (storeCount <= 0){
                    // console.log("callback sent...");
                    callback(null);
                }
            }

            // get all the user ids that have liked each store.
            storeIDS.forEach(function (storeID) {

                // console.log("sending notification for: " +storeID + "...");

                firebase.database().ref("likes_registry/businesses/" +businessID+ "/stores/" +storeID+ "/likes").once("value").then(function (snapshot) {

                    if (snapshot.exists()){

                        var userIDS = Object.keys(snapshot.val());
                        // console.log(userIDS);

                        // console.log(type);

                        var obj = type;

                        // console.log(obj);

                        obj["age"] = currentTime;
                        obj["business_name"] = name;
                        obj["business_id"] = businessID;
                        obj["store_id"] = storeID;
                        obj["key"] = internalKey;
                        obj["user_ids"] = userIDS;

                        var options = {
                            method: "post",
                            body: obj,
                            json: true
                        };

                        // console.log("\nOBJECT\n" + obj);
                        // console.log(obj["store_id"]);

                        performRequest("https://stackle-live.appspot.com/notifyAllUsers", options, function (error, response, body) {
                            if(error != null){
                                console.log(error);
                            }
                        });

                        // performRequest.post("https://stackle-1278.appspot.com/notifyAllUsers", { json: {
                        //
                        //     "user_ids": userIDS,
                        //     "business_id": businessID,
                        //     "business_name": business_name,
                        //     "type": type,
                        //     "store_id": storeID,
                        //     "key": internalKey
                        //
                        // }});

                    }

                    storeCount -= 1;
                    // console.log("NEW STORE COUNT: " +storeCount);
                    sendCallback();

                });

            });

        }
    }

    //get all store IDS

    firebase.database().ref("businesses/" +businessID+ "/store_manifest").once("value").then(function (snapshot) {

        if (snapshot.exists){
            storeIDS = Object.keys(snapshot.val());
            has_storeIDS = true;
            sendNotification();
        } else {
            callback("Store snapshot does not exists");
        }
    });

    // Get the businesses name
    firebase.database().ref("businesses/" +businessID+ "/name/").once("value").then(function (snapshot) {

        if (snapshot.exists()) {

            name = snapshot.val();
            has_name = true;
            sendNotification();

        } else {
            callback("name not found");
        }

    });

}

// Used in creating, updating and removing promotions. Sets the given itemGroup to either on or off promotion.
function updateGroup(businessID, obj, type, onPromotion, callback) {

    if (obj === undefined){

        callback(null);

    } else {
        console.log("updating group... " + type);
        var objectKeys = Object.keys(obj);
        var groupsToUpdate = objectKeys.length;
        sendCallback();

        function sendCallback() {
            if (groupsToUpdate == 0) {
                callback(null);
            }
        }

        if (groupsToUpdate !== 0) {
            console.log("found data for update... updating " + type);

            Object.keys(obj).forEach(function (groupID) {

                var groupSet;
                var itemsToSet = obj[groupID].length;


                function updateComplete() {
                    if (groupSet && itemsToSet == 0) {
                        groupsToUpdate -= 1;
                        sendCallback();
                    }
                }

                // update the group
                firebase.database().ref("businesses/" + businessID + "/" + type + "/" + groupID).update({
                    on_promotion: onPromotion
                }).then(function () {

                    groupSet = true;
                    updateComplete();

                }, function (error) {
                    console.log(error);
                    callback(error);
                });

                // update all the items for this group
                for (var i = 0; i < obj[groupID].length; i++) {
                    firebase.database().ref("businesses/" + businessID + "/" + type + "_manifest/" + groupID + "/" +
                        obj[groupID][i]).update({

                        on_promotion: onPromotion

                    }).then(function () {

                        itemsToSet -= 1;
                        updateComplete();

                    }, function (error) {
                        console.log(error);
                        callback(error);
                    });
                }

            });

        }
    }
}

//Note this should not be used if accuracy is highly important. This function only estimates the current week in a
// yearly cycle and does not account for the 1 / 7 or 2 / 7 days that are missing every year. Instead it simply treats
// these days as a part of week 52. For high accuracy an alternative function would need to be made where DLST and other
// descrepancies factored in and these odd days are carried for every other year.
function getWeek(){

    var currentTime = Date.now();
    var currentYear = new Date().getUTCFullYear();
    // var MSInDay = 1000 * 60 * 60 * 24;
    var MSInDay = 86400000;
    var yearOffsets = Math.floor((currentYear - 1970) / 4);
    var MSInCurrentYear = currentTime - ((currentYear - 1970) * MSInDay * 365 + (yearOffsets * MSInDay));
    var currentWeek = Math.floor(MSInCurrentYear / (MSInDay * 7));

    return currentWeek;

}

function sumOfArray(array){
    var _val = 0;

    array.forEach(function (val) {
       _val += val;
    });

    return _val;
}

function averageOfArray(array){
    return sumOfArray(array) / array.length;
}


/**
 *
 *  Gets the average of averages for two equally sized arrays.
 *
*/

function averageOfArrays(numeratorArray, denominatorArray){

    var average = 0;
    var divisor = 0;

    if (!Array.isArray(numeratorArray) || !Array.isArray(denominatorArray) || numeratorArray.length != denominatorArray.length){
        return 0;
    } else {

        for (var i = 0; i < numeratorArray.length; i++){
            if (denominatorArray[i] != 0){
                average += numeratorArray[i] / denominatorArray[i];
                divisor += 1;
            }

        }

        if (divisor != 0){
            return average / divisor;
        } else {
            return -1;
        }

    }

}

function buildEmail_DepositReceived(businessID, userID, callback){
    var hasBusinessName = false;
    var hasUserEmail = false;

    var businessName;
    var userEmail;

    function sendEmail(){
        if (hasBusinessName && hasUserEmail){

            var email = EmailTemplate_DepositReceived;
            email["to"] = userEmail;

            email["text"] = "" +
                "Hi,\n\n" +
                "" +
                "This email is just to confirm that "+businessName+" has been successfully credited.\n\n" +
                "Please note that it can take up to 24 hours for any deactivated bidwords to be reactivated.\n\n" +
                "" +
                "Sincerely,\n" +
                "" +
                "The Stackle Team";

            ejs.renderFile(__dirname + "/views/emails/DepositReceived.ejs", {businessName: businessName}, function (error, body) {
                if (error){
                    console.log(error);
                } else {
                    email["html"] = body;
                }

                callback(email);

            });

            // callback(email);
        }
    }

    // get the businesses name
    firebase.database().ref("businesses/" +businessID+ "/name").once("value").then(function(snapshot){

        // console.log("reading business name");

        if(snapshot.exists()){
            hasBusinessName = true;
            businessName = snapshot.val();
            sendEmail();
        }

    });

    // get the users email address
    firebase.database().ref("users_lookup/" +userID).once("value").then(function(snapshot){

        if (snapshot.exists()){
            userEmail = snapshot.val();
            hasUserEmail = true;
            sendEmail();
        }

    });
}

function buildEmail_StoreAdded(userID, storeGeo, storeLoc, businessName, callback){

    firebase.database().ref("users_lookup/" +userID).once("value").then(function(snapshot){
        if(snapshot.exists()){
            var email = EmailTemplate_StoreAdded;
            email["to"] = snapshot.val();

            email["text"] = "" +
                "Hi,\n\n" +
                "" +
                "A new store has been added to " +businessName+ " in " +storeLoc+ ", " +storeGeo+ ".\n\n" +
                "" +
                "If you did not add or authorise anyone to add this store please contact us immediately at:\n" +
                "" +
                "Email: info@stackle.co.za\n" +
                "" +
                "Phone: 021 851 2820\n\n" +
                "" +
                "Sincerely,\n" +
                "" +
                "Stackle Pty Ltd";

            ejs.renderFile(__dirname + "/views/emails/StoreAdded.ejs", {businessName: businessName, storeLoc: storeLoc, storeGeo: storeGeo}, function (error, body) {

                error ? console.log(error) : email["html"] = body;

                callback(email);
            });

        }
    });


}

function userNameForUserID(userID, callback){
    firebase.database().ref("users/" +userID+ "/details/name").once("value").then(function (snapshot) {
        if (snapshot.exists()){

            callback(snapshot.val());

        } else {
            callback(null);
        }
    });
}

function userSurnameForUserID(userID, callback){
    firebase.database().ref("users/" +userID+ "/details/surname").once("value").then(function (snapshot) {
        if (snapshot.exists()){

            callback(snapshot.val());

        } else {
            callback(null);
        }
    });
}

function storeLocationForIDS(businessID, storeID, callback){
    firebase.database().ref("businesses/" +businessID+ "/store_manifest/"+storeID).once("value").then(function (snapshot) {
        if (snapshot.exists()){
            callback(snapshot.val());
        } else {
            callback(null);
        }
    })
}

function businessNameForBusinessID(businessID, callback){
    firebase.database().ref("businesses/" +businessID+ "/name").once("value").then(function (snapshot) {
        if (snapshot.exists()){
            callback(snapshot.val());
        } else {
            callback(null);
        }
    });
}

function removeBidwordsLessThanValue(businessID, maxValue, callback){

    var hasStoreIDS = false;
    var hasBidwords = false;

    var storeIDS;
    var bidwords;

    function calculateBidWords() {
        if (hasStoreIDS && hasBidwords) {

            var bidwordKeys = Object.keys(bidwords);

            var offset = 0;
            var loopCount = bidwordKeys.length;

            for (var i = 0; i < loopCount; i++){

                if (bidwords[bidwordKeys[i - offset]] >= maxValue){
                    bidwordKeys.splice(i - offset, 1);
                    offset++;
                }

            }

            var obj = {
                "adwords": bidwordKeys
            };

            updateAlgolia(storeIDS, obj, function (error) {

                if (error != null){
                    console.log(error);
                    callback(error);
                } else {
                    callback(null);
                }

            });

        }
    }

    //allbidwords

    firebase.database().ref("advertising/" +businessID+ "/adwords").once("value").then(function (snapshot) {

        if (snapshot.exists()){

            bidwords = snapshot.val();
            hasBidwords = true;
            calculateBidWords();

        } else {
            callback(null);
        }

    });

    //storeIDS
    firebase.database().ref("businesses/" +businessID+ "/store_manifest").once("value").then(function (snapshot) {

        if (snapshot.exists()){

            storeIDS = Object.keys(snapshot.val());
            hasStoreIDS = true;
            calculateBidWords();

        } else {
            callback("error");
        }

    });

}

/**
 * The delete functions are only used to delete "local" orphan data
 *
 * These functions are called by the daily tasks post.
 *
 * Any orphan data should be deleted very quickly.
 *
 *  **Orphan data is data that has been written somewhere but there is no supporting data or record of it being written.
 *
 *  Most of these cases should however be caught by the doesExist functions and this is mostly a precaution
*/

function deleteReply(businessID, storeID, reviewID, commentID, replyID, id, callback){

    var likesRemoved = false;
    var replyRemoved = false;

    function sendResponse(){
        if (likesRemoved && replyRemoved){
            callback(null, id);
        }
    }

    //REMOVE LIKES:
    firebase.database().ref("likes_registry/businesses/" +businessID+ "/stores/" +storeID+ "/reviews/" +reviewID+ "/comments/" +commentID+ "/replies/" +replyID).remove().then(function () {

        likesRemoved = true;
        sendResponse();

    }, function (error) {
        console.log(error);
        callback(error);
    });

    //REMOVE REPLY:
    firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/replies/" +reviewID+ "/" +commentID+ "/" +replyID).remove().then(function () {

        replyRemoved = true;
        sendResponse();

    }, function (error) {
        console.log(error);
        callback(error);
    });
}

function deleteComment(businessID, storeID, reviewID, commentID, id, callback){

    /**
     * the id parameters is only used for permanent delete.
     * other calls should just enter a placeholder such as ""
     */

    var likes_removed = false;
    var replies_removed = false;
    var comment_removed = false;

    function sendResponse(){
        if (likes_removed && replies_removed && comment_removed){
            callback(null, id);
        }
    }

    // delete likes
    firebase.database().ref("likes_registry/businesses/" +businessID+ "/stores/" +storeID+ "/reviews/" +reviewID+ "/comments/" +commentID).remove().then(function () {
        likes_removed = true;
        sendResponse();
    }, function (error) {
        console.log(error);
        callback(error);
    });

    //delete replies
    firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/replies/" +reviewID+ "/"+commentID).remove().then(function () {
        replies_removed = true;
        sendResponse();
    }, function (error) {
        console.log(error);
        callback(error);
    });

    // remove comment
    firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/comments/" +reviewID+ "/" +commentID).remove().then(function () {
        comment_removed = true;
        sendResponse();
    }, function (error) {
        console.log(error);
        callback(error);
    });

}

// does not update rating or the review count - used by daily tasks delete
function deleteOrphanReview(businessID, storeID, userID, imageIDS, id, callback){

    var review_removed = false;
    var comments_removed = false;
    var likes_removed = false;
    var replies_removed = false;
    var count_images_to_remove = imageIDS == undefined ? 0 : imageIDS.length;

    function sendResponse(){
        if (review_removed && comments_removed && likes_removed && replies_removed && count_images_to_remove == 0) {
            callback(null, id);
        }
    }

    // remove review using userID
    firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/reviews/" +userID).remove().then(function () {

        review_removed = true;
        sendResponse();

    }, function (error) {
        console.log(error);
        callback(error);
    });

    // remove comments using userID
    firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/comments/" +userID).remove().then(function () {

        comments_removed = true;
        sendResponse();

    }, function (error) {
        console.log(error);
        callback(error);
    });

    //REMOVE LIKES:
    firebase.database().ref("likes_registry/businesses/" +businessID+ "/stores/" +storeID+ "/reviews/" +userID).remove().then(function(){

        likes_removed = true;
        sendResponse();

    }, function(error){
        console.log(error);
        callback(error);
    });

    //REMOVE REPLIES:
    firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/replies/" +userID).remove().then(function () {

        replies_removed = true;
        sendResponse();

    }, function (error) {

        console.log(error);
        callback(error);

    });

    //remove images using provided images

    if (imageIDS != undefined){
        imageIDS.forEach(function (_image_id) {
            firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/images/reviews/" +_image_id).remove().then(function () {

                count_images_to_remove--;
                sendResponse();

            }, function (error) {
                console.log(error);
                callback(error);
            });
        });
    }


}

// deletes the review AND updates remove_image and the rating. Does not update the review count;
function deleteReview(businessID, storeID, userID, imageIDS, callback){

    var image_remove_requested = false;
    var rating_updated = false;
    var comments_removed = false;
    var likes_removed = false;
    var replies_removed = false;
    var review_removed = false;
    var review_images_counted = false;
    var count_updated = false;

    var count_images_to_remove = imageIDS == undefined ? 0 : imageIDS.length;

    function sendResponse() {
        if (image_remove_requested && rating_updated && comments_removed && likes_removed && replies_removed &&
            review_images_counted && review_removed && count_images_to_remove == 0 && count_updated){
            callback(null);
        }
    }

    //update count
    firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/review_count").once("value").then(function (snapshot) {

        var count = 0;
        if(snapshot.exists()){
            count = snapshot.val() - 1;
        }

        if (count < 0){
            count = 0;
        }

        firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/").update({
            "review_count": count
        }).then(function () {

            count_updated = true;
            sendResponse();

        }, function (error) {
            console.log(error);
            callback(error);
        });

    });

    //request image removal: NODE JS cannot directly remove an image. Needs to be done via the admin panel.
    firebase.database().ref("remove_images/").update(function(){

        var obj = {};

        var imagesToRemove = imageIDS == undefined ? 0 : imageIDS.length;

        for (var i = 0; i < imagesToRemove; i++){
            obj[imageIDS[i]+""+Date.now()] = "businesses/" +businessID+ "/stores/" +storeID+ "/reviews/" +imageIDS[i]+ ".jpg"
        }

        return obj;

    }()).then(function(){
        image_remove_requested = true;
        sendResponse();
    }, function(error){
        console.log(error);
        callback(error);
    });

    firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/reviews/" +userID+ "/rating").once("value").then(function (snapshot) {

        if(snapshot.exists()){
            var rating = snapshot.val();

            //update rating
            firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/rating/").once("value").then(function (snapshot) {
                if (snapshot.exists()){

                    var num = snapshot.val()["numerator"] - rating;
                    var denom = snapshot.val().denominator - 5;

                    if (denom < 5 || num < 0){
                        denom = 5;
                        num = 3;
                    }

                    firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/rating").update({
                        numerator: num,
                        denominator: denom
                    }).then(function () {

                        rating_updated = true;
                        sendResponse();

                    }, function (error) {
                        console.log(error);
                        callback(error);
                    });

                } else {

                    console.log("ERROR: store rating does not exist. BID: " + businessID+ " SID: " +storeID);
                    callback(error);
                }

            });

            // check if this review had any images; if so how many
            firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/reviews/" +userID+ "/image").once("value").then(function (snapshot) {

                if (snapshot.exists()){
                    var count = snapshot.val().length;

                    //count the number of review images there currently are
                    firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/images_review").once("value").then(function (snapshot) {
                        var newCount = 0;

                        if (snapshot.exists()){
                            newCount = snapshot.val() - count;

                            if (newCount < 0 ){
                                newCount = 0;
                            }

                        }

                        firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/").update(function () {

                            var obj = {};
                            obj["images_review"] = newCount;
                            return obj;

                        }()).then(function () {

                            review_images_counted = true;
                            sendResponse();

                        }, function (error) {
                            console.log(error);
                            callback(error);
                        });

                        //remove images using provided images

                        imageIDS.forEach(function (_image_id) {
                            firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/images/reviews/" +_image_id).remove().then(function () {

                                count_images_to_remove--;
                                sendResponse();

                            }, function (error) {
                                console.log(error);
                                callback(error);
                            });
                        });

                    });

                } else {
                    review_images_counted = true;
                    sendResponse();
                }

                // remove review using userID
                firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/reviews/" +userID).remove().then(function () {

                    review_removed = true;
                    sendResponse();

                }, function (error) {
                    console.log(error);
                    callback(error);
                });

            });

        } else {

            console.log("ERROR: review rating does not exist. BID: " + businessID+ " SID: " +storeID);
            callback(error);
        }

    });

    // remove comments using userID
    firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/comments/" +userID).remove().then(function () {

        comments_removed = true;
        sendResponse();

    }, function (error) {
        console.log(error);
        callback(error);
    });

    //REMOVE LIKES:
    firebase.database().ref("likes_registry/businesses/" +businessID+ "/stores/" +storeID+ "/reviews/" +userID).remove().then(function(){

        likes_removed = true;
        sendResponse();

    }, function(error){
        console.log(error);
        callback(error);
    });

    //REMOVE REPLIES:
    firebase.database().ref("businesses/" +businessID+ "/stores/" +storeID+ "/replies/" +userID).remove().then(function () {

        replies_removed = true;
        sendResponse();

    }, function (error) {

        console.log(error);
        callback(error);

    });

}

function deleteImage(business_id, store_id, type, image_id, id, callback){

    var manifest_updated = false;
    var likes_removed = false;

    function sendResponse() {
        if (manifest_updated && likes_removed){
            callback(null, id);
        }
    }

    //update manifest
    firebase.database().ref("businesses/" + business_id + "/stores/" + store_id + "/images/" +type+ "/" + image_id).remove().then(function () {

        manifest_updated = true;
        sendResponse();

    }, function (error) {
        console.log(error);
        callback(error);
    });

    //remove likes
    firebase.database().ref("likes_registry/businesses/" +business_id+ "/stores/" +store_id+ "/images/" +type+ "/" +image_id).remove().then(function () {

        likes_removed = true;
        sendResponse();

    }, function (error) {
        console.log(error);
        callback(error);
    });
}

function deleteStore(business_id, store_id, id, callback){

    var businessMembersToUpdate = 0;
    var count_users_updated = 0;
    var likes_registry_updated = false;
    var manifest_updated = false;
    var is_removed = false;

    function sendResponse() {

        if (likes_registry_updated && manifest_updated && is_removed && businessMembersToUpdate == 0 && count_users_updated == 0){
            callback(null, id);
        }

    }

    // get all the business members and remove the store from their reference
    firebase.database().ref("businesses/" +business_id+ "/members").once("value").then(function (snapshot) {

        if (snapshot.exists()){

            var memberIDS = Object.keys(snapshot.val());
            businessMembersToUpdate = memberIDS.length;

            memberIDS.forEach(function (memberID) {

                firebase.database().ref("users/" +memberID+ "/businesses/" +business_id+ "/" +store_id).remove().then(function () {
                    businessMembersToUpdate -- ;
                    sendResponse();
                });

            });

        } else {
            callback(error);
        }

    });

    // Get all members for that particular store and remove them
    firebase.database().ref("businesses/" +business_id+ "/stores/" +store_id+ "/members").once("value").then(function (snapshot) {

        if (snapshot.exists()){

            var member_ids = Object.keys(snapshot.val());
            count_users_updated = member_ids.length;

            //cycle through all the members and update. Go to the user -> business manifest and remove that storeID
            member_ids.forEach(function (member_id) {
                //updates the user -> business manifest
                firebase.database().ref("users/" + member_id + "/businesses/" + business_id + "/" +
                    "" + store_id).remove().then(function () {

                    count_users_updated--;
                    sendResponse();

                }, function (error) {
                    console.log(error);
                    callback(error);

                });

            });

        }

        // remove the store
        firebase.database().ref("businesses/" + business_id + "/stores/" + store_id).remove().then(function () {
            is_removed = true;
            sendResponse();
        }, function (error) {
            console.log(error);
            callback(error);
        });

        //update the business manifest
        firebase.database().ref("businesses/" + business_id + "/store_manifest/" + store_id).remove().then(function () {

            manifest_updated = true;
            sendResponse();

        }, function (error) {
            console.log(error);
            callback(error);
        });

        // update likes registry
        firebase.database().ref("likes_registry/businesses/" +business_id+ "/stores/" +store_id).remove().then(function () {

            likes_registry_updated = true;
            sendResponse();

        }, function (error) {

            console.log(error);
            callback(error);

        });

    });
}

function deleteBusiness(business_id, id, callback){

    var businessMembers = -1;
    var confirmation_link = false;
    var businessBudget = false;
    var businessAdvertising = false;
    var likesDeleted = false;
    var businessObject = false;

    function sendResponse () {
        if (businessMembers == 0 && confirmation_link && businessBudget && businessAdvertising && likesDeleted && businessObject){
            callback(null, id);
        }
    }

    firebase.database().ref("businesses/" +business_id+ "/members").once("value").then(function (snapshot) {

        if (snapshot.exists()){

            var memberIDS = Object.keys(snapshot.val());
            businessMembers = memberIDS.length;

            memberIDS.forEach(function (memberID) {

                firebase.database().ref("users/" +memberID+ "/businesses/" +business_id).remove().then(function(){
                    businessMembers--;
                    sendResponse();
                });

            });

        } else {
            businessMembers = 0;
            sendResponse();
        }

        // remove confirmation links
        firebase.database().ref("confirmation_links/remove_business/" +business_id).remove().then(function () {
            confirmation_link = true;
            sendResponse();
        });

        // remove daily budget
        firebase.database().ref("daily_budget/" +business_id).remove().then(function () {
            businessBudget = true;
            sendResponse();
        });

        // remove advertising stats
        firebase.database().ref("advertising/" +business_id).remove().then(function () {
            businessAdvertising = true;
            sendResponse();
        });

        // remove all likes
        firebase.database().ref("likes_registry/businesses/" +business_id).remove().then(function () {
            likesDeleted = true;
            sendResponse();
        });

        //remove the business object
        firebase.database().ref("businesses/" +business_id).remove().then(function () {
            businessObject = true;
            sendResponse();
        });

        firebase.database().ref("unowned_businesses/" +business_id).remove();

    });


}

function deleteItemGroup(business_id, type, item_group_id, callback){

    var manifest_updated = false;
    var group_updated = false;

    function sendResponse() {
        if (manifest_updated && group_updated){
            callback(null);
        }
    }

    // update manifest
    firebase.database().ref("businesses/" +business_id+ "/" +type+ "_manifest/" +item_group_id).remove().then(function () {

        manifest_updated = true;
        sendResponse();

    }, function (error) {
        console.log(error);
        callback(error);
    });

    // update group
    firebase.database().ref("businesses/" +business_id+ "/" +type+ "/" +item_group_id).remove().then(function () {

        group_updated = true;
        sendResponse();

    }, function (error) {
        console.log(error);
        callback(error);
    });

}

function deleteItem(business_id, type, item_group_id, item_id, callback){
    // remove from item manifest
    firebase.database().ref("businesses/" +business_id+ "/" +type+ "_manifest/" +item_group_id+ "/" +item_id).remove().then(function () {
        manifest_updated = true;
        callback(null);
    }, function (error) {
        console.log(error);
        callback(error);
    });
}

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

// MARK: Port

app.listen(process.env.PORT || 8080, function(){
    console.log("server initialized...");
});

// MARK: Test Code

// app.post("/testStoreHours", function (request, response) {
//
//     var has_offset = false;
//     var has_holiday_hours = false;
//     var has_standard_hours = false;
//
//     var utc_offset = 0;
//     var holiday_hours = {};
//     var standard_hours = {};
//     var timeTable = {};
//
//
//     // build timetable for that week
//     function buildTimeTable() {
//         if(has_offset && has_holiday_hours && has_standard_hours){
//
//             //  -   Get the interval at that stores location
//             var todayMinutes = new Date().getUTCMinutes();
//             var todayHours = Number(new Date().getUTCHours()) + Number(utc_offset);
//             // Now get the date at that store using the offset hours
//             var todayDate = getDayAtStore(todayHours);
//             var todayMonth = new Date().getUTCMonth();
//             var todayYear = new Date().getUTCFullYear();
//             // Lastly get the interval for that stores location
//             var todayInterval = new Date(Date.UTC(todayYear, todayMonth, todayDate, todayHours, todayMinutes));
//
//             // console.log(todayMinutes + " Minutes");
//             // console.log(todayHours + " Hours");
//             // console.log(todayDate + " Date");
//             // console.log(todayMonth + " Month");
//             // console.log(todayYear + " Year");
//             // console.log(todayInterval.valueOf() + " SUM");
//             // Given the holiday hours we have.
//
//             // Are we inside any of the holiday intervals?
//             Object.keys(holiday_hours).forEach(function (holidayKey) {
//
//                 // Get all the start components
//                 var startDateComponents = holiday_hours[holidayKey]["start_date"].split(" ");
//                 var startTimeComponents = holiday_hours[holidayKey]["open_hour"];
//
//                 var startYear = startDateComponents[2];
//                 var startMonth = getMonthIntegerForString(startDateComponents[0]);
//                 var startDate = startDateComponents[1].split(",")[0];
//                 var startHour = getHoursFromString(startTimeComponents);
//                 var startMinutes = getMinutesFromString(startTimeComponents);
//
//                 // Get all the end components
//                 var endDateComponents = holiday_hours[holidayKey]["end_date"].split(" ");
//                 var endTimeComponents = holiday_hours[holidayKey]["close_hour"];
//
//                 var endYear = endDateComponents[2];
//                 var endMonth = getMonthIntegerForString(endDateComponents[0]);
//                 var endDate = endDateComponents[1].split(",")[0];
//                 var endHour = getHoursFromString(endTimeComponents);
//                 var endMinutes = getMinutesFromString(endTimeComponents);
//
//                 // console.log("YEAR: " + endYear);
//                 // console.log("Month: " + endMonth);
//                 // console.log("Date: " + endDate);
//                 // console.log("Hour: " + endHour);
//                 // console.log("Minutes: " + endMinutes);
//
//                 // Build up each interval
//                 var startInterval = new Date(Date.UTC(startYear, startMonth, startDate, startHour, startMinutes, 0, 0));
//                 var endInterval = new Date(Date.UTC(endYear, endMonth, endDate, endHour, endMinutes, 0, 0));
//
//                 var todayIntervalValue = todayInterval;
//
//                 for (var i = 0; i < 8; i++){
//
//                     todayIntervalValue = todayIntervalValue.valueOf() + (i * 24 * 60 * 60 * 1000);
//
//                     // Determine if we are currently inside this interval or not.
//                     if (todayIntervalValue >= startInterval.valueOf() && todayIntervalValue <= endInterval.valueOf()){
//
//                         var dayInt = todayInterval.getUTCDay() + i;
//
//                         if (dayInt >= 7){
//                             dayInt -= 7;
//                         }
//
//                         var todayText = getDayTextFromInt(dayInt);
//
//                         if (i == 7){
//                             timeTable["next_"+todayText + "_open"] = startTimeComponents;
//                             timeTable["next_"+todayText + "_close"] = endTimeComponents;
//                             timeTable["holiday_end"] = holiday_hours[holidayKey]["end_date"];
//                         } else {
//                             // Add this holidays open / close time to the timetable
//                             timeTable[todayText + "_open"] = startTimeComponents;
//                             timeTable[todayText + "_close"] = endTimeComponents;
//                         }
//
//                     }
//
//                 }
//
//             });
//
//             // fill in any blank slots there might be with standard hours
//             for (var i = 0; i < 7; i++){
//                 if (timeTable[getDayTextFromInt(i) + "_open"] === undefined ){
//                     timeTable[getDayTextFromInt(i) + "_open"] = standard_hours[getDayTextFromInt(i) + "_open"];
//                     timeTable[getDayTextFromInt(i) + "_close"] = standard_hours[getDayTextFromInt(i) + "_close"];
//                 }
//             }
//
//             var todayTimeInterval = todayInterval.getUTCHours() * 60 + todayInterval.getUTCMinutes();
//             // console.log(todayInterval.getUTCHours() + " UTC HOURS");
//
//             for (var i = 0; i < 8; i++){
//                 var todayInt = todayInterval.getUTCDay() + i;
//                 if (todayInt > 6){
//                     todayInt -= 7;
//                 }
//
//                 // console.log("Loop # " + i);
//
//                 if(timeTable[getDayTextFromInt(todayInt) + "_open"] !== timeTable[getDayTextFromInt(todayInt) + "_close"]){
//
//                     var todayOpen = getHoursFromString(timeTable[getDayTextFromInt(todayInt) + "_open"]) * 60 + getMinutesFromString(timeTable[getDayTextFromInt(todayInt) + "_open"]);
//                     var todayClose = getHoursFromString(timeTable[getDayTextFromInt(todayInt) + "_close"]) * 60 + getMinutesFromString(timeTable[getDayTextFromInt(todayInt) + "_close"]);
//
//                     // Reverse the flow if needed
//                     if (todayClose < todayOpen){
//                         todayInt += 1;
//
//                         if (todayInt > 6){
//                             todayInt -= 7;
//                         }
//
//                         todayClose = getHoursFromString(timeTable[getDayTextFromInt(todayInt) + "_close"]) * 60 + getMinutesFromString(timeTable[getDayTextFromInt(todayInt) + "_close"]) + 1440;
//                     }
//
//                     // console.log(todayOpen + " TODAY OPEN");
//                     // console.log(todayClose + "TODAY CLOSE");
//                     // console.log(todayTimeInterval + " TODAY TIME INTERVAL");
//
//                     if (todayTimeInterval >= todayOpen && todayTimeInterval <= todayClose && i == 0){
//                         // console.log("option 1");
//                         response.status(200).send("The store is open until: " + timeTable[getDayTextFromInt(todayInt) + "_close"]);
//                         break;
//                     } else if (todayTimeInterval < todayOpen && i == 0){
//                         // console.log("option 2");
//                         response.status(200).send("The store will be open at: " + timeTable[getDayTextFromInt(todayInt) + "_open"]);
//                         break;
//                     } else if (todayTimeInterval < todayOpen && i == 1) {
//                         // console.log("option 3");
//                         response.status(200).send("The store will be open at: " + timeTable[getDayTextFromInt(todayInt) + "_open"] + " tomorrow.");
//                         break;
//                     } else if (todayTimeInterval < todayOpen) {
//                         // console.log("option 4");
//                         response.status(200).send("The store will be open on: " +getDayTextFromInt(todayInt)+" at "+ timeTable[getDayTextFromInt(todayInt) + "_open"]);
//                         break;
//                     }
//
//                 } else if (i == 0){
//                     todayTimeInterval = 0;
//                 } else if (i == 7 && timeTable["next_"+getDayTextFromInt(todayInt) + "_open"] !== undefined){
//                     // console.log("The store is on holiday and will be open again on: " + timeTable["holiday_end"]);
//                     response.status(200).send("The store is on holiday and will be open again on: " + timeTable["holiday_end"]);
//                 }
//
//                 if (i == 7){
//                     // response.status(200).send({holidayOBJ: holiday_hours, standardHoursOBJ: standard_hours, timeTableOBJ: timeTable});
//                     response.status(200).send("Store is closed indefinitely");
//                 }
//
//             }
//
//             // response.status(200).send({holidayOBJ: holiday_hours, standardHoursOBJ: standard_hours, timeTableOBJ: timeTable});
//         }
//     }
//
//     // get standard hours
//     firebase.database().ref("businesses/-KM3W_NaGp49qXHs4-qh/business_hours/standard").once("value").then(function (snapshot) {
//
//         if (snapshot.exists()){
//
//             standard_hours = snapshot.val();
//             has_standard_hours = true;
//             buildTimeTable();
//
//         } else {
//             response.status(500).send("Internal Error");
//         }
//
//     });
//
//     // get holiday hours
//     firebase.database().ref("businesses/-KM3W_NaGp49qXHs4-qh/business_hours/holiday").once("value").then(function (snapshot) {
//
//         if (snapshot.exists()) {
//             holiday_hours = snapshot.val();
//         }
//
//         has_holiday_hours = true;
//         buildTimeTable();
//     });
//
//     // get offset
//     firebase.database().ref("businesses/-KM3W_NaGp49qXHs4-qh/stores/-KM3W_NaGp49qXHs4-qi/utc_offset").once("value").then(function (snapshot) {
//         if (snapshot.exists()){
//             utc_offset = snapshot.val();
//         }
//
//         has_offset = true;
//         buildTimeTable();
//
//     });
//
//     // returns the integer of the day in a week ie 0 for sunday, 1 for monday etc
//     function getDayAtStore(hoursAtLocation) {
//         var today = new Date().getUTCDate();
//
//         // is store ahead by one day
//         if (hoursAtLocation >= 24){
//
//             today += 1;
//             return today;
//             //is store behind by one day
//         } else if (hoursAtLocation < 0){
//
//             today -= 1;
//             return today;
//
//         } else {
//             return today;
//         }
//     }
//
//     // given a shortend month text expression determine its corressponding integer value
//     function getMonthIntegerForString(monthText) {
//         var obj = {
//             "Jan": 0,
//             "Feb": 1,
//             "Mar": 2,
//             "Apr": 3,
//             "May": 4,
//             "Jun": 5,
//             "Jul": 6,
//             "Aug": 7,
//             "Sep": 8,
//             "Oct": 9,
//             "Nov": 10,
//             "Dec": 11
//         };
//
//         return obj[monthText];
//
//     }
//
//     // Given the full the time description ie 2:30 PM extract the 24 clock hours
//     function getHoursFromString(hoursData) {
//
//         var components = hoursData.split(" ");
//         var hours = Number(components[0].split(":")[0]);
//
//         if (components[1] == "PM" && hours != 12){
//             hours += 12;
//         }
//
//         return hours;
//     }
//
//     // Given the full time description ie 2:30 PM extract the minutes
//     function getMinutesFromString(minutesData) {
//         var components = minutesData.split(" ");
//         var minutes = Number(components[0].split(":")[1]);
//
//         return minutes;
//     }
//
//     function getDayTextFromInt(dayInt) {
//
//         var obj = {
//             0: "sunday",
//             1: "monday",
//             2: "tuesday",
//             3: "wednesday",
//             4: "thursday",
//             5: "friday",
//             6: "saturday"
//         };
//
//         return obj[dayInt];
//
//     }
// });